<?PHP


error_reporting ( E_ALL ) ;
ini_set('display_errors', '1');
$suppress_gz_handler = 1 ;
@set_time_limit ( 15*60 ) ; # Time limit 45min

$hide_header = true ;
$hide_doctype = true ;
include_once ( "queryclass.php") ;
include_once ( "common_images.php" ) ;
high_mem ( 64 ) ;

//apache_setenv('no-gzip', 1);
ini_set('zlib.output_compression', 0);
ini_set('implicit_flush', 1);
//for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
//ob_implicit_flush(1);

$major_languages = array (
	'en', 'de', 'fr', 'pl', 'ja', 'nl', 'it', 'pt', 'es', 'sv', 
) ;

$evil_templates_commons = array (
	'pd-russia' ,
	'pd-soviet',
	'no source' ,
	'no source since' ,
	'pd-italy',
) ;

$hard_ignore_images['all'][] = 'Nobel_prize_medal.svg' ;
$hard_ignore_images['all'][] = 'Silhouette-personne.jpg' ;
$hard_ignore_images['all'][] = 'Tuzo_Nobel.png' ;
$hard_ignore_images['all'][] = 'Replace_this_image_male.png' ;
$hard_ignore_images['all'][] = 'Replace_this_image_female.png' ;
$hard_ignore_images['all'][] = 'NobelPrizeMedal.jpg' ;
$hard_ignore_images['all'][] = 'No_person.jpg' ;
$hard_ignore_images['all'][] = 'Libri_books2.jpg' ;
$hard_ignore_images['all'][] = 'No_portrait_cs.svg' ;
$hard_ignore_images['all'][] = 'Thinking_man.png' ;
$hard_ignore_images['all'][] = 'Harp2_ganson.svg' ;
$hard_ignore_images['all'][] = 'Julius_caesar.jpg' ;
$hard_ignore_images['all'][] = 'Japan_Prize.png' ;
$hard_ignore_images['all'][] = 'Yellow_star_Jude_Jew.svg' ;
$hard_ignore_images['all'][] = 'Révolution_de 1830_-_Combat_devant_l\'hôtel_de_ville_-_28.07.1830.jpg' ;
$hard_ignore_images['all'][] = 'Speaker_Icon.svg' ;
$hard_ignore_images['all'][] = 'Bologna-Stemma.png' ;
$hard_ignore_images['all'][] = 'Mammoth.png' ;
$hard_ignore_images['all'][] = 'Stub calciatori.png' ;




#____________________________________________________________________________________________________________

function print_form () {
  global $language , $project , $data , $datatype ;
  global $sources , $params ;
  global $tusc_user , $tusc_password ;
  
	header('Content-type: text/html; charset=utf-8');
	print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;
	
  print "<html style='border:10px solid #99C7FF;background-color:#EEEEEE'>
  <head>
  <title>FIST - Free Image Search Tool</title>
  <style type='text/css'>
    .form_table { border:1px solid black;background-color:#CCCCCC }
    .result_header { border-bottom:2px solid grey;background-color:#EEEEEE }
    .main_div { margin-left:5px;margin-right:5px;margin-bottom:15px;margin-top:0px }
    .main_th { border-bottom:2px solid black;background-color:#CCCCCC }
    .ll_unknown { border-bottom:1px solid grey;background-color:#FFFFCC }
    .ll_commons { border-bottom:1px solid grey;background-color:#CCCCFF }
    .ll_good { border-bottom:1px solid grey;background-color:#CCFFCC }
    .ll_bad { border-bottom:1px solid grey;background-color:#FFCCCC }
    .flickr { border-bottom:1px solid grey;background-color:#DDCCDD }
    .gimp { border-bottom:1px solid grey;background-color:#DDDDCC }
    .no_language_links { color:white; background-color:red; border:1px solid black; padding-left:5px; padding-right:5px }
    .black_bottom { border-bottom:1px solid black }
    .black_top { border-top:1px solid black }
    .gray_bottom { border-bottom:1px solid gray }
  </style>
  </head>
  <body style='margin:0px'>" ;

  $used = db_get_usage_counter ( 'fist' ) . '.' ;
  print get_common_header ( "fist.php" , "FIST" ) ;
  print "<div class='main_div'>" ;
  print "<h1 style='margin:0px;padding:0px'>
  <font size='8'>F<img src='http://upload.wikimedia.org/wikimedia/commons/thumb/2/2a/Fist.svg/26px-Fist.svg.png' />ST &ndash; Free Image Search Tool</font></h1>
  Search for free images to add to Wikipedia articles, or replace non-free images and placeholders.<br/>$used" ;
  
  $is_categories = $datatype == 'categories' ? 'checked' : '' ;
  $is_articles = $datatype == 'articles' ? 'checked' : '' ;
  $is_replaceimages = $datatype == 'replaceimages' ? 'checked' : '' ;
  $is_ricategories = $datatype == 'ricategories' ? 'checked' : '' ;
  $is_random = $datatype == 'random' ? 'checked' : '' ;
  
  $is_source_flickr = isset ( $sources['flickr'] ) ? 'checked' : '' ;
  $is_source_languagelinks = isset ( $sources['languagelinks'] ) ? 'checked' : '' ;
  $is_source_gimp = isset ( $sources['gimp'] ) ? 'checked' : '' ;
  $is_source_commons = isset ( $sources['commons'] ) ? 'checked' : '' ;
  $is_source_wts = isset ( $sources['wts'] ) ? 'checked' : '' ;
  $is_source_everystockphoto = isset ( $sources['everystockphoto'] ) ? 'checked' : '' ;
  $is_source_geograph = isset ( $sources['geograph'] ) ? 'checked' : '' ;
  $is_source_agenciabrasil = isset ( $sources['agenciabrasil'] ) ? 'checked' : '' ;
  
  
  $is_forarticles_all = $params['forarticles'] == 'all' ? 'checked' : '' ;
  $is_forarticles_noimage = $params['forarticles'] == 'noimage' ? 'checked' : '' ;
  $is_forarticles_lessthan = $params['forarticles'] == 'lessthan' ? 'checked' : '' ;
  $has_no_candidate = isset ( $params['skip_no_candidate'] ) ? 'checked' : '' ;
  $free_only = isset ( $params['free_only'] ) ? 'checked' : '' ;
  $commons_only = isset ( $params['commons_only'] ) ? 'checked' : '' ;
  $include_flickr_id = isset ( $params['include_flickr_id'] ) ? 'checked' : '' ;
  $flickr_new_name_from_article = isset ( $params['flickr_new_name_from_article'] ) ? 'checked' : '' ;
  $flickr_other_languages = isset ( $params['flickr_other_languages'] ) ? 'checked' : '' ;
  $esp_skip_flickr = isset ( $params['esp_skip_flickr'] ) ? 'checked' : '' ;
  $show_existing_images = isset ( $params['show_existing_images'] ) ? 'checked' : '' ;
  $commonsense = isset ( $params['commonsense'] ) ? 'checked' : '' ;
  $skip_articles_in_categories = isset ( $params['skip_articles_in_categories'] ) ? 'checked' : '' ;
  $use_article_as_file_name = isset ( $params['use_article_as_file_name'] ) ? 'checked' : '' ;
  $use_article_as_description = isset ( $params['use_article_as_description'] ) ? 'checked' : '' ;
  
  $is_out_html = $params['output_format'] == 'out_html' ? 'checked' : '' ;
  $is_out_xml = $params['output_format'] == 'out_xml' ? 'checked' : '' ;
  $is_out_json = $params['output_format'] == 'out_json' ? 'checked' : '' ;

  $jpeg = isset ( $params['jpeg'] ) ? 'checked' : '' ;
  $png = isset ( $params['png'] ) ? 'checked' : '' ;
  $gif = isset ( $params['gif'] ) ? 'checked' : '' ;
  $tiff = isset ( $params['tiff'] ) ? 'checked' : '' ;
  $svg = isset ( $params['svg'] ) ? 'checked' : '' ;
  $ogg = isset ( $params['ogg'] ) ? 'checked' : '' ;
    
  print "<form method='post' action='./fist_api.php'><table class='form_table' cellpadding='3px' cellspacing='0px'>
<!--
  <tr><th class='black_bottom'>Project</th><td colspan='2'>
  <input type='text' name='language' value='$language' size='2' /> .
  <input type='text' name='project' value='$project' size='10' /> .org
  </td></tr>
-->

  <!--Scan-->
  <tr><th class='black_bottom' valign='top' rowspan='2'>Scan</th>
  
  <td><input type='text' name='language' value='$language' size='2' /> .
  <input type='text' name='project' value='$project' size='10' /> .org
  </td></tr><tr>
  
  <td class='black_bottom'>
  <textarea name='data' cols='30' rows='8'>$data</textarea></td>
  <td class='black_bottom' valign='top'>To the left are (one line each):<br/>
  <input type='radio' name='datatype' value='categories' id='categories' $is_categories/>
  <label for='categories'>Categories of articles</label>
  (<input type='text' name='params[catdepth]' value='{$params['catdepth']}' size='1' /> subcategories deep)<br/>
  <input type='radio' name='datatype' value='articles' id='articles' $is_articles/>
  <label for='articles'>Articles</label><br/>
  <input type='radio' name='datatype' value='replaceimages' id='replaceimages' $is_replaceimages/>
  <label for='replaceimages'>Images to be replaced</label><br/>
  <input type='radio' name='datatype' value='ricategories' id='ricategories' $is_ricategories/>
  <label for='ricategories'>Categories of images to be replaced</label> [<i>slooow</i>]<br/>
  <input type='radio' name='datatype' value='random' id='random' $is_random/>
  <label for='random'>Random pages</label>
  (<input type='text' name='params[random]' value='{$params['random']}' size='1' /> &times;)  [<i>slooow</i>]<br/><br/>
  </td></tr>
  
  
  
  <!--Image sources-->
  <tr><th valign='top' rowspan='8'>Image<br/>sources</th>
  
  <!--Language links-->
  <td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[languagelinks]' id='languagelinks' value='1' $is_source_languagelinks/>
  <label for='languagelinks'>Other languages & Commons<br/><dd/><small>Follows language links and looks<br/>for images there</small></label>
  </td><td class='gray_bottom'>
  <input type='text' id='llmax' name='params[ll_max]' value='{$params['ll_max']}' size='2' />
  <label for='llmax'>results per article (max)</label><br/>
  <input type='checkbox' name='params[free_only]' id='free_only' value='1' $free_only/>
  <label for='free_only'>Show only images with free licenses</label><br/>
  <input type='checkbox' name='params[commons_only]' id='commons_only' value='1' $commons_only/>
  <label for='commons_only'>Show images from commons only</label>  
  </td></tr>
  
  <!--Commons-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[commons]' id='commons' value='1' $is_source_commons/>
  <label for='commons'><a target='_blank' href='http://commons.wikimedia.org'>Wikimedia Commons</a> search<br/>
  <dd/><small>Results directly from the internal<br/>Commons search</small></label>
  </td><td valign='top' class='gray_bottom'>
  <input type='text' id='commonsmax' name='params[commons_max]' value='{$params['commons_max']}' size='2' />
  <label for='commonsmax'> results per article (max)</label><br/>
<!--  <input type='checkbox' id='commonsense' name='params[commonsense]' $commonsense size='2' />
  <label for='commonsense'> Use CommonSense to suggest categories and galleries</label><br/> -->
  </td></tr>
  
  <!--Flickr-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[flickr]' id='flickr' value='1' $is_source_flickr/>
  <label for='flickr'><a target='_blank' href='http://www.flickr.com'>Flickr</a><br/>
  <dd/><small>Results from Flickr for CC-BY and<br/>CC-BY-SA images</small></label>
  </td><td class='gray_bottom'>
  <input type='text' id='flickrmax' name='params[flickr_max]' value='{$params['flickr_max']}' size='2' />
  <label for='flickrmax'> results per article (max)</label><br/>
  <input type='checkbox' name='params[include_flickr_id]' id='include_flickr_id' value='1' $include_flickr_id/>
  <label for='include_flickr_id'>Include the Flickr image ID in the filename suggestion</label><br/>
  <input type='checkbox' name='params[flickr_new_name_from_article]' id='flickr_new_name_from_article' value='1' $flickr_new_name_from_article/>
  <label for='flickr_new_name_from_article'>Base the filename suggestion on the article name</label><br/>
  <input type='checkbox' name='params[flickr_other_languages]' id='flickr_other_languages' value='1' $flickr_other_languages/>
  <label for='flickr_other_languages'>Try language link titles on flickr if neccessary</label>
  </td></tr>

  <!--WikiTravel Shared-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[wts]' id='wts' value='1' $is_source_wts/>
  <label for='wts'><a target='_blank' href='http://wikitravel.org/shared'>WikiTravel Shared</a> search<br/>
  <dd/><small>Results directly from the internal<br/>WikiTravel search</small></label>
  </td><td valign='top' class='gray_bottom'>
  <input type='text' id='wtsmax' name='params[wts_max]' value='{$params['wts_max']}' size='2' />
  <label for='wtsmax'> results per article (max)</label><br/>
  <i>Note : No thumbnails available; click on the icons</i>
<!--  <input type='checkbox' id='commonsense' name='params[commonsense]' $commonsense size='2' />
  <label for='commonsense'> Use CommonSense to suggest categories and galleries</label><br/>-->
  </td></tr>
  
  <!--GIMP-SAVY-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[gimp]' id='gimp' value='1' $is_source_gimp/>
  <label for='gimp'><a target='_blank' href='http://gimp-savvy.com/PHOTO-ARCHIVE/index.html'>GIMP-SAVVY</a><br/>
  <dd/><small>Searches a collection of PD images,<br/>mostly NASA and NOAA</small></label>
  </td><td class='gray_bottom'>
  <input type='text' id='gimpmax' name='params[gimp_max]' value='{$params['gimp_max']}' size='2' />
  <label for='gimpmax'> results per article (max)</label><br/>
  <i>Note : GIMP-SAVVY blocks most thumbnail requests.</i>
  </td></tr>
  
  <!--EVERYSTOCKPHOTO-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[everystockphoto]' id='everystockphoto' value='1' $is_source_everystockphoto/>
  <label for='everystockphoto'><a target='_blank' href='http://everystockphoto.com/index.html'>everystockphoto</a><br/>
  <dd/><small>Stock photos under various licenses</small></label>
  </td><td class='gray_bottom'>
  <input type='text' id='espmax' name='params[esp_max]' value='{$params['esp_max']}' size='2' />
  <label for='espmax'> results per article (max)</label><br/>
<!--  <input type='checkbox' name='params[esp_skip_flickr]' id='esp_skip_flickr' value='1' $esp_skip_flickr/>
  <label for='esp_skip_flickr'>Do no show results from flickr</label> -->
  </td></tr>
  
  <!--agenciabrasil-->
  <tr><td valign='top' class='gray_bottom'>
  <input type='checkbox' name='sources[agenciabrasil]' id='agenciabrasil' value='1' $is_source_agenciabrasil/>
  <label for='agenciabrasil'><a target='_blank' href='http://www.agenciabrasil.gov.br'>Agencia Brasil</a><br/>
  <dd/><small>Government photos under CC-BY 2.5</small></label>
  </td><td class='gray_bottom'>
  <input type='text' id='abmax' name='params[ab_max]' value='{$params['ab_max']}' size='2' />
  <label for='abmaxmax'> results per article (max)</label>
  </td></tr>
  
  <!--GEOGRAPH.ORG-->
  <tr><td valign='top'>
  <input type='checkbox' name='sources[geograph]' id='geograph' value='1' $is_source_geograph/>
  <label for='geograph'><a target='_blank' href='http://www.geograph.org.uk'>Geograph (UK)</a><br/>
  <dd/><small>Photos from the UK under CC-BY-SA-2.0</small></label>
  </td><td>
  <input type='text' id='geographmax' name='params[geograph_max]' value='{$params['geograph_max']}' size='2' />
  <label for='geographmax'> results per article (max)</label>
  </td></tr>
  
  
  
  <!--List-->
  <tr><th valign='top' class='black_top'>List</th>
  
  <td class='black_top' valign='top'>
  <input type='radio' name='params[forarticles]' id='forarticles_all' value='all' $is_forarticles_all/>
  <label for='forarticles_all'>All articles</label><br/>
  <input type='radio' name='params[forarticles]' id='forarticles_noimage' value='noimage' $is_forarticles_noimage/>
  <label for='forarticles_noimage'>Articles that have no image</label><br/>
  <input type='radio' name='params[forarticles]' id='forarticles_lessthan' value='lessthan' $is_forarticles_lessthan/>
  <label for='forarticles_lessthan'>Articles that have less than</label>
  <input type='text' name='params[lessthan_images]' value='{$params['lessthan_images']}' size='1' />
  images<br/>

  <input type='checkbox' name='params[skip_articles_in_categories]' id='skip_articles_in_categories' value='1' $skip_articles_in_categories/>
  <label for='skip_articles_in_categories'>Skip articles in </label><a target='_blank' href=\"http://meta.wikipedia.org/wiki/FIST/Ignored_categories\">these categories</a><br/>

  <input type='checkbox' name='params[show_existing_images]' id='show_existing_images' value='1' $show_existing_images/>
  <label for='show_existing_images'>Show images already in the article</label><br/>

  <input type='checkbox' name='params[skip_no_candidate]' id='skip_no_candidate' value='1' $has_no_candidate/>
  <label for='skip_no_candidate'>List only articles with potential images</label><br/>

  <input type='checkbox' name='params[use_article_as_file_name]' id='use_article_as_file_name' value='1' $use_article_as_file_name/>
  <label for='use_article_as_file_name'>Use the article name as default filename</label><br/>

  <input type='checkbox' name='params[use_article_as_description]' id='use_article_as_description' value='1' $use_article_as_description/>
  <label for='use_article_as_description'>Use the article name as description</label><br/>
  
  Default thumbnail size is 
  <input type='text' name='params[default_thumbnail_size]' value='{$params['default_thumbnail_size']}' size='1' /> px<br/>

  <table border='0'>
  <tr><td valign='top' rowspan='2'>Find</td>
  <td><input type='checkbox' name='params[jpeg]' id='jpeg' value='1' $jpeg/><label for='jpeg'>JPEG</label></td>
  <td><input type='checkbox' name='params[png]' id='png' value='1' $png/><label for='png'>PNG</label></td>
  <td><input type='checkbox' name='params[gif]' id='gif' value='1' $gif/><label for='gif'>GIF</label></td>
  </tr><tr>
  <td><input type='checkbox' name='params[svg]' id='svg' value='1' $svg/><label for='svg'>SVG</label></td>
  <td><input type='checkbox' name='params[ogg]' id='ogg' value='1' $ogg/><label for='ogg'>OGG</label></td>
  </tr></table>
  
  Potential images have to be at least<dd/>
  <input type='text' name='params[min_width]' value='{$params['min_width']}' size='1' />
  &times
  <input type='text' name='params[min_height]' value='{$params['min_height']}' size='1' />
  pixels in size
  </td>
  
  <td class='black_top' valign='top'>
	Color codes :<br/>
  	<div class='ll_commons'>Image from commons (free license)</div>
  	<div class='ll_good'>Free license or public domain</div>
  	<div class='ll_unknown'>Unknown copyright status</div>
  	<div class='ll_bad'>Non-free image (copyvio, fair use, etc.)</div>
  	<div class='flickr'>Image from flickr (CC-BY/-SA license)</div>
  	<div class='gimp'>Image from GIMP-SAVVY (public domain)</div>
  	<hr/>
  	Results as 
    <input type='radio' name='params[output_format]' value='out_html' id='out_html' $is_out_html/>
    <label for='out_html'>Web page</label>
    <input type='radio' name='params[output_format]' value='out_xml' id='out_xml' $is_out_xml/>
    <label for='out_xml'>XML</label>
    <input type='radio' name='params[output_format]' value='out_json' id='out_json' $is_out_json/>
    <label for='out_json'>JSON</label>
  	
  </td>

  </tr>
  
  <!-- TUSC -->
  <tr>
  <th style='border-top:2px solid black'>Commons user name</th>
  <td style='border-top:2px solid black' colspan='3'><input name='tusc_user' value='$tusc_user' type='text' /></td>
  </tr>
  <tr>
  <th>TUSC password</th>
  <td colspan='2'><input name='tusc_password' value='$tusc_password' type='password' /></td>
  <td><input type='submit' name='doit' value='Do it!' /></td>
  </tr>

  </table></form>" ;
  myflush() ;
}

#______________________________________________________
# Get the list of articles, according to mode
function get_article_list () {
	global $language , $project , $data , $datatype , $params ;
	global $wq , $articles ;
	$articles = array () ;
	$lines = explode ( "\n" , $data ) ;

	foreach ( $lines AS $k => $v ) {
		remove_namespace_prefix ( $lines[$k] , 6 ) ; # Image
		remove_namespace_prefix ( $lines[$k] , 14 ) ; # Category
	}
	
	# Random
	if ( $datatype == 'random' ) {
		$at = db_get_random_pages ( $language , $project , 0 , $params['random'] ) ;
		foreach ( $at AS $a ) $articles[$a] = $a ;
		return ;
	}
	
	foreach ( $lines AS $l ) {
		$l = ucfirst ( trim ( $l ) ) ;
		if ( $l == '' ) continue ;
		if ( $datatype == 'categories' ) {
			$dummyarray = array() ;
			$at = db_get_articles_in_category ( $language , $l , $params['catdepth'] , 0 , $dummyarray , true , 25000 ) ;
			if ( count ( $at ) == 0 ) { # None found, try talk pages...
				$dummyarray = array() ;
				$at = db_get_articles_in_category ( $language , $l , $params['catdepth'] , 1 , $dummyarray , true , 25000 ) ;
			}
			foreach ( $at AS $a ) $articles[$a] = $a ;
		} else if ( $datatype == 'articles' ) {
			$articles[$l] = $l ;
		} else if ( $datatype == 'replaceimages' ) {
      $l = "Image:$l" ;
			$at = $wq->get_used_image ( $l ) ;
			foreach ( $at AS $a ) $articles[$a] = $a ;
		} else if ( $datatype == 'ricategories' ) {
			$at2 = db_get_images_in_category ( $language , $l , $params['catdepth'] ) ;
			foreach ( $at2 AS $b ) {
//        print "!$b! " ;
        $b = "Image:$b" ;
				$at = $wq->get_used_image ( $b ) ;
				foreach ( $at AS $a ) $articles[$a] = $a ;
			}
		}
	}
	asort ( $articles ) ;
}

#______________________________________________________
# Determine article "type" (license status as good, bad, commons)
function get_image_type ( $language , $title , $templates ) {
	global $good_templates , $evil_templates , $evil_templates_commons ;
	
	$status = 'unknown' ;
	foreach ( $templates AS $t ) {
		$t = strtolower ( $t ) ;
		$t2 = str_replace ( '_' , ' ' , $t ) ;
		if ( $language == 'commons' ) {
			if ( in_array ( $t , $evil_templates_commons ) ) return 'bad' ;
			if ( in_array ( $t2 , $evil_templates_commons ) ) return 'bad' ;
			continue ;
		}
		if ( in_array ( $t , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t2 , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t , $good_templates ) ) $status = 'good' ;
		if ( in_array ( $t2 , $good_templates ) ) $status = 'good' ;
	}
	if ( $language == "commons" ) return "commons" ;
	
	return $status ;
}

#______________________________________________________
# Generate results wrapper table for a single source
function results_section ( $text , $title , $colspan , $append = '' ) {
  if ( $text == '' ) return '' ;
  $ret = '<table width="100%" cellspacing="1px" cellpadding="2px">' ;
  $ret .= '<tr><td colspan="' . 
          $colspan . 
          '" class="result_header"><b>' . 
          $title . 
          '</b>' . 
          $append . 
          '</td></tr>' ;
  $ret .= $text ;
	$ret .= "</table>" ;
  return $ret ;
}

function remove_namespace_prefix ( &$article , $namespace ) {
	global $namespaces ;
	$article = ucfirst ( trim ( $article ) ) ;
	$l = strlen ( $namespaces[$namespace] ) + 1 ;
	if ( substr ( $article , 0 , $l ) == $namespaces[$namespace] . ':' ) {
		$article = substr ( $article , $l ) ;
	}
}


function cleanup_article_name ( &$article ) {
  $article = str_replace ( '_' , ' ' , $article ) ;
  $article = str_replace ( '-' , ' ' , $article ) ;
  $article = str_replace ( '(' , ' ' , $article ) ;
  $article = str_replace ( ')' , ' ' , $article ) ;
}


function get_text_between ( $text , $start , $end ) {
	$x = array_pop ( explode ( $start , $text , 2 ) ) ;
	return array_shift ( explode ( $end , $x , 2 ) ) ;
}

function suggest_image_name ( $fn , $article ) {
	global $params ;
	if ( !isset ( $params['use_article_as_file_name'] ) ) return $fn ;
	$x = explode ( '.' , $fn ) ;
	if ( count ( $x ) < 2 ) return $article ;
	return $article . '.' . array_pop ( $x ) ;
}








#_____________________________________________________________________________________________________________
# FLICKR

function find_images_flickr ( $article , $ll = false , $count = 0 ) {
  global $params , $language_links , $language , $project , $major_languages , $api_output ;
  $apiret = array () ;
  $username = "" ;
  $ret = "" ;
  $ret2 = "" ;
  $include_flickr_id = isset ( $params['include_flickr_id'] ) ;
  $flickr_new_name_from_article = isset ( $params['flickr_new_name_from_article'] ) ? 'checked' : '' ;
  
  $article1 = str_replace ( ')' , ' ' , $article ) ;
  $article1 = str_replace ( '(' , ' ' , $article1 ) ;
  $article1 = str_replace ( '-' , ' ' , $article1 ) ;
  $article2 = array_shift ( explode ( '(' , $article , 2 ) ) ;
  
  $found = get_flickr_hits ( $article1 , $params['flickr_max'] ) ;
  if ( count ( $found ) == 0 and $article1 != $article2 ) $found = get_flickr_hits ( $article2 , $params['flickr_max'] ) ;

	if ( count ( $found ) < $params['flickr_max'] and !$ll and isset ( $params['flickr_other_languages'] ) ) {
		if ( !isset ( $language_links ) ) $language_links = db_get_language_links ( $article , $language , $project ) ;
		$check = array () ;
		foreach ( $language_links AS $k => $v ) {
			$v = str_replace ( '-' , ' ' , $v ) ;
			if ( $k == $language ) continue ;
			if ( $v == $article ) continue ;
			if ( !in_array ( $k , $major_languages ) ) continue ;
			if ( in_array ( $v , $check ) ) continue ;
			$check[] = $v ;
		}
		
		$count2 = count ( $found ) ;
		foreach ( $check AS $c ) {
			$subimages = find_images_flickr ( $c , true , $count2 ) ;
			if ( $api_output ) {
				foreach ( $subimages AS $s ) $apiret[] = $subimages ;
			} else $ret2 .= $subimages ;
			$count2 = count ( $found ) . substr_count ( $ret2 , '<tr>' ) ;
			if ( $count2 >= $params['flickr_max'] ) break ;
		}
	}


  if ( !$api_output and count ( $found ) == 0 and $ret2 == '' ) return '' ; # Nothing found on flickr
  if ( $api_output and count ( $found ) == 0 and count ( $ret2 ) ) return $apiret ; # Nothing found on flickr

  $missing_title = '<i>Unnamed image</i>' ;
  foreach ( $found AS $f ) {
    $count++ ;
    if ( $count > $params['flickr_max'] ) break ;
  	if ( $flickr_new_name_from_article or $f->title == $missing_title ) $f->title = $article ;

	if ( $api_output ) {
		$flickr_api_url = "http://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=".get_flickr_key()."&photo_id=" . $f->id ;
		$flickr_text = file_get_contents ( $flickr_api_url ) ;
		$flickr_image_url = array_pop ( explode ( " source=\"" , $flickr_text ) ) ;
		$flickr_image_url = trim ( array_shift ( explode ( "\"" , $flickr_image_url , 2 ) ) ) ;
		
		$apiret[] = array (
			'n' => 'image' ,
			'a' => array (
				'name' => apitext ( $f->title ) ,
				'id' => apitext ( $f->id ) ,
				'location' => 'flickr' ,
				'desc_url' => apiurl ( $f->page_url ) ,
				'thumb_url' => apiurl ( $f->s_url ) ,
				'file_url' => apiurl ( $flickr_image_url ) ,
			)
		) ;
	}
  	
    $ret .= '<tr><td class="flickr">' ;
    $ret .= get_flickr_image_table ( $f , $username , $include_flickr_id ) ;
    $ret .= '</td></tr>' ;
  }

  if ( !$ll ) {
    $app = ' (<a target="_blank" href="http://flickr.com/search/?ct=0&l=commderiv&q=' . urlencode ( $article1 ) . '">search flickr manually</a>)' ;
    $ret = results_section ( $ret.$ret2 , 'Flickr' , 1 , $app ) ;
  }

	if ( $api_output ) {
		if ( !$ll ) {
			$apiret = array (
				'n' => 'set' ,
				'*' => $apiret ,
				'a' => array ( 'type' => 'flickr' )
			) ;
		}
		return $apiret ;
	}

	return $ret ;
}



#_____________________________________________________________________________________________________________
# LANGUAGE LINKS

function find_images_languagelinks ( $article ) {
	global $params , $language , $project , $note , $language_links , $thumbsize ;
	global $tusc_user , $tusc_password , $image_ns_name , $api_output ;
	$langlinks = db_get_language_links ( $article , $language , $project ) ;
	$language_links = $langlinks ;
	
	$ret = array () ;
	
	if ( count ( $langlinks ) == 0 ) { # No language links, no images...
		$url = "http://www.google.de/search?start=0&ie=utf-8&oe=utf-8&q=" ;
		$url .= urlencode ( "$article site:$project.org -site:$language.$project.org" ) ;
		if ( $note != '' ) $note .= '; ' ;
		$note .= '<span class="no_language_links">No language links</span>' ;
		$note .= ". <a target='_blank' href=\"$url\">Google-search other " . $project . "s</a> for this" ;
		return $ret ;
	}
  
	$used_images = db_get_image_list ( $article , $language , $project ) ; # Already used in article
	
	$images = array () ;
	$commons_usage = array () ;
	$images['commons'] = array () ;
	foreach ( $langlinks AS $lang => $page ) {
    $all_images = db_get_image_list ( $page , $lang , $project ) ;
    if ( count ( $all_images ) == 0 ) continue ;
    $local_images = db_local_images ( $lang , $all_images ) ;
    if ( !isset ( $images[$lang] ) ) $images[$lang] = array () ;
    foreach ( $all_images AS $image ) {
      if ( in_array ( $image , $local_images ) ) {
        if ( !isset ( $params['commons_only'] ) ) $images[$lang][$image] = $image ;
      } else {
      	$images['commons'][$image] = $image ;
      	$thelink = get_wikipedia_url ( $lang , $page , '' , $project ) ;
      	$thelink = "<a target='_blank' href=\"$thelink\">$lang</a>" ;
      	if ( isset ( $commons_usage[$image] ) ) $commons_usage[$image] .= ", $thelink" ;
      	else $commons_usage[$image] = $thelink ;
      }
    }
	}

#	print "TESTING:" . implode ( "|" , array_keys ( $commons_usage ) ) . "<br/>" ;

	unset ( $images[$language] ) ; # Paranoia
	$images['_commons'] = $images['commons'] ;
	unset ( $images['commons'] ) ;
	ksort ( $images ) ;
	$count = 0 ;
	foreach ( $images AS $lang => $limages ) {
    if ( $lang == '_commons' ) $lang = 'commons' ;
    $pro = $lang == 'commons' ? 'wikimedia' : $project ;
    foreach ( $limages AS $image ) {
      if ( in_array ( $image , $used_images ) ) continue ; # Already used in that article
      
      # Get image data and check size
      $data = db_get_image_data ( $image , $lang , $pro ) ;
      if ( !isset ( $data->img_width ) ) continue ;
      if ( $data->img_width == '' or $data->img_height == '' ) continue ; # Non-images
      if ( $data->img_width < $params['min_width'] ) continue ;
      if ( $data->img_height < $params['min_height'] ) continue ;

      $count++ ;
      if ( $count > $params['ll_max'] ) break ;
      
      if ( $data->img_width > $data->img_height ) {
        $nw = $thumbsize ;
      } else {
        $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
      }
      
      $thumburl = get_thumbnail_url ( $lang , $image , $nw , $pro ) ;
      $imageurl = get_wikipedia_url ( $lang , "Image:$image" , "" , $pro ) ;
      $image_title = str_replace ( '_' , ' ' , $image ) ;
      
      $templates = db_get_used_templates ( $lang , $image , 6 ) ;

      if ( $language == 'de' && in_array ( 'PD-US-no_notice' , $templates ) ) continue ;
      if ( $language == 'de' && in_array ( 'PD-US-not_renewed' , $templates ) ) continue ;
      
      $type = get_image_type ( $lang , $image , $templates ) ;
      $class = 'unknown' ;
      if ( $lang == 'commons' ) $class = 'commons' ;
      if ( $type == 'good' or $type == 'bad' ) $class = $type ;
      $style = "valign='top' class='ll_$class'" ;
      
      if ( $lang != 'commons' ) $template_text = implode ( '</i>", "<i>' , $templates ) ;
      else $template_text = '' ;
      if ( $template_text != '' ) $template_text = "Uses templates \"<i>$template_text</i>\"." ;
      
      $image_noext = explode ( '.' , $image_title ) ;
      array_pop ( $image_noext ) ;
      $image_noext = implode ( '.' , $image_noext ) ;
      if ( $lang == 'commons' ) {
      	if ( !isset ( $commons_usage[$image] ) ) $thumb_text = '' ;
      	else $thumb_text = "Used on " . $commons_usage[$image] . ".<br/>" ;
      	
      	if ( isset ( $params['use_article_as_description'] ) ) $thumb_desc = "$article." ;
      	else $thumb_desc = "$image_noext." ;
        if ( $params['default_thumbnail_size'] != '' ) $thumb_desc = $params['default_thumbnail_size'] . "px|" . $thumb_desc ;
        
      	$thumb_text .= "<tt>[[$image_ns_name:$image_title|thumb|$thumb_desc]]</tt>" ;
      } else $thumb_text = '' ;
      
      if ( $lang != 'commons' and $type == 'good' ) {
      	$newname = suggest_image_name ( $image_title , $article ) ;
        $newname = ansi2ascii ( $newname ) ;
        $image_enc = $image_title ;
        $upload = "<form method='post' action='./commonshelper.php' target='_blank'>
        <input type='text' name='newname' size='40' value='$newname' />
        <input type='hidden' name='image' value='$image_enc' />
        <input type='hidden' name='language' value='$lang' />
        <input type='hidden' name='tusc_user' value='$tusc_user' />
        <input type='hidden' name='tusc_password' value='$tusc_password' />
        <input type='hidden' name='commonsense' value='1' />
        <input type='checkbox' id='reallydirectupload' name='reallydirectupload' value='1' checked />
        <label for='reallydirectupload'>One-click upload</label>
        <input type='submit' name='doit' value='Upload to commons' />
        </form>" ;
      } else $upload = '' ;
      
      $pr = $lang == 'commons' ? 'wikimedia' : $project ;
      
      if ( $api_output ) {
      	$ret[] = array (
      		'n' => 'image' ,
      		'a' => array (
      			'name' => apitext ( $image_title ) ,
      			'location' => "$lang.$pr" ,
      			'width' => $data->img_width ,
      			'height' => $data->img_height ,
      			'bytes' => $data->img_size ,
      			'file_url' => apiurl ( get_image_url ( $lang , $image , $project ) ) ,
      			'desc_url' => apiurl ( $imageurl ) ,
      			'thumb_url' => apiurl ( $thumburl ) ,
      			'freeness' => $type ,
#      			'note' => apitext ( $template_text . $thumb_text ) ,
      			
      		)
      	) ;
      } else {
		  $ret[] = "<tr>
		  <td $style width='75px'><a target='_blank' href=\"$imageurl\"><img border='0' src='$thumburl'/></a></td>
		  <td $style>
		  <b>$image_title</b> on $lang.$pr;
		  {$data->img_width}&times;{$data->img_height}px, {$data->img_size} bytes<br/>
		  $template_text$thumb_text$upload
		  </td></tr>" ;
		}
    }
	}
	
	if ( count ( $ret ) == 0 ) { # Nothing found
	    if ( $note != '' ) $note .= '; ' ;
    
		$langlink_links = array () ;
		foreach ( $langlinks AS $k => $v ) {
			$langlink_links[] = "<a target='_blank' href=\"" . get_wikipedia_url ( $k , $v , '' , $project ) . "\">$k</a>" ;
		}
		
		$note .= "Searched for images in " . implode ( ', ' , $langlink_links ) . " - no suitable ones found" ;
	} else {
		if ( $api_output ) {
			return array (
				'n' => 'set' ,
				'a' => array ( 'type' => 'language_links' ) ,
				'*' => $ret
			) ;
		} else {
			$ret = implode ( '' , $ret ) ;
		    $ret = results_section ( $ret , 'Images used in other wikipedias (from the respective wikipedias or from Wikimedia Commons)' , 2 ) ;
		}
	}

	return $ret ;
}

#_____________________________________________________________________________________________________________
# GIMP-SAVVY

function find_images_gimp ( $article ) {
	global $params , $api_output ;
	$server = "http://gimp-savvy.com" ;
	$url = "$server/cgi-bin/keysearch.cgi?words=" . urlencode ( $article ) ;
	$html = file_get_contents ( $url ) ;
	$html = array_pop ( explode ( '<table' , $html ) ) ;
	$html = array_shift ( explode ( '</table' , $html ) ) ;
	$trs = explode ( '<tr' , $html ) ;
	array_shift ( $trs ) ;

	$results = array () ;
	foreach ( $trs AS $tr ) {
		if ( count ( explode ( '100%' , $tr , 2 ) ) != 2 ) continue ; # No full match
		$x = explode ( " href='" , $tr , 2 ) ;
		if ( count ( $x ) < 2 ) continue ;
		$x = array_pop ( $x ) ;
		$x = explode ( "'" , $x , 2 ) ;
		$r = "" ;
		$r->img_url = array_shift ( $x ) ;
		
		$x = array_pop ( $x ) ;
		$x = explode ( '<img src=' , $x , 2 ) ;
		if ( count ( $x ) < 2 ) continue ;
		$x = array_pop ( $x ) ;
		$x = explode ( ' ' , $x , 2 ) ;
		$r->thumb_url = array_shift ( $x ) ;
		$x = explode ( '>' , array_pop ( $x ) , 2 ) ;
		$r->thumb_size = array_shift ( $x ) ;
		
		$x = explode ( '<b>' , array_pop ( $x ) , 2 ) ;
		$x = explode ( '</b>' , array_pop ( $x ) , 2 ) ;
//		$r->img_wh = explode ( 'x' , array_shift ( $x ) ) ;
		$r->img_size = str_replace ( 'x' , '&times;' , array_shift ( $x ) ) ;

		
		$y = file_get_contents ( $server . $r->img_url ) ;

		$x = array_pop ( explode ( "<img src='" , $y , 2 ) ) ;
		$x = array_shift ( explode ( "'" , $x , 2 ) ) ;
		$r->img_url_real = $x ;
		
		$x = array_pop ( explode ( "<strong>" , $y , 2 ) ) ;
		$x = array_shift ( explode ( "</strong>" , $x , 2 ) ) ;
		$r->tags = trim ( $x ) ;
		
		$results[] = $r ;
		if ( count ( $results ) >= $params['gimp_max'] ) break ;
	}
	
	if ( count ( $results ) == 0 ) return "" ;
	
	$gs_logo = 'http://gimp-savvy.com/images/gimp-savvy.gif' ;
	
	if ( $api_output ) $ret = array ( 'n' => 'set' , 'a' => array ( 'type' => 'gimp-savvy' ) , '*' => array() ) ;
	else $ret = "" ;
	foreach ( $results AS $r ) {
		if ( $api_output ) {
			$n = array (
				'n' => 'image' ,
				'a' => array (
					'name' => apitext ( array_pop ( explode ( '?' , $r->img_url ) ) ) ,
					'freeness' => 'PD' ,
					'thumb_url' => apiurl ( $gs_logo ) ,
					'file_url' => apiurl ( $server . $r->img_url_real ) ,
					'desc_url' => apiurl ( $server . $r->img_url ) ,
//					'width' => $r->img_wh[0] ,
//					'height' => $r->img_wh[1] ,
					'text' => apitext ( $r->tags ) ,
				)
			) ;
			$ret['*'][] = $n ;
		} else {
			$ret .= "<tr><td class='gimp' valign='top'>" ;
			$ret .= '<a target="_blank" href="' . $server . $r->img_url_real . '">' ;
//			$ret .= "<img src=\"" . $server . $r->thumb_url . "\" alt='Thumbnail access restricted by GIMP-SAVVY' border=0 " . $r->thumb_size . " />" ;
			$ret .= "<img lowsrc=\"$gs_logo\" src=\"" . $server . $r->img_url_real . "\" width='120px' border=0 alt='Thumbnail access restricted by GIMP-SAVVY' />" ;
			$ret .= "</a>" ;
			$ret .= "</td><td class='gimp' width='100%' valign='top'>" ;
			$ret .= "<b>" . array_pop ( explode ( '?' , $r->img_url ) ) . "</b>" ;
			$ret .= " (<a target='_blank' href='" . $server . $r->img_url . "'>description page</a>)" ;
//			$ret .= "<br/>" . $r->img_size . " px" ;
			$ret .= "<br/>Tags : <i>" . $r->tags . "</i>" ;
			$ret .= "</td></tr>" ;
		}
	}
	if ( $api_output ) return $ret ;
	return results_section ( $ret , "GIMP-SAVVY" , 2 ) ;
}


#_____________________________________________________________________________________________________________
# COMMONS

function find_images_commons ( $article ) {
	global $params , $api_output ;
	if ( $api_output ) $ret = array ( 'n' => 'set' , '*' => array() , 'a' => array ( 'type' => 'commons' ) ) ;
	else $ret = '' ;
	
	$result = json_decode ( file_get_contents ( "http://commons.wikimedia.org/w/api.php?action=opensearch&search=".myurlencode($article)."&format=json&namespace=6&limit=" . $params['commons_max'] ) ) ;
	$result = $result[1] ;
#	print "<pre>" ; print_r ( $result ) ; print "</pre>" ;
	
	foreach ( $result AS $img ) {
		$name = array_pop ( explode ( ':' , $img , 2 ) ) ;
		$desc_url = 'http://commons.wikimedia.org/wiki/File:' . myurlencode ( $name ) ;
		$file_url = get_image_url ( 'commons' , $name , 'wikipedia' ) ;
		$thumb_url = get_thumbnail_url ( 'commons' , $name , 120 , 'wikipedia' ) ;
		
		if ( $api_output ) {
			$ret['*'][] = array ( 'n' => 'image' , 'a' => array (
				'name' => apitext ( $name ) ,
				'desc_url' => apiurl ( $desc_url ) ,
				'file_url' => apiurl ( $file_url ) ,
				'thumb_url' => apiurl ( $thumb_url ) ,
				'freeness' => 'commons' ,
			) ) ;
		} else {
			$ret .= "<tr>" ;
			$ret .= "<td class='ll_commons'><a target='_blank' href='$desc_url'><img border=0 src='$thumb_url' /></a></td>" ;
			$ret .= "<td class='ll_commons' width='100%' valign='top'><b>$name</b><br/>" ;
			$ret .= "<tt>[[File:$name|thumb|" ;
			if ( $params['default_thumbnail_size'] != '' ) $ret .= $params['default_thumbnail_size'] . "px|" ;
			if ( isset ( $params['use_article_as_description'] ) ) $ret .= $article ;
			else $ret .= $name ;
			$ret .= "]]</tt></td>" ;
			$ret .= "</tr>" ;
		}
	}
	
	if ( $api_output ) return $ret ;
	
	$search_link = "http://commons.wikimedia.org/w/index.php?title=Special%3ASearch&fulltext=Search&search=" . myurlencode ( $article ) ;
	$search_link = " (<a href='$search_link' target='_blank'>Search Commons for $article</a>)" ;
	return results_section ( $ret , 'Wikimedia Commons' , 2 , $search_link ) ;
}

function find_images_wikitravel ( $article ) {
  return find_images_mediawiki ( $article , 
            'http://wikitravel.org/shared/Special:Search?ns6=1&searchx=Search&search=' , 
            'WikiTravel Shared' ,
            '<ol' ,
            '</ol>' ,
            " (search 
  <a target=\"_blank\" href=\"http://wikitravel.org/shared\"><img border='0' src=\"http://wikitravel.org/wikitravel-shared.png\" height='14px'/></a>
  <a target=\"_blank\" href=\"http://wikitravel.org/shared/Special:Search?ns6=1&searchx=Search&search=$1\">WikiTravel Shared</a>)" ,
            'll_good' ,
            0 ) ;
}

function find_images_mediawiki ( $article , $base_url , $thesource , $sep1 , $sep2 , $theheading , $class , $is_wikimedia ) {
  global $params , $imagetypes , $redirect_from , $api_output ;
  $thumbsize = 75 ;
  cleanup_article_name ( $article ) ;
  $url = $base_url . urlencode ( $article ) ;
  $html = file_get_contents ( $url ) ;
  
  $x = array_pop ( explode ( '</strong>' , $html , 2 ) ) ;
  $x = array_pop ( explode ( $sep1 , $x , 2 ) ) ;
  $x = array_shift ( explode ( $sep2 , $x , 2 ) ) ;
  
  $x = explode ( '</a>' , $x ) ;
  array_pop ( $x ) ; # Not needed
  
  $ret = array() ;
  $count = 0 ;
  foreach ( $x AS $i ) {
    $i = array_pop ( explode ( '">' , $i ) ) ;
    $i2 = array_pop ( explode ( ':' , $i , 2 ) ) ; # Removing "Image:"

    $image_noext = explode ( '.' , $i2 ) ;
    $ext = strtolower ( array_pop ( $image_noext ) ) ;
    $image_noext = implode ( '.' , $image_noext ) ;
    if ( !isset ( $imagetypes[$ext] ) ) continue ;

    # Get image data and check size
    if ( $is_wikimedia ) {
      $data = db_get_image_data ( $i2 , 'commons' , 'wikimedia' ) ;
      if ( !isset ( $data->img_width ) ) continue ;
      if ( $data->img_width == '' or $data->img_height == '' ) continue ; # Non-images
      if ( $data->img_width < $params['min_width'] ) continue ;
      if ( $data->img_height < $params['min_height'] ) continue ;
      
      $count++ ;
      if ( $count > $params['commons_max'] ) break ;

      if ( $data->img_width > $data->img_height ) {
        $nw = $thumbsize ;
      } else {
        $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
      }
    } else {
      $nw = $thumbsize ; # FIX THIS!!!
    }

    if ( $is_wikimedia ) {
      $thumb_url = get_thumbnail_url ( 'commons' , $i2 , $nw , 'wikimedia' ) ;
      $page_url = get_wikipedia_url ( 'commons' , $i , "" , 'wikimedia' ) ;
      $file_url = get_image_url ( 'commons' , $i2 , 'wikimedia' ) ;

      if ( isset ( $params['use_article_as_description'] ) ) $thumb_desc = "$article." ;
      else $thumb_desc = "$image_noext." ;
      if ( $params['default_thumbnail_size'] != '' ) $thumb_desc = $params['default_thumbnail_size'] . "px|" . $thumb_desc ;

      $thumb_text = "<tt>[[$i|thumb|$thumb_desc]]</tt>" ;
    } else {
    
    // http://wikitravel.org/upload/shared/thumb/c/cd/Karte_ubahn_berlin.png/746px-Karte_ubahn_berlin.png
    // http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Dresden-Garnisionskirche-gp.jpg/796px-Dresden-Garnisionskirche-gp.jpg
    
      $thumb_url = 'http://wikitravel.org/favicon.ico' ; // HACK FIXME
      $page_url = 'http://wikitravel.org/shared/Image:' . urlencode ( $i2 ) ;
      $file_url = get_image_url ( 'commons' , $i2 , 'wikimedia' ) ;
      $file_url = str_replace ( 'upload.wikimedia.org/wikimedia/commons' , 'wikitravel.org/upload/shared' , $file_url ) ;
      $thumb_text = '' ;
    }

	if ( $api_output ) {
		$ret[] = array (
			'n' => 'image' ,
			'a' => array (
				'name' => apitext ( $i2 ) ,
				'location' => 'wikitravel' ,
				'freeness' => 'wikitravel' ,
      			'file_url' => apiurl ( $file_url ) ,
				'thumb_url' => apiurl ( $thumb_url ) ,
				'desc_url' => apiurl ( $page_url ) ,
			)
		) ;
	} else {
		$ret[] = '<tr><td class="' . $class . '" valign="top">' ;
		$ret[] = "<a target='_blank' href=\"$page_url\"><img src=\"$thumb_url\" border='0' /></a>" ;
		$ret[] = '</td><td class="' . $class . '" valign="top" width="100%">' ;
		$ret[] = "<b>$i2</b>" ;
		if ( $thumb_text != '' ) $ret[] = "<br/>$thumb_text" ;
		$ret[] = "</td></tr>" ;
	}
  }

  if ( isset ( $params['commonsense'] ) and $is_wikimedia and !$api_output ) {
  	$keywords = trim ( str_replace('_',' ',"$article $redirect_from") ) ;
    $cs = real_common_sense ( 'en' , '' , explode ( ' ' , $keywords ) , array ( 'CATEGORIES' , 'GALLERIES' ) ) ;
    $categories = isset ( $cs['CATEGORIES'] ) ? $cs['CATEGORIES'] : array() ;
    $galleries = isset ( $cs['GALLERIES'] ) ? $cs['GALLERIES'] : array() ;
    if ( count ( $categories ) + count ( $galleries ) > 0 ) {
      $ret[] = "<tr><td colspan='2' class='$class'>" ;

      if ( count ( $categories ) > 0 ) {
        $ret[] = "Try the following categories on Commons : " ;
        $a = array () ;
        foreach ( $categories AS $c ) {
          $url = get_wikipedia_url ( 'commons' , "Category:$c" , '' , 'wikimedia' ) ;
          $a[] = "<a target='_blank' href=\"$url\">$c</a>" ;
        }
        $ret[] = implode ( ", " , $a ) ;
        $ret[] = "<br/>" ;
      }
      
      if ( count ( $galleries ) > 0 ) {
        $ret[] = "Try the following galleries on Commons : " ;
        $a = array () ;
        foreach ( $galleries AS $c ) {
          $url = get_wikipedia_url ( 'commons' , $c , '' , 'wikimedia' ) ;
          $a[] = "<a target='_blank' href=\"$url\">$c</a>" ;
        }
        $ret[] = implode ( ", " , $a ) ;
        $ret[] = "<br/>" ;
      }
      
      $ret[] = "</td></tr>" ;
    }
  }

  $urltitle = urlencode ( $article ) ;
  $search_link = str_replace ( '$1' , $urltitle , $theheading ) ;
  
  if ( $api_output ) {
  	return array (
  		'n' => 'set' ,
  		'*' => $ret ,
  		'a' => array ( 'type' => $is_wikimedia ? 'commons' : 'wikitravel' ) 
  	) ;
  }
  
	$ret = implode ( '' , $ret ) ;
	return results_section ( $ret , $thesource , 2 , $search_link ) ;
}



#_____________________________________________________________________________________________________________
# EVERYSTOCKPHOTO

function find_images_everystockphoto ( $article ) {
  global $params , $imagetypes , $api_output ;
  cleanup_article_name ( $article ) ;
  // http://www.everystockphoto.com/search.php?initialSearch=true&sField=bristol&x=0&y=0&sBool=and&sSource=&sLicense=&sPortrait=on&sLandscape=on&sSquare=on&sMinW=&sMinH=&sDispRes=on&sDispLic=on&sDispSrc=on&sDispRtg=on
//  $url = "http://everystockphoto.com/results.php?seed=" . urlencode ( $article ) ;
  $url = "http://www.everystockphoto.com/search.php?initialSearch=true&sField=" . urlencode ( $article ) ;
  $search_link = " (search <a target='_blank' href=\"$url\">everystockphoto</a> manually)" ;
  $html = file_get_contents ( $url ) ;
  $thumbs = explode ( '<div class="thumb">' , $html ) ;
  array_shift ( $thumbs ) ; # Not needed
  
  $ret = "" ;
  $apiret = array ( 'n' => 'set' , '*' => array() , 'a' => array ( 'type' => 'everystockphoto' ) ) ;
  $count = 0 ;
  foreach ( $thumbs AS $t ) {
  	$t = array_shift ( explode ( '</a>' , $t , 2 ) ) ;
  	$name = htmlspecialchars_decode ( get_text_between ( $t , ' title="' , '"' ) ) ;
  	$text = htmlspecialchars_decode ( get_text_between ( $t , ' alt="' , '"' ) ) ;
  	$thumb_url = get_text_between ( $t , ' src="' , '"' ) ;
  	$desc_url = "http://everystockphoto.com/" . get_text_between ( $t , ' href="' , '"' ) ;
  	$file_url = 'http://www.everystockphoto.com/gotoImage.php?imageId=' . array_pop ( explode ( '?imageId=' , $desc_url , 2 ) ) ;
  	$class = 'll_unknown' ;

	if ( $api_output ) {
		$apiret['*'][] = array ( 'n' => 'image' , 'a' => array (
			'name' => apitext ( $name ) ,
			'text' => apitext ( $text ) ,
			'freeness' => 'unknown' ,
			'thumb_url' => apiurl ( $thumb_url ) ,
			'desc_url' => apiurl ( $desc_url ) ,
			'file_url' => apiurl ( $file_url ) ,
		) ) ;
	} else {
		$ret .= "<tr>" ;
		$ret .= "<td class='$class'><a href='$desc_url' target='_blank'><img src='$thumb_url' border=0 /></a></td>" ;
		$ret .= "<td valign='top' class='$class' width='100%'>$name<br/>$text</td>" ;
		$ret .= "</tr>" ;
	}

  	$count++ ;
  	if ( $count >= $params['esp_max'] ) break ;
  }  
  
  if ( $api_output ) return $apiret ;
  return results_section ( $ret , "everystockphoto.com" , 2 , $search_link ) ;
}


#_____________________________________________________________________________________________________________
# GEOGRAPH.ORG

function get_geograph_button ( $url ) {
	global $tusc_user , $tusc_password ;
	$ret = '' ;
    $ret .= "<form target='_blank' action='geograph_org2commons.php' method='post' style='display:inline;margin:0'>" ;
//    $ret .= "Single-click upload to Commons as " ;
//    $ret .= "<input type='text' size='50' name='new_name' value=\"" . ansi2ascii ( $ft ) . ".jpg\" />" ;
    $ret .= "<input type='hidden' name='flickr_url' value='$url' />" ;
//    $ret .= "<input type='hidden' name='gotoeditpage' value='1' />" ;
    $ret .= "<input type='hidden' name='tusc_user' value='$tusc_user' />" ;
    $ret .= "<input type='hidden' name='tusc_password' value='$tusc_password' />" ;
    $ret .= "<input type='submit' name='doit' value='Directly upload this image to Commons using TUSC' />" ;
    $ret .= "</form>" ;
    return $ret ;
}

function find_images_geograph ( $article ) {
	global $tusc_user , $tusc_password ;
  global $params , $imagetypes , $api_output ;
  $apiret = array ( 'n' => 'set' , 'a' => array('type'=>'geograph.org') , '*' => array() ) ;
  $ret = '' ;
  $cnt = 0 ;
  cleanup_article_name ( $article ) ;
//  $base_query_url = 'http://www.nearby.org.uk/geograph/find-syndicator.php?format=GeoPhotoRSS&query=' ;
  $base_query_url = 'http://www.geograph.org.uk/syndicator.php?source=fist&format=GeoPhotoRSS&q=' ;
  $query_url = $base_query_url ;
  $query_url .= urlencode ( $article ) ;
  $rss = file_get_contents ( $query_url ) ;
  $items = explode ( '<item ' , $rss ) ;
  
  if ( count ( $items ) == 1 ) { # No results
	  $query_url = $base_query_url ;
	  $query_url .= urlencode ( $article ) ;
	  $rss = file_get_contents ( $query_url ) ;
	  $items = explode ( '<item ' , $rss ) ;
  }
  
  array_shift ( $items ) ;
  foreach ( $items AS $i ) {
  	$thumb = get_text_between ( $i , '<photo:thumbnail>' , '</photo:thumbnail>' ) ;
  	$url = get_text_between ( $i , '<link>' , '</link>' ) ;
  	$desc = get_text_between ( $i , '<description>' , '</description>' ) ;
  	$source = get_text_between ( $i , '<dc:source>' , '</dc:source>' ) ;
  	$author = get_text_between ( $i , '<dc:creator>' , '</dc:creator>' ) ;
  	$title = get_text_between ( $i , '<title>' , '</title>' ) ;
  	$date = get_text_between ( $i , '<dc:date>' , '</dc:date>' ) ;
  	$coord = get_text_between ( $i , '<georss:point>' , '</georss:point>' ) ;
  	
  	$source_id = array_pop ( explode ( '/' , $url ) ) ;
  	$coor = str_replace ( ' ' , '|' , $coord ) ;
  	if ( $coor != '' ) $coor = "{{Location dec|$coor}}" ;
  	
  	if ( $api_output ) {
  		$full_page = file_get_contents ( $url ) ;
		$image_url = $full_page ;
		$image_url = array_pop ( explode ( 'mainphoto' , $image_url , 2 ) ) ;
		$image_url = array_shift ( explode ( '/>' , $image_url , 2 ) ) ;
		$image_url = array_pop ( explode ( 'src="' , $image_url , 2 ) ) ;
		$image_url = array_shift ( explode ( '"' , $image_url , 2 ) ) ;

		$ar = array ( 'n' => 'image' , 'a' => array (
			'source' => apitext ( $source ) ,
			'author' => apitext ( $author ) ,
			'name' => apitext ( $title ) ,
			'coordinates' => apitext ( $coord ) ,
			'date' => apitext ( $date ) ,
			'thumb_url' => apiurl ( $thumb ) ,
			'desc_url' => apiurl ( $url ) ,
			'file_url' => apiurl ( $image_url ) ,
			'text' => apitext ( $desc ) ,
			'freeness' => 'CC-BY-SA-2.0' ,
		) ) ;
		$apiret['*'][] = $ar ;
	}
  	
  	$ret .= '<tr>' ;
  	$ret .= "<td class='ll_good'>
  	<a target='_blank' href=\"$url\"><img border=0 src=\"$thumb\"/></a>
  	</td><td valign='top' class='ll_good' width='50%'>
  	<b>$title</b><br/>
  	<i><small>$desc</small></i><br/>
  	Taken by <a href='$source' target='_blank'>$author</a> on $date at $coord.<br/>
  	Released under <a href='http://creativecommons.org/licenses/by-sa/2.0/' target='_blank'>CC-BY-SA 2.0</a>." ;
  	if ( $tusc_user != '' && $tusc_password != '' ) $ret .= get_geograph_button ( $url ) ;
  	$ret .= "
  	</td><td valign='top' class='ll_good' width='50%'>
  	<textarea style='font-size:80%;width:100%' cols='50' rows='6'>
{{Information
|Description=$desc
|Source=From [$url geograph.co.uk]
|Date=$date
|Author=[$source $author]
|Permission=Creative Commons Attribution Share-alike license 2.0
}}
$coor

{{geograph|$source_id|$author}}
</textarea>
  	</td>" ;
  	$ret .= '</tr>' ;
  	$cnt++ ;
  	if ( $cnt >= $params['geograph_max'] ) break ;
  }

	if ( $api_output ) return $apiret ;
  return results_section ( $ret , "<a href='http://geograph.org.uk' target='_blank'>geograph.org.uk</a>" , 3 ) ;
}



#_____________________________________________________________________________________________________________
# agenciabrasil.gov.br

function find_images_agenciabrasil ( $article ) {
  global $params , $imagetypes , $api_output ;
  $apiret = array ( 'n' => 'set' , 'a' => array ( 'type' => 'agenciabrasil' ) , '*' => array() ) ;
  $ret = '' ;
  $cnt = 0 ;
  cleanup_article_name ( $article ) ;

  $base_query_url = 'http://www.agenciabrasil.gov.br/imagens/search?portal_type=ATPhoto&SearchableText=' ;
  $query_url = $base_query_url ;
  $query_url .= urlencode ( $article ) ;
  $cont = file_get_contents ( $query_url ) ;
  
  $key1 = ' nowrap valign="top">' ;
  $key2 = 'http://www.agenciabrasil.gov.br/media/' ;
  $key3 = 'http://stream.agenciabrasil.gov.br/media/imagens' ;
  $items = explode ( $key1 , $cont ) ;
  array_shift ( $items ) ; // Pre-table
  foreach ( $items AS $i ) {
  	$thumb = array_shift ( explode ( '</tr>' , $i ) ) ;
  	$thumb = array_pop ( explode ( '<img' , $thumb ) ) ;
  	$thumb = get_text_between ( $thumb , '"' , '"' ) ;
  	$url = $key2 . get_text_between ( $i , $key2 , '"' ) ;
  	$i2 = explode ( ' class="lista1"' , $i ) ;
  	$i2 = array_pop ( $i2 ) ;
  	$desc = get_text_between ( $i2 , '/view">' , '</a>' ) ;

  	$i2 = explode ( ' class="lista2"' , $i ) ;
  	$i2 = array_pop ( $i2 ) ;
  	$date = get_text_between ( $i2 , '<i>' , '</i>' ) ;

  	$author = 'Brasilian government' ;
  	
  	if ( $api_output ) {
  		$file_url = str_replace ( 'http://www.' , 'http://stream.' , $url ) ;
  		$file_url = str_replace ( '/view' , '' , $file_url ) ;
  		$name = explode ( '/' , $url ) ;
  		array_pop ( $name ) ;
  		$name = array_pop ( $name ) ;
  		$apiret['*'][] = array ( 'n' => 'image' , 'a' => array (
  			'name' => apitext ( $name ) ,
  			'desc_url' => apiurl ( $url ) ,
  			'thumb_url' => apiurl ( $thumb ) ,
  			'file_url' => apiurl ( $file_url ) ,
  			'text' => apitext ( $desc ) ,
  			'freeness' => 'CC-BY-2.5' ,
  		) ) ;
  	}

  	$ret .= '<tr>' ;
  	$ret .= "<td class='ll_good'>
  	<a target='_blank' href=\"$url\"><img border=0 src=\"$thumb\"/></a>
  	</td><td valign='top' class='ll_good' width='50%'>
  	<i><small>$desc</small></i><br/>
  	Taken by <a href='http://www.agenciabrasil.gov.br' target='_blank'>$author</a> on $date.<br/>
  	Released under <a href='http://creativecommons.org/licenses/by/2.5/' target='_blank'>CC-BY 2.5</a>.
  	</td><td valign='top' class='ll_good' width='50%'>
  	<textarea style='font-size:80%;width:100%' cols='50' rows='5'>
{{Information
|Description=$desc
|Source=From [$url agenciabrasil.gov.br]
|Date=$date
|Author=$author
|Permission=Creative Commons Attribution license 2.5 (Brasil)
}}</textarea>
  	</td>" ;
  	$ret .= '</tr>' ;
  	$cnt++ ;
  	if ( $cnt >= $params['ab_max'] ) break ;
  }

	if ( $api_output ) return $apiret ;
  return results_section ( $ret , "<a href='http://www.agenciabrasil.gov.br' target='_blank'>agenciabrasil.gov.br</a>" , 3 ) ;
}



#_____________________________________________________________________________________________________________
# THUMBNAILROW

function get_thumbnail_row ( $images ) {
  global $language , $project , $thumbsize , $api_output ;
  $images_intl = array () ;
  foreach ( $images AS $k => $i ) {
  	$images_intl[$k] = array_pop ( explode ( ':' , $i , 2 ) ) ;
  }
  $local_images = db_local_images ( $language , $images_intl ) ;
  $apiret = array() ;
  $ret = '<tr>' ;
  $divsize = $thumbsize + 5 ;
  $divsize = $divsize . "px" ;
  $count = 0 ;
  foreach ( $images AS $i ) {
  	$i_short = array_pop ( explode ( ':' , $i , 2 ) ) ;
  	$is_local = in_array ( get_db_safe ( $i_short ) , $local_images ) ;
  	$lang = $is_local ? $language : 'commons' ;
  	$proj = $is_local ? $project : 'wikimedia' ;
  	
  	$data = db_get_image_data ( $i_short , $lang , $proj ) ;
  	if ( !isset ( $data->img_width ) or $data->img_width <= 0 ) $nw = $thumbsize ;
  	else if ( $data->img_width > $data->img_height ) $nw = $thumbsize ;
  	else $nw = floor ( $data->img_width * $thumbsize / $data->img_height ) ;
  	
  	$templates = db_get_used_templates ( $lang , $i_short , 6 ) ;
    $type = get_image_type ( $lang , $i_short , $templates ) ;
    $class = 'unknown' ;
    if ( $lang == 'commons' ) $class = 'commons' ;
    if ( $type == 'good' or $type == 'bad' ) $class = $type ;

  	
  	$thumb_url = get_thumbnail_url ( $lang , $i_short , $nw , $proj  ) ;
  	$image_url = get_wikipedia_url ( $lang , "Image:$i_short" , '' , $proj ) ;
  	
  	if ( $api_output ) {
  		$apiret[] = array ( 
  			'n' => 'image' ,
  			'a' => array (
  				'name' => htmlspecialchars($i_short) , 
  				'location' => "$lang.$project" ,
  				'freeness' => $type ,
      			'file_url' => apiurl ( get_image_url ( $lang , $i_short , $project ) ) ,
  				'desc_url' => apiurl($image_url) , 
  				'thumb_url' => apiurl($thumb_url) ,
  			) ,
  		) ;
	}
  	
  	
  	$ret .= "<td width='$divsize' height='$divsize' align='center' class='ll_$class'>" ;
  	$ret .= "<a target='_blank' href=\"$image_url\"><img border=0 src=\"$thumb_url\" /></a><br/><b>$i_short</b></td>\n" ;
  	$count++ ;
  	if ( $count % 6 == 0 and $count < count ( $images ) ) $ret .= "</tr><tr>" ;
  }
  $ret .= '</tr>' ;
  if ( $api_output ) return array ( 'n' => 'set' , '*' => $apiret , 'a' => array ( 'type' => 'already_in_article' ) ) ;
  return results_section ( $ret , "Images already in article" , count ( $images ) ) ;
}


#______________________________________________________
# Find categories to skip

function is_article_in_skip_category ( $article ) {
	global $skip_categories , $language , $project , $wq ;
	
	# Load categories
	if ( count ( $skip_categories ) == 0 ) {
		$url = get_wikipedia_url ( "meta" , "FIST/Ignored_categories" , "raw" , "wikipedia" ) ; ;
		$url = str_replace ( '%2F' , '/' , $url ) ;
		$text = file_get_contents ( $url ) ;
		$lines = explode ( "\n" , $text ) ;
		$in_section = false ;
		foreach ( $lines AS $l ) {
			if ( substr ( $l , 0 , 1 ) == '=' ) { # Heading
				if ( $in_section ) break ; # New section, end this
				$l = strtolower ( trim ( str_replace ( '=' , '' , $l ) ) ) ;
				$in_section = $l == "$language.$project" ;
			} else {
				if ( !$in_section ) continue ;
				$l = str_replace ( '_' , ' ' , $l ) ;
				$l = trim ( substr ( $l , 1 ) ) ;
				if ( $l == '' ) continue ;
				$skip_categories[] = $l ;
			}
		}
	}
	
	# Fetch article categories
	$cats = $wq->get_categories ( $article ) ;
	foreach ( $cats AS $c ) {
		$c = trim ( str_replace ( '_' , ' ' , $c ) ) ;
		if ( in_array ( $c , $skip_categories ) ) return true ;
	}
	
	return false ;
}


#______________________________________________________
# Main routine for finding images

function find_images ( $article ) {
	global $sources , $params , $language , $project , $wq , $note ;
	global $language_links , $wq , $redirect_from , $api_output ;
	
	# Check and follow redirect
	if ( db_is_redirect ( $article , $language , $project , 0 ) ) {
		$redirect_from = $article ;
		$article = $wq->get_redirect_target ( $article ) ;
	} else $redirect_from = '' ;
	
	# Check for "skip categories"
	if ( isset ( $params['skip_articles_in_categories'] ) ) {
		if ( is_article_in_skip_category ( $article ) ) return ;
	}
	
	# Prep
	$note = '' ;
	$out = array () ;
	$article = str_replace ( '_' , ' ' , $article ) ;
	$uarticle = str_replace ( ' ' , '_' , $article ) ;
	$skip_no_candidates = isset ( $params['skip_no_candidate'] ) ;
	
	# Check article
	$existing_images = $wq->get_images_on_page ( $article , 1 ) ;
	$noi = count ( $existing_images , 1 ) ; # !!!!
	if ( $params['forarticles'] == 'all' ) $max = $noi+1 ;
	else if ( $params['forarticles'] == 'noimage' ) $max = 1 ;
	else $max = $params['lessthan_images'] ;
	
	$images_message = $noi > 1 ? 'images' : 'image' ;
	if ( $noi == 0 ) $images_message = "no images" ;
	else $images_message = "$noi $images_message" ;
	if ( $noi >= $max and $skip_no_candidates ) return ; # Don't show
  
  
  	if ( $api_output ) {
  	} else {
	  	if ( $noi >= $max ) $out[] = "<font color='green'><i>Not searched (already has $images_message).</i></font>" ;
	  	$images_message = "<br/>(has $images_message)" ;
	}
	

	# Run through the searches
	if ( count ( $out ) == 0 ) {
		unset ( $out_wp );
		unset ( $out_co );
		
    	if ( isset ( $sources['languagelinks'] ) ) $out_wp = find_images_languagelinks ( $article ) ;
    	else unset ( $language_links ) ;

	    if ( isset ( $sources['commons'] ) ) $out_co = find_images_commons ( $article ) ;
	    
	    if ( isset ( $out_co ) ) $out[] = $out_co ;
	    if ( isset ( $out_wp ) ) $out[] = $out_wp ;
	    
	    
	    if ( isset ( $sources['wts'] ) ) $out[] = find_images_wikitravel ( $article ) ;
	    
    	if ( isset ( $params['jpeg'] ) ) { # The rest only have jpegs, no need to search them if that's not wanted
			if ( isset ( $sources['flickr'] ) ) $out[] = find_images_flickr ( $article ) ;
			if ( isset ( $sources['gimp'] ) ) $out[] = find_images_gimp ( $article ) ;
			if ( isset ( $sources['everystockphoto'] ) ) $out[] = find_images_everystockphoto ( $article ) ;
			if ( isset ( $sources['geograph'] ) ) $out[] = find_images_geograph ( $article ) ;
			if ( isset ( $sources['agenciabrasil'] ) ) $out[] = find_images_agenciabrasil ( $article ) ;
		}
	}
	
	if ( isset ( $params['show_existing_images'] ) and $noi > 0 ) $out[] = get_thumbnail_row ( $existing_images ) ;

	# Nothing found, and skip articles when nothing found?
	if ( count ( $out ) == 0 and $skip_no_candidates ) return ; # No results for article
	
	# Output
	if ( !$api_output ) {
		$out = implode ( '' , $out ) ;
		if ( $out == '' ) $out = "<font color='red'><i>No images found!</i></font>" ;
		if ( $note != '' ) $out .= "<br/>$note." ;
		$out = str_replace ( "</table><br/>" , "</table>" , $out ) ;
	}
	
	$v_url = get_wikipedia_url ( $language , $article , '' , $project ) ;
	$e_url = get_wikipedia_url ( $language , $article , 'edit' , $project ) ;
	
	if ( $api_output ) {
		return array (
			'n' => 'article' ,
			'*' => $out ,
			'a' => array ( 'title' => htmlspecialchars ( $article ) , 'view_url' => apiurl($v_url) , 'edit_url' => apiurl($e_url) , 'has_images' => $noi )
		) ;
	} else {
		print "<tr>
		<th class='main_th' valign='top' width='150px'>
		<a target='_blank' href=\"$v_url\">$article</a><br/>
		[<a target='_blank' href=\"$e_url\">edit</a>]$images_message</th>
		<td style='border-bottom:2px solid black' valign='top'>
		$out
		</td></tr>\n" ;
	}
}

function get_search_url() {
  global $language , $project , $data , $datatype , $sources , $params ;
  $url = 'http://tools.wikimedia.de/~magnus/fist.php?doit=1' ;
  $url .= '&language=' . urlencode ( $language ) ;
  $url .= '&project=' . urlencode ( $project ) ;
  $url .= '&data=' . urlencode ( $data ) ;
  $url .= '&datatype=' . urlencode ( $datatype ) ;
  
  foreach ( $params AS $k => $v )
    $url .= '&params[' . urlencode ( $k ) . ']=' . urlencode ( $v ) ;
  foreach ( $sources AS $k => $v )
    $url .= '&sources[' . urlencode ( $k ) . ']=' . urlencode ( $v ) ;

  return $url ;
}

function apiurl ( $url ) {
	return htmlentities ( $url ) ;
}

function apitext ( $t ) {
	return htmlspecialchars ( $t ) ;
}

function api2xml ( $d ) {
	if ( is_array ( $d ) ) {
		if ( count ( $d ) == 0 ) return '' ;
		$ret = '<' . $d['n'] ;
		
		if ( isset ( $d['a'] ) ) {
			foreach ( $d['a'] AS $k => $v ) {
				$ret .= ' ' . $k . '="' . $v . '"' ;
			}
		}
		
		if ( isset ( $d['*'] ) ) {
			$ret .= '>' ;
			if ( is_array ( $d['*'] ) ) {
				foreach ( $d['*'] AS $sub ) $ret .= api2xml ( $sub ) ;
			} else $ret .= $d['*'] ;
			$ret .= '</' . $d['n'] . '>' ;
		} else $ret .= ' />' ;
		return $ret ;
	} else return $d ;
}

#____________________________________________________________________________________________________________

# Read request
$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$data = trim ( get_request ( 'data' , '' ) ) ;
$datatype = get_request ( 'datatype' , 'categories' ) ;
$sources = get_request ( 'sources' , array() ) ;
$params = get_request ( 'params' , array() ) ;

$tusc_user = get_request ( 'tusc_user' , '' ) ;
$tusc_password = get_request ( 'tusc_password' , '' ) ;

$doit = isset ( $_REQUEST['doit'] ) ;
$test = isset ( $_REQUEST['test'] ) ;
$skip_categories = array () ;

$image_ns_name = 'File' ;
if ( $language == 'de' ) $image_ns_name = 'Datei' ;

$wq = new WikiQuery ( $language , $project ) ;

# Init for new form
if ( !$doit and count ( $sources ) == 0 ) {
  $sources = array ( 
    'flickr' => 1,
    'languagelinks' => 1,
    'commons' => 1,
  ) ;
  $params = array (
    'catdepth' => 0,
    'flickr_max' => 5,
    'll_max' => 5,
    'gimp_max' => 5,
    'commons_max' => 5,
    'wts_max' => 5,
    'esp_max' => 5,
    'ab_max' => 5,
    'geograph_max' => 5,
    'random' => 50,
    'lessthan_images' => 3,
    'default_thumbnail_size' => '',
    'min_width' => 80,
    'min_height' => 80,
    'include_flickr_id' => 1,
	'esp_skip_flickr' => 1,
    'jpeg' => 1,
    'gif' => 1,
#    'tiff' => 1,
    'png' => 1,
    'svg' => 1,
    'ogg' => 1,
  ) ;
}

# Some implicated rules
if ( !isset($params['default_thumbnail_size']) ) $params['default_thumbnail_size'] = 200 ;
if ( !isset($params['wts_max']) ) $params['wts_max'] = 5 ;
if ( !isset($params['geograph_max']) ) $params['geograph_max'] = 5 ;
if ( !isset($params['forarticles']) ) $params['forarticles'] = 'noimage' ;
if ( !isset($params['esp_max']) ) $params['esp_max'] = 5 ;
if ( !isset($params['ab_max']) ) $params['ab_max'] = 5 ;
if ( !isset($params['output_format']) ) $params['output_format'] = 'out_html' ;
if ( $datatype == 'replaceimages' and $params['forarticles'] == 'noimage' ) $params['forarticles'] = 'all' ;
$api_output = $params['output_format'] != 'out_html' ;

$api_data = array (
	'n' => 'result' ,
	'a' => array () ,
	'*' => array ()
) ;
$use_api = false ;
$thumbsize = 75 ;
$note = '' ;
$flickr_key = get_flickr_key() ;
if ( $doit ) db_increase_usage_counter ( 'fist' ) ;

if ( $doit and $api_output ) {
	$search_url = apiurl ( get_search_url() ) ;
	$api_data['a']['run_url'] = $search_url ;
} else {
	print_form () ;
}

$is_on_toolserver = false ;

if ( $doit ) {
	$namespaces = $wq->get_namespaces() ;

  $imagetypes = array () ;
  if ( isset ( $params['jpeg'] ) ) { $imagetypes['jpg'] = 1 ; $imagetypes['jpeg'] = 1 ; }
  if ( isset ( $params['tiff'] ) ) { $imagetypes['tif'] = 1 ; $imagetypes['tiff'] = 1 ; }
  if ( isset ( $params['png'] ) ) $imagetypes['png'] = 1 ;
  if ( isset ( $params['gif'] ) ) $imagetypes['gif'] = 1 ;
  if ( isset ( $params['svg'] ) ) $imagetypes['svg'] = 1 ;
  if ( isset ( $params['ogg'] ) ) $imagetypes['ogg'] = 1 ;

  $check_exists = false ; # TODO : make this a parameter

	if ( $api_output ) {
		get_article_list () ;
		foreach ( $articles AS $article ) {
			$api_data['*'][] = find_images ( $article ) ;
		}
	} else {
		print "Copy the URL of <a href=\"" . get_search_url() . "\">this link</a> to get a pre-filled search for the values you have entered above.<br/>" ;
		print "Retrieving article list ... " ; myflush() ;
		get_article_list () ;
		print count ( $articles ) . " articles found. Searching for images will take time, please be patient...<br/>" ;
		myflush() ;
		
		print "<table style='border:1px solid black' cellpadding='2px' cellspacing='0px' width='100%'>" ;
		foreach ( $articles AS $article ) {
			find_images ( $article ) ;
		}
		
		print "</table>" ;
		print "<hr/>All done!" ;
	}
}


if ( $doit and $api_output ) {
	if ( $params['output_format'] == 'out_json' ) {
		header('Content-type: application/x-json; charset=utf-8');
		print json_encode ( $api_data ) ;
	} else { // XML; default API output
		header('Content-type: text/xml; charset=utf-8');
		print '<?xml version="1.0" encoding="UTF-8" ?>' ;
		print api2xml ( $api_data ) ;
	}
} else {
	print "</div></body></html>" ;
}
myflush() ;

?>
