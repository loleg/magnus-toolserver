<?PHP
error_reporting ( E_ALL ) ;

include_once ( 'queryclass.php' ) ;

function load_dnb_from_html ( $pnd ) {
	$ret = array () ;
	$html = file_get_contents ( 'marx.html' ) ; # TODO load from url
	$t = explode ( '<table>' , $html , 2 ) ;
	$t = array_pop ( $t ) ;
	$t = explode ( '</table>' , $t ) ;
	
	$tmp = array_shift ( $t ) ;
	preg_match_all ( '/<td>(.*?)<\/td>\s*<td>(.*?)<\/td>/' , $tmp , $d1 ) ;
	$ret['head'] = array () ;
	foreach ( $d1[1] AS $k => $v ) {
		$ret['head'][$v][] = $d1[2][$k] ;
	}
	
	$ret['ikt'] = array () ;
	array_shift ( $t ) ;
	while ( count ( $t ) > 0 ) {
		$d2 = array_shift ( $t ) ;
		if ( preg_match ( '/<img/' , $d2 ) ) break ;
		$d2 = array_pop ( explode ( '<table' , $d2 , 2 ) ) ;
		$d2 = explode ( '<tr>' , $d2 ) ;
		
		$n = array () ;
		
		foreach ( $d2 AS $d ) {
			preg_match_all ( '/<td.*?>(.*?)<\/td>/' , $d , $row ) ;
			$row = $row[1] ;
			$n[] = $row ;
		}
		
		if ( count ( $n ) < 3 ) break ;
		if ( count ( $n[0] ) != 0 ) break ;
		if ( count ( $n[1] ) != 1 ) break ;
		array_shift ( $n ) ;
		array_shift ( $n ) ;
		
		# TODO fix <b>
		# TODO fix &nbsp;
		$ret['ikt'][] = $n ;
	}

//	print "<pre>" ; print_r ( $ret ) ; print "</pre>" ;
	return $ret ;
}

function xml2list ( $xml ) {
	$ret = array () ;
	$xml = explode ( '<' , $xml ) ;
	foreach ( $xml AS $x ) {
		$ret[] = '<' . $x ;
	}
	return $ret ;
}

function wiki2plaintext ( $wiki ) {
	$skip = 1 ;
	if ( stristr ( $wiki , '{{'  ) ) $skip = 0 ;
	else if ( stristr ( $wiki , '[['  ) ) $skip = 0 ;
	else if ( stristr ( $wiki , "''"  ) ) $skip = 0 ;
	if ( $skip ) return $wiki ; // Not necessary to call wiki2xml

	$url = 'http://toolserver.org/~magnus/wiki2xml/w2x.php?whatsthis=wikitext&doit=1&use_templates=none&output_format=text&site='.urlencode('de.wikipedia.org/w').'&text=' . urlencode ( $wiki ) ;
	$txt = file_get_contents ( $url ) ;
	return $txt ;
}

function fix_template_structure ( $tl ) {
	$ret = array () ;
	$ret['name'] = '' ;
	$ret['*'] = array () ;
	$argcnt = 0 ;
	foreach ( $tl AS $x ) {
		if ( $x == '<space/>' ) {
		} else if ( substr ( $x , 0 , 2 ) == '</' ) {
		} else {
			$x = explode ( '>' , $x , 2 ) ;
			if ( substr ( $x[0] , 0 , 4 ) == '<arg' ) {
				$k = explode ( 'name=' , $x[0] , 2 ) ;
				if ( count ( $k ) == 2 ) {
					$k = array_pop ( $k ) ;
					$k = trim ( str_replace ( '"' , '' , $k ) ) ;
				} else {
					$argcnt++ ;
					$k = $argcnt ;
				}
				$v = wiki2plaintext ( trim ( $x[1] ) ) ;
				$ret['*'][$k] = $v ;
			} else { // target
				$ret['name'] = ucfirst ( trim ( $x[1] ) ) ;
			}
		}
	}
	return $ret ;
}

function extract_templates_from_xml_list ( $xl ) {
	$ret = array () ;
	$cnt = 0 ;
	$tmp = array () ;
	foreach ( $xl AS $x ) {
		if ( substr ( $x , 0 , 9 ) == '<template' ) {
			$cnt++ ;
			if ( $cnt > 1 ) $tmp[] = $x ;
		} else if ( substr ( $x , 0 , 10 ) == '</template' ) {
			$cnt-- ;
			if ( $cnt == 0 ) {
				$ret[] = fix_template_structure ( $tmp ) ;
				$tmp = array () ;
			} else $tmp[] = $x ;
		} else {
			if ( $cnt > 0 ) $tmp[] = $x ;
		}
	}
	return $ret ;
}

function fix_link_structure ( $xl ) {
	// First entry is target, ignore the rest
	$ret = $xl[0] ;
	$ret = array_pop ( explode ( '>' , $ret , 2 ) ) ;
	return trim ( $ret ) ;
}

function add_link ( $l , &$o ) {
	$l = trim ( $l ) ;
	if ( $l == '' ) return ;
	$ll = strtolower ( $l ) ;
	$g = 'link' ;
	if ( substr ( $ll , 0 , 6 ) == 'datei:' ) { $g = 'file' ; $l = substr ( $l , 6 ) ; }
	else if ( substr ( $ll , 0 , 6 ) == 'image:' ) { $g = 'file' ; $l = substr ( $l , 6 ) ; }
	else if ( substr ( $ll , 0 , 5 ) == 'file:' ) { $g = 'file' ; $l = substr ( $l , 5 ) ; }
	else if ( substr ( $ll , 0 , 5 ) == 'bild:' ) { $g = 'file' ; $l = substr ( $l , 5 ) ; }
	else if ( substr ( $ll , 0 , 9 ) == 'category:' ) { $g = 'cat' ; $l = substr ( $l , 9 ) ; }
	else if ( substr ( $ll , 0 , 10 ) == 'kategorie:' ) { $g = 'cat' ; $l = substr ( $l , 10 ) ; }
	else if ( substr ( $ll , 0 , 7 ) == 'simple:' ) { $g = 'lang' ; }
	else { // TODO language link
		$n = explode ( ':' , $l ) ;
		if ( count ( $n ) == 2 ) {
			$p = trim ( $n[0] ) ;
			$p = explode ( '-' , $p ) ;
			$g = 'lang' ;
			foreach ( $p AS $lp ) {
				if ( strlen ( $lp ) > 4 ) $g = 'link' ;
				else if ( $lp != strtolower ( $lp ) ) $g = 'link' ;
			}
		}
	}
	if ( !isset ( $o[$g] ) ) $o[$g] = array () ;
	$l = trim ( $l ) ;
	if ( $g != 'lang' ) $l = ucfirst ( $l ) ;
	$o[$g][] = $l ;
}

function extract_links_from_xml_list ( $xl ) {
	$ret = array () ;
	$cnt = 0 ;
	$tmp = array () ;
	foreach ( $xl AS $x ) {
		if ( substr ( $x , 0 , 5 ) == '<link' ) {
			$cnt++ ;
			if ( $cnt > 1 ) $tmp[] = $x ;
		} else if ( substr ( $x , 0 , 6 ) == '</link' ) {
			$cnt-- ;
			if ( $cnt == 0 ) {
				$l = fix_link_structure ( $tmp ) ;
				add_link ( $l , $ret ) ;
				$tmp = array () ;
			} else $tmp[] = $x ;
		} else {
			if ( $cnt > 0 ) $tmp[] = $x ;
		}
	}
	return $ret ;
}

function load_from_wikipedia ( $title ) {
	$url = 'http://toolserver.org/~magnus/wiki2xml/w2x.php?whatsthis=articlelist&doit=1&use_templates=none&output_format=xml&site='.urlencode('de.wikipedia.org/w').'&text=' . urlencode ( $title ) ;
	$xml = file_get_contents ( $url ) ;
	$xl = xml2list ( $xml ) ;
	$tl = extract_templates_from_xml_list ( $xl ) ;
	$ln = extract_links_from_xml_list ( $xl ) ;
	
//	print htmlspecialchars ( $xml ) ;
	print "<pre>" ; print_r ( $ln ) ; print "</pre>" ;
}


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "darBNDer.php" ) ;

//$dnb = load_dnb_from_html ( '118578537' ) ;
$wd = load_from_wikipedia ( 'Karl Marx' ) ;

//print "<pre>" ; print_r ( $dnb ) ; print "</pre>" ;

print "</body></html>" ;
