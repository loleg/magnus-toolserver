<?PHP

include "common.php" ;

function prettynum ( $n ) {
	$ret = '' ;
	while ( strlen ( $n ) > 0 ) {
		$ret = ' ' . substr ( $n , -3 , 3 ) . $ret ;
		$n = substr ( $n , 0 , -3 ) ;
	}
	return trim ( $ret ) ;
}

function get_page_creation_date ( $language , $project , $title , $ns = 0 ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = $language . 'wiki_p' ;
	make_db_safe ( $title ) ;
	
	$sql = "SELECT ".get_tool_name()." min(rev_timestamp) AS d from page,revision where rev_page=page_id AND page_namespace=$ns and page_title=\"{$title}\"" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return $ret ;#{ print 'Line 171:' . mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
	while ( $o = mysql_fetch_object ( $res ) ) {
		$d = $o->d ;
		$d = substr($d,0,4)."-".substr($d,4,2)."-".substr($d,6,2)." ".substr($d,8,2).":".substr($d,10,2).":".substr($d,12,2) ;
		return $d ;
	}
	return "UNKNOWN" ;
}


$depth = get_request ( 'depth' , '9' ) ;
$cats = get_request ( 'cats' , "" ) ;
$combination = get_request ( 'combination' , 'subset' ) ;
$autolang = get_request ( 'autolang' , 0 ) ;
$pcd = get_request ( 'page_creation_date' , 0 ) ;

$date = get_request ( 'date' , '2010-03' ) ;
$date = trim ( str_replace ( '-' , '' , $date ) ) ;
$date2 = substr ( $date , 0 , 4 ) . "-" . substr ( $date , 4 , 2 ) ;

$this_url = "http://toolserver.org/~magnus/treeviews.php?depth=$depth&date=$date2&cats=" . urlencode ( $cats ) ;
$this_url .= '&combination=' . $combination ;
$this_url .= '&autolang=' . $autolang ;
if ( $pcd ) $this_url .= "&page_creation_date=1" ;

print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;
print '<script src="./lib/sorttable.js"></script>' ; // http://www.kryogenix.org/code/browser/sorttable/
print '</head><body>' ;
print get_common_header ( "treeviews.php" , "Tree Views" ) ;

$checked_subset = $combination == 'subset' ? 'checked' : '' ;
$checked_union = $combination == 'union' ? 'checked' : '' ;
$checked_autolang = $autolang ? 'checked' : '' ;
$checked_page_creation_date = $pcd ? 'checked' : '' ;

print "<img src='http://farm5.static.flickr.com/4008/4390759754_74ec3111cd_m.jpg' style='float:right' />
<h1>TreeViews</h1>
<div><b><big>Also, try the <a href='http://toolserver.org/~magnus/ts2/treeviews'>new version</a>!</big></b></div><br/>
Examples :
<a href='http://toolserver.org/~magnus/treeviews.php?depth=9&date=2010-03&cats=Indianapolis%2C+Indiana%0D%0APublic+art&doit=1'>Public art in Indianapolis</a> |
<a href='http://toolserver.org/~magnus/treeviews.php?depth=9&date=2010-03&cats=British+Museum%0D%0A-Natural+History+Museum%0D%0A-British+Museum+directors%0D%0A-Employees+of+the+British+Museum%0D%0A-Trustees+of+the+British+Museum&doit=1'>British Museum, without staff and National History Museum</a>
<br/><form method='post' action='./treeviews.php'><table border='1'>
<tr><th>Month</th><td><input type='text' name='date' value='$date2' /></td></tr>
<tr><th>Default depth</th><td><input type='number' name='depth' value='$depth' /></td></tr>
<tr><th>Categories</th><td><textarea name='cats' rows='10' style='width:100%'>$cats</textarea><br/>
<small>
Enter one category per line<br/>
Prefix categories with a language (default \"en:\"); searches will be grouped by language<br/>
Prefix a line with \"-\" to exclude pages in the category tree<br/>
Append \"|DEPTH\" to a category to change the tree depth for that category<br/>
A subset of all category trees per language will be taken
</small>
</td></tr>
<tr><th>Combination</th><td>Show pages in 
<input name='combination' type='radio' value='subset' id='subset' $checked_subset /><label for='subset'>all</label> / 
<input name='combination' type='radio' value='union' id='union' $checked_union /><label for='union'>either</label>
category tree(s)<br/>
<small>\"Either\" can produce a very long list, and take a very long time to finish!</small>
</td></tr>
<tr><th>Auto-lang</th><td><input type='checkbox' name='autolang' id='autolang' value='1' $checked_autolang /><label for='autolang'>Automatically check corresponding categories in other languages as well (can take long!)</label></td></tr>
<tr><th>Creation date</th><td><input type='checkbox' name='page_creation_date' id='page_creation_date' value='1' $checked_page_creation_date /><label for='page_creation_date'>Show page creation date</label></td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Do it!' /></td></tr>
</table></form>" ;

if ( isset ( $_REQUEST['doit'] ) ) {
	print "Links to this query <a href=\"$this_url&doit=1\">with</a> and <a href=\"$this_url\">without</a> auto-start." ;
	myflush() ;

	$lines = explode ( "\n" , $cats ) ;
	$lcats = array () ;
	foreach ( $lines AS $l ) {
		$l = trim ( $l ) ;
		$neg = '' ;
		if ( substr ( $l , 0 , 1 ) == '-' ) {
			$l = trim ( substr ( $l , 1 ) ) ;
			$neg = '-' ;
		}
		if ( $l == '' ) continue ;
		$l = explode ( ':' , $l , 2 ) ;
		if ( count ( $l ) == 2 && strlen ( $l[0] ) > 4 ) $l = array ( implode ( ':' , $l ) ) ;
		$cat = array_pop ( $l ) ;
		if ( count ( $l ) == 0 ) $lang = 'en' ;
		else $lang = $l[0] ;
		$lcats[$lang][$neg.$cat] = $neg.$cat ;
	}
/*
	if ( $autolang == 1 ) {
		$lcats2 = array () ;
		foreach ( $lcats AS $lang => $cs ) {
			$wq = new WikiQuery ( $lang , 'wikipedia' ) ;
			foreach ( $cs AS $c ) {
				if ( '-' == substr ( $c , 0 , 1 ) ) continue ;
				$data = $wq->get_language_links ( 'Category:' . $c ) ;
				foreach ( $data AS $nl => $nc ) {
					$nc = explode ( ':' , $nc , 2 ) ;
					if ( 2 != count ( $nc ) ) continue ;
					$nc = array_pop ( $nc ) ;
					$lcats2[$nl][$nc] = $nc ;
				}
			}
		}
		foreach ( $lcats2 AS $lang => $cs ) {
			foreach ( $cs AS $c ) {
				$lcats[$lang][$c] = $c ;
			}
		}
//		print "<pre>" ; print_r ( $lcats ) ; print "</pre><hr/>" ;
	}
	print "<br/><i>Note : Auto-language system is changing...</i><br/>" ;
*/

	print "<br/>" ;
	$global_total = 0 ;
	$gpages = array () ;
	$orig_lang = array () ;
	foreach ( $lcats AS $lang => $c ) {
		$orig_lang[$lang] = 1 ;
		print "Scanning $lang.wikipedia ...<br/>" ; myflush() ;
		$dc = array () ;
		$nc = array () ;
		foreach ( $c AS $cat ) {
			if ( substr ( $cat , 0 , 1 ) == '-' ) {
				$nc[] = substr ( $cat , 1 ) ;
			} else {
				$dc[] = $cat ;
			}
		}
		$dc = urlencode ( implode ( "\n" , $dc ) ) ;
		$nc = urlencode ( implode ( "\n" , $nc ) ) ;
		$url = "http://toolserver.org/~magnus/catscan_rewrite.php?format=php&language=$lang&depth=$depth&doit=1&categories=$dc&negcats=$nc";
		if ( $combination == 'union' ) $url .= '&comb[union]=1' ;
//		print "<pre>$url</pre>" ;
		$data = unserialize ( file_get_contents ( $url ) ) ;
		$pages = array () ;
		foreach ( $data['*'][0]['*'] AS $x ) {
			if ( $x['a']['namespace'] != '0' ) continue ;
			$t = $x['a']['title'] ;
			$pages[$t] = 0 ;
		}
		$gpages[$lang] = $pages ;
	}

	if ( $autolang == 1 ) {
		print "Filling language links...<br/>" ; myflush();
		$gp2 = array () ;
		foreach ( $gpages AS $lang => $pages ) {
			foreach ( $pages AS $page => $dummy ) {
				$nn = db_get_language_links ( $page , $lang , 'wikipedia' ) ;
				foreach ( $nn AS $l => $t ) {
					$gp2[$l][$t] = 0 ;
				}
			}
		}
		foreach ( $gp2 AS $lang => $pages ) {
			foreach ( $pages AS $p => $dummy ) {
				$gpages[$lang][$p] = 0 ;
			}
		}
	}

	ksort ( $gpages ) ;
	print "<h3>Languages</h3>" ;
	foreach ( $gpages AS $lang => $pages ) {
		print "<a href='#$lang.wikipedia'>" ;
		if ( isset ( $orig_lang[$lang] ) ) print "<b>$lang</b>" ;
		else print $lang ;
		print "</a> " ;
	}
	print "<br/>" ;

//print "<hr><pre>" ; print_r ( $gpages ) ; print "</pre>" ;
//print "<hr/>MAINTENANCE<br/>\n" ; exit ( 0 ) ;


	myflush() ;
	foreach ( $gpages AS $lang => $pages ) {
		$comb2 = strtoupper ( substr ( $combination , 0 , 1 ) ) . substr ( $combination , 1 ) ;
		print "<h2><a name='$lang.wikipedia'>$comb2 of $lang.wikipedia categories</a></h2>" ; myflush() ;
		print count ( $pages ) . " pages found. <small>Click on a table header to sort.</small>" ;
		$total = 0 ;
		print "<table border='1' class='sortable'><thead><tr><th>Page</th><th>Views during $date2</th>" ;
		if ( $pcd ) print "<th>Page creation date</th>" ;
		print "</tr></thead><tbody>" ;
		ksort ( $pages ) ;
		foreach ( $pages AS $page => $cnt ) {
			$url = "http://stats-classic.grok.se/json/$lang/$date/" . urlencode ( urlencode ( $page ) ) ;

			$d = json_decode ( file_get_contents ( $url ) , true ) ;

			$cnt = $d['total_views'] ;
/*			$cnt = 0 ;
			foreach ( $d['daily_views'] AS $theday => $thecount ) {
				$cnt += $thecount ;
			}*/
			
			$pages[$page] = $cnt ;
			$total += $cnt ;
			$link = "<a href=\"http://$lang.wikipedia.org/wiki/$page\">" . ucfirst ( str_replace ( '_' , ' ' , $page ) ) . "</a>" ;
			print "<tr><td>$link</td><td nowrap style='text-align:right'>" . prettynum ( $cnt ) . "</td>" ;
			if ( $pcd ) {
				$the_date = get_page_creation_date ( $lang , 'wikipedia' , $page ) ;
				print "<td>$the_date</td>" ;
			}
			print "</tr>" ;
			myflush() ;
		}
		print "</tbody><tfoot><tr><th>Total</th><td nowrap style='text-align:right'>" . prettynum ( $total ) . "</td></tr></tfoot>" ;
		print "</table>" ;
		$global_total += $total ;
		myflush() ;
//		print "<pre>" ; print_r ( $data ) ; print "</pre>" ;
	}
	
	print "<hr/><big>Global total : <b>" . prettynum ( $global_total ) . "</b> page views in $date2</big>" ;
}

print "</body></html>" ;

?>
