<?PHP

error_reporting ( E_ALL ) ;

include ( "common.php" ) ;
high_mem ( 60 , 'mygallery' ) ;
@set_time_limit ( 15*60 ) ; # Time limit 15min


#________________________________________________________________________________________

function db_get_user_images_and_cats ( $username , $language , $project ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = $language . 'wiki_p' ;
	make_db_safe ( $username ) ;
	$username = str_replace ( '_' , ' ' , $username ) ;

	
	$ret = array () ;
	$sql = "SELECT ".get_tool_name()." img_name,img_size,img_width,img_height,page_id,cl_to FROM image,page,categorylinks WHERE img_name=page_title AND page_namespace=6 AND cl_from=page_id AND img_user_text='{$username}'" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return $ret ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[] = $o ;
	}
	return $ret ;
}

function show_category_tree ( $cat , &$is_parent_of , &$categories , &$image_in_cateogries , $depth = 0 ) {
  global $sct_done , $language , $project , $cats_only_once , $cat_shown ;
  if ( isset ( $sct_done[$cat] ) ) return ;
  $sct_done[$cat] = 1 ;
  
  if ( !isset ( $categories[$cat] ) AND !isset ( $is_parent_of[$cat] ) ) {
    unset ( $sct_done[$cat] ) ;
    return ;
  }


  print "<ul" ;
  if ( $depth == 0 ) print " class='mktree' id='fulltree'" ;
  print ">" ;
  
  $max = 100 ; # px
  $catlink = "<a name='category_$cat' target='_blank' href=\"http://$language.$project.org/wiki/Category:$cat\">" . str_replace('_',' ',$cat) . "</a>" ;
  if ( isset ( $categories[$cat] ) and $cats_only_once and isset ( $cat_shown[$cat] ) ) {
    $catlink = "<a target='_blank' href=\"http://$language.$project.org/wiki/Category:$cat\">" . str_replace('_',' ',$cat) . "</a>" ;
    print "<li class='liOpen'><b>$catlink</b> ($categories[$cat]) <a href='#category_$cat'><i>shown elsewhere</i></a>" ;
  } else if ( isset ( $categories[$cat] ) ) {
    $cat_shown[$cat] = 1 ;
    print "<li class='liOpen'><b>$catlink</b> ($categories[$cat])" ;
    print "<ul style='right:100%'><li style='list-type:none;'>" ;
    foreach ( $image_in_cateogries[$cat] AS $i ) {
      if ( $i->img_width == 0 ) {
        $w = 0 ;
      } else if ( $i->img_width > $i->img_height ) {
        $w = $max ;
      } else {
        $w = round ( $i->img_width / $i->img_height * $max ) ;
      }
      if ( $w == 0 ) $img_url = get_thumbnail_url ( $language , "Crystal Clear app kaboodle.png" , 50 , $project ) ;
      else $img_url = get_thumbnail_url ( $language , $i->img_name , $w , $project ) ;
      $img_title = str_replace ( '_' , ' ' , $i->img_name ) ;
      $url = get_wikipedia_url ( $language , "Image:" . $i->img_name , '' , $project ) ;

      $disp = $w == 0 ? 'block' : 'inline' ;
      $maxx = $w == 0 ? '' : "width:{$max}px;" ;
      print "<div style='display:$disp;margin-right:5px;vertical-align:text-top;$maxx'>" ;
      if ( $w == 0 ) print "<img src='http://upload.wikimedia.org/wikipedia/commons/b/b3/Xmms.png' width='16px'/> " ;
      print "<a target='_blank' href=\"$url\">" ;
      if ( $w == 0 ) print $img_title ;
      else print "<img border='0' alt=\"$img_title\" title=\"$img_title\" src=\"$img_url\"/>" ;
      print "</a></div>" ;
    }
    print "</li></ul>" ;
  } else {
    print "<li>$catlink" ;
  }
  
  if ( isset ( $is_parent_of[$cat] ) ) {
    foreach ( $is_parent_of[$cat] AS $child ) {
      show_category_tree ( $child , $is_parent_of , $categories , $image_in_cateogries , $depth+1 ) ;
    }
  }
  print "</li></ul>\n" ;
  unset ( $sct_done[$cat] ) ;
  myflush();
}


function add_to_category_tree ( $cat , &$is_child_of , &$is_parent_of ) {
  global $mysql_con , $language ;
  if ( isset ( $is_child_of[$cat] ) ) return ; # Had that covered already
#  print "Adding $cat<br/>\n" ; myflush();


	$db = $language . 'wiki_p' ;
  $sql = "SELECT cl_to FROM page,categorylinks WHERE page_title=\"$cat\" AND page_namespace=14 AND cl_from=page_id" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) { print mysql_error(); return ; }
	$parents = Array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
    $parents[] = $o->cl_to ;
  }
  $is_child_of[$cat] = $parents ;
  foreach ( $parents AS $p ) {
    if ( !isset ( $is_parent_of[$p] ) ) $is_parent_of[$p] = Array () ;
    $is_parent_of[$p][$cat] = $cat ;
#    print "Sub $p<br/>" ;
    add_to_category_tree ( $p , $is_child_of , $is_parent_of ) ;
  }
}

function filter_tree ( $cat , &$is_parent_of , &$categories ) {
  global $sct_done ;
  if ( isset ( $sct_done[$cat] ) ) return $sct_done[$cat] ;
  if ( !isset ( $is_parent_of[$cat] ) ) return 0 ;
  
  $ret = 0 ;
  if ( isset ( $categories[$cat] ) ) $ret = 1 ;
  $sct_done[$cat] = 1 ;
  foreach ( $is_parent_of[$cat] AS $k => $child ) {
    if ( !isset ( $is_parent_of[$child] ) ) $r = 0 ;
    else $r = filter_tree ( $child , $is_parent_of , $categories ) ;
    if ( $r == 1 ) {
      $ret = 1 ;
    } else {
      unset ( $is_parent_of[$child] ) ;
    }
  }
#  unset ( $sct_done[$cat] ) ;
  if ( $ret == 0 || count ( $is_parent_of[$cat] ) == 0 ) {
    unset ( $is_parent_of[$cat] ) ;
#      print "Nuking $cat<br/>" ;
  }
  return $ret ;
}



#________________________________________________________________________________________

print "<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' type='text/css' href='dhtmltree/mktree.css' media='all'>
</head>
<body>" ;
#<script type='text/javascript' src='dhtmltree/mktree.js'></script>
print get_common_header ( 'mygallery.php' ) ;
print "<h1>Shows the complete category tree for a user's files</h1>" ;

$language = fix_language_code ( get_request ( 'language' , 'commons' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikimedia' ) ) ;
$user = get_request ( 'user' , '' ) ;
$cats_only_once = isset ( $_REQUEST['cats_only_once'] ) ;
if ( $user == '' ) $cats_only_once = 1 ;
$cats_only_once_check = $cats_only_once ? 'checked' : '' ;

print "<form method='post'>
<table>
<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>
<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
<tr><th>User</th><td><input type='text' name='user' value='{$user}'/></td></tr>
<tr><th></th><td><input type='checkbox' name='cats_only_once' value='1' {$cats_only_once_check}/>Show each category only once</td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Run'/> (note: it may take a few minutes to load, depending on the number of user files)</td></tr>
</table></form>" ;

if ( $user == '' ) {
  exit ;
}


# Query image/category pairs
print "Looking for user images ... " ; myflush() ;
$ui = db_get_user_images_and_cats ( $user , $language , $project ) ;
print count ( $ui ) . " image/category combinations found.<br/>" ; myflush() ;

# Assemble categories
$categories = Array () ;
foreach ( $ui AS $i ) {
  $c = $i->cl_to ;
  if ( !isset ( $categories[$c] ) ) $categories[$c] = 1 ;
  else $categories[$c]++ ;
}
print count ( $categories ) . " unique categories (including licensing and user categories, which will be removed).<br/>" ;

# Filter categories
arsort ( $categories ) ;
foreach ( $categories AS $k => $v ) {
  $kl = strtolower ( $k ) ;
  if ( substr ( $kl , 0 , 4 ) == 'gfdl' ||
       substr ( $kl , 0 , 3 ) == 'pd-' ||
       substr ( $kl , 0 , 14 ) == 'self-published' ||
       substr ( $kl , 0 , 13 ) == 'media_lacking' ||
       substr ( $kl , 0 , 3 ) == 'pd_' ||
       substr ( $kl , 0 , 3 ) == 'cc-'
      ) unset ( $categories[$k] ) ;
}

# Images in categories
$unique_images = Array () ;
$image_in_cateogries = Array () ;
foreach ( $ui AS $i ) {
  if ( !isset ( $image_in_cateogries[$i->cl_to] ) ) $image_in_cateogries[$i->cl_to] = Array () ;
  $image_in_cateogries[$i->cl_to][] = $i ;
  $unique_images[$i->img_name] = $i->img_name ;
}
print count ( $unique_images ) . " unique images.<br/>" ;

# Category tree
print "Creating category tree ... " ; myflush() ;
if ( $language == 'de' ) $basecat = '!Hauptkategorie' ;
else if ( $language == 'en' ) $basecat = 'Contents' ;
else $basecat = 'CommonsRoot' ;
$is_child_of = array () ;
$is_parent_of = array () ;
$is_child_of[$basecat] = Array () ;
foreach ( $categories AS $k => $v ) {
#  if ( $v < 10 ) continue ; # TESTING
  add_to_category_tree ( $k , $is_child_of , $is_parent_of ) ;
}

print "done!<br/>" ; myflush() ;

$cat_shown = Array () ;
$sct_done = Array () ;
$sct_done['Copyright_statuses'] = 1 ;
$sct_done['Self-published_work'] = 1 ;
$sct_done['User_galleries'] = 1 ;
show_category_tree ( $basecat , $is_parent_of , $categories , $image_in_cateogries ) ;

print "<body></html>" ;

?>
