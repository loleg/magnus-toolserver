<?PHP
error_reporting ( E_ALL ) ;
$suppress_gz_handler = 1 ;
@set_time_limit ( 45*60 ) ; # Time limit 45min

include_once ( 'queryclass.php' ) ;
include_once ( 'phpdiff.php' ) ;
high_mem ( 64 , 'relicenser' ) ;

$keywords = array (
	"GFDL", 
	"Creative Commons", 
	"CC-", 
	"PD", 
	"public domain", 
	"gemeinfrei", 
	"Bild-frei", 
	"GNU Free Documentation Licence",
	"wahrscheinlich",
) ;


function print_form () {
	global $language , $project , $user , $from , $to , $showdiff ;
	$showdiff_checked = $showdiff ? 'checked' : '' ;
	print "<form method='post' action='relicenser.php'>
	<table border='1'>
	<tr><th>Language</th><td><input type='text' name='language' value='$language' /></td></tr>
	<tr><th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>
	<tr><th>User</th><td><input type='text' name='user' value='$user' /></td></tr>
	<tr><th>From</th><td><input type='text' name='from' value='$from' /> (default: last year)</td></tr>
	<tr><th>To</th><td><input type='text' name='to' value='$to' /> (default: now)</td></tr>
<!--	<tr><th/><td><input type='checkbox' name='showdiff' value='1' id='showdiff' $showdiff_checked /><label for='showdiff'>Always show diff</label></td></tr>-->
	<tr><th/><td><input type='submit' name='doit' value='Do it' /></td></tr>
	</table>
	</form>" ;
}

function collapse_time ( $t ) {
	return preg_replace ( '/[^\d]/' , '' , $t ) ;
}

function db_get_user_edited_pages_between ( $language , $project , $user_id , $from , $to ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name($language,$project);
	make_db_safe ( $user ) ;
	
	$min = collapse_time ( $from ) ;
	$max = collapse_time ( $to ) ;
	
	$ret = array () ;
	$sql = "SELECT ".get_tool_name()." * FROM revision,page WHERE page_id=rev_page AND page_namespace=6 AND rev_user=$user_id AND rev_timestamp <= '$max' AND rev_timestamp >= '$min'" ;
#	print "<p>$sql</p>" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) { print  mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[] = $o ;
#		print "<p>" . $o->page_title . "</p>" ;
	}
	return $ret ;
}

function db_get_previous_revision ( $language , $project , $page_id , $rev_timestamp ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name($language,$project);
	$sql = "SELECT ".get_tool_name()." rev_id FROM revision WHERE rev_page=$page_id AND rev_timestamp < '$rev_timestamp' ORDER BY rev_timestamp DESC" ;
//	print "<p>$sql</p>" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) { print  mysql_error() . "<br/>" ; return $ret ; } # Some error has occurred
	if ( $o = mysql_fetch_object ( $res ) ) return $o->rev_id ;
	return '' ;
}


function get_old_version ( $language , $project , $title , $revision ) {
	if ( $revision == "" ) return "" ; // No such version
	$url = "http://$language.$project.org/w/index.php?title=$title&oldid=$revision&action=raw" ;
	return file_get_contents ( $url ) ;
}

function get_license_hash ( $t ) {
	global $keywords ;
	$ret = array () ;
	$t = strtolower ( $t ) ;
	$t = str_replace ( '_' , ' ' , $t ) ;
	$t = str_replace ( "\n" , ' ' , $t ) ;
	$t = str_replace ( '  ' , ' ' , $t ) ;
	foreach ( $keywords AS $key ) {
		$k = strtolower ( $key ) ;
		$k = str_replace ( '_' , ' ' , $k ) ;
		$ret[$key] = count ( split ( $k , $t ) ) - 1 ;
	}
	return $ret ;
}

function check_edit ( $d , $idata ) {
	global $language , $project , $showdiff ;
	$prev_id = db_get_previous_revision ( $language , $project , $d->page_id , $d->rev_timestamp ) ;
	$url = get_wikipedia_url ( $language , "Image:".$d->page_title , 'view' , $project ) ;
	$url_version = $url . "&oldid=" . $d->rev_id ;
	$url_diff = $url . "&oldid=$prev_id&diff=" . $d->rev_id ;
//	print "<li><a target='_blank' href=\"$url\">" . $d->page_title . "</a> (<a target='_blank' href=\"$url_version\">" . $d->rev_timestamp . "</a> / <a target='_blank' href=\"$url_diff\">diff</a>)" ;
	print "<li><a target='_blank' href=\"$url\">" . $d->page_title . "</a> (<a target='_blank' href=\"$url_diff\">diff</a>)" ;
	if ( isset ( $d->commons ) ) print " [image on Commons]" ;
	$t1 = get_old_version ( $language , $project , "Image:" . $d->page_title , $prev_id ) ;
	$t2 = get_old_version ( $language , $project , "Image:" . $d->page_title , $d->rev_id ) ;
	$l1 = get_license_hash ( $t1 ) ;
	$l2 = get_license_hash ( $t2 ) ;
	$show = $showdiff ;
	
	foreach ( $l1 AS $k => $v1 ) {
		$v2 = $l2[$k] ;
		if ( $v1 == $v2 ) continue ;
		print "; <span style='color:red'>" ;
		if ( $v1 > $v2 ) print "removed \"$k\"" ;
		else print "added \"$k\"" ;
		if ( $v2 - $v1 > 1 ) print " (" . ( $v2 - $v1 ) . "&times;)" ;
		print "</span>" ;
//		$show = 1 ; // Always show diff when suspect
	}
	
//	if ( $show ) print "<pre style='font-size:8pt;background:#DDDDDD'>" . PHPDiff ( "$t1\n" , "$t2\n" ) . "</pre>" ;
	
	print "</li>\n" ;
	myflush() ;
}




$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$user = get_request ( 'user' , '' ) ;
$from = get_request ( 'from' , date ( 'Y-m-d H:i:s' , strtotime('-1 year') ) ) ;
$to = get_request ( 'to' , date ( 'Y-m-d H:i:s' , time() ) ) ;

if ( isset ( $_REQUEST['doit'] ) ) $showdiff = isset ( $_REQUEST['showdiff'] ) ;
else $showdiff = 1 ;
$showdiff = 0 ;


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "relicenser.php" ) . "\n" ;
myflush() ;


print_form () ;

if ( isset ( $_REQUEST['doit'] ) and $user != '' ) {
	$uid = db_get_user_id ( $language , $user , $project ) ;
	print "<p>User <i>$language.$project:$user</i> (#$uid).</p>" ;
	$user_images = db_get_user_images ( $user , $language , $project ) ;
	print "<p>User has uploaded " . count ( $user_images ) . " files (current versions only).</p>" ;
	$data = db_get_user_edited_pages_between ( $language , $project , $uid , $from , $to ) ;
	print "<p>User made " . count ( $data ) . " edits to file description pages between $from and $to.</p>" ;
	
	foreach ( $data AS $k => $v ) {
		if ( isset ( $user_images[$v->page_title] ) ) unset ( $data[$k] ) ;
	}
	
	print "<p>User made " . count ( $data ) . " edits to file description pages of files he didn't upload between $from and $to.</p>" ;
	
	$i = array() ;
	foreach ( $data AS $k => $v ) {
		$i[$v->page_title] = $v->page_title ;
	}
	$imgdata = db_get_images_data ( $i , $language , $project ) ;
	$imgdata_commons = db_get_images_data ( $i , 'commons' , 'wikimedia' ) ;
	foreach ( $imgdata_commons AS $k => $v ) {
		if ( isset ( $imgdata[$k] ) ) continue ;
		$v->commons = 1 ;
		$imgdata[$k] = $v ;
	}

	$user2image = array () ;
	foreach ( $imgdata AS $k => $v ) {
		if ( !isset ( $user2image[$v->img_user_text] ) ) $user2image[$v->img_user_text] = array() ;
		$user2image[$v->img_user_text][] = $v->img_name ;
	}
	
	ksort ( $user2image ) ;
	foreach ( $user2image AS $user => $image ) {
	
		print "<h3>Uploaded by $user</h3>\n<ul>\n" ;
		foreach ( $data AS $v ) {
			if ( $imgdata[$v->page_title]->img_user_text != $user ) continue ;
			check_edit ( $v , $imgdata[$v->page_title] ) ;
		}
		print "</ul>\n<hr/>" ;
	}
}

print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
