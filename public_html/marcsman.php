<?PHP

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

include "common.php" ;

$avoid_categories = array ( 'United States' , 'America' ) ;
$author_license = array (
	'Carol M. Highsmith' => 'PD-Highsmith'
) ;

function get_commons_description_from_marc ( $marc ) {
	global $avoid_categories , $author_license ;
	$ret = "" ;

	// Init
	$desc = array () ;
	$author = '' ;
	$date = '' ;
	$permission = array () ;
	$license = array() ;
	$source = array ( '{{LOC-Image|id=' , '' , '.' , '' , '}}' ) ;
	$cat_cands = array () ;
	$dimensions = array() ;
	$medium = array() ;
	$title = array() ;
	$location = array() ;
	$accession = array() ;
	$notes = array() ;
	$institution = array() ;

	// Init months
	$month = array() ;
	$months['january'] = '01' ;
	$months['february'] = '02' ;
	$months['march'] = '03' ;
	$months['april'] = '04' ;
	$months['may'] = '05' ;
	$months['june'] = '06' ;
	$months['july'] = '07' ;
	$months['august'] = '08' ;
	$months['september'] = '09' ;
	$months['october'] = '10' ;
	$months['november'] = '11' ;
	$months['december'] = '12' ;
	
	// Parse template/category list
	$tae = file_get_contents ( 'http://meta.wikimedia.org/w/index.php?title=MARCsman/Collections&action=raw' ) ;
	$tae = explode ( "\n" , $tae ) ;
	$l_source = '' ;
	$pot_source = array() ;
	foreach ( $tae AS $l ) {
		$l = trim ( $l ) ;
		if ( preg_match ( '/^\*\s*\'\'\'(.+)\'\'\'$/' , $l , $m ) ) {
			$l_source = trim ( preg_replace ( '/[\.,]+$/' , '' , $m[1] ) ) ;
#			$pot_source[$l_source]['url'] = $m[1] ;
			$pot_source[$l_source]['cats'] = array() ;
			$pot_source[$l_source]['templates'] = array() ;
			$pot_source[$l_source]['permission'] = '' ;
			if ( preg_match ( '/"(.+?)"\s*$/' , $l , $m2 ) ) $pot_source[$l_source]['permission'] = $m2[1] ;
		} else if ( $l_source != '' and preg_match ( '/^\*\*/' , $l , $m ) ) {
			if ( preg_match ( '/\[\[:Category:(.+?)\]\]/i' , $l , $m ) ) {
				$pot_source[$l_source]['cats'][] = $m[1] ;
			}
			if ( preg_match ( '/\{\{tl\|(.+?)\}\}/i' , $l , $m ) ) {
				$pot_source[$l_source]['templates'][] = $m[1] ;
			}
		} else {
			$l_source = '' ;
			continue ;
		}
	}
	
	// Parse MARC
	$l = '' ;
	foreach ( $marc AS $m ) {
		$m[4] = htmlspecialchars_decode ( trim ( $m[4] ) ) ;
		if ( $m[0] == '' ) $m[0] = $l ;
		else $l = $m[0] ;
		if ( $m[0] == 856 and $m[1] == 4  and $m[2] == 1 ) {
			if ( $m[3] == 'd' ) $source[1] = $m[4] ;
			if ( $m[3] == 'f' ) $source[3] = $m[4] ;
		}
		if ( $m[0] == 500 and $m[3] == 'a' ) {
			$desc[] = $m[4] ;
		}
		if ( $m[0] == 540 and $m[3] == 'a' ) {
			$permission[] = $m[4] ;
		}
		if ( $m[0] >= 500 and $m[0] < 600 ) {
			$notes[$m[0]][] = preg_replace ( '/\s*;$/' , '' , $m[4] ) ;
		}
		if ( $m[0] == 260 and $m[3] == 'c' ) {
			$date = trim ( $m[4] ) ;
			if ( preg_match ( '/^\[between (\d{4}) and (\d{4})\]$/i' , $date , $d2 ) ) {
				$date = '{{otherdate|between|' . $d2[1] . '|' . $d2[2] . '}}' ;
			} else if ( preg_match ( '/^c\s*(\d{4})$/i' , $date , $d2 ) ) {
				$date = '{{date|' . $d2[1] . '}}' ;
			} else if ( preg_match ( '/^ca\.\s*(\d{4})$/i' , $date , $d2 ) ) {
				$date = '{{other date|circa|' . $d2[1] . '}}' ;
			} else if ( preg_match ( '/^(\w+)\s(\d+),\s*(\d{4})$/i' , $date , $d2 ) ) {
				if ( isset ( $months[strtolower($d2[1])] ) ) $date = '{{date|' . $d2[3] . '|' . $months[strtolower($d2[1])] . '|' . $d2[2] . '}}' ;
			} else if ( preg_match ( '/^(\d{4})[- ](\w+)$/i' , $date , $d2 ) ) {
				if ( isset ( $months[strtolower($d2[2])] ) ) $date = '{{date|' . $d2[1] . '|' . $months[strtolower($d2[2])] . '}}' ;
			} else if ( preg_match ( '/^(\w+) (\d{4})$/i' , $date , $d2 ) ) {
				if ( isset ( $months[strtolower($d2[1])] ) ) $date = '{{date|' . $d2[2] . '|' . $months[strtolower($d2[1])] . '}}' ;
			} else if ( preg_match ( '/^(\d{4})-(\d{4})$/i' , $date , $d2 ) ) {
				$date = '{{other date|between|' . $d2[1] . '|' . $d2[2] . '}}' ;
			} else {
				$date = preg_replace ( '/\.$/' , '' , $date ) ;
				$date = preg_replace ( '/^\[(.+)\]$/' , '$1' , $date ) ;
			}
		}
		if ( $m[0] == 37 ) {
			if ( $m[3] == 'a' or $m[3] == 'c' ) $accession[] = trim ( $m[4] ) ;
		}
		if ( $m[0] == 245 ) {
			$v = trim ( preg_replace ( '/[ :;.]+$/' , '' , $m[4] ) ) ;
			$v = trim ( preg_replace ( '/^\((.+)\)$/' , '$1' , $m[4] ) ) ;
			if ( $m[3] == 'a' ) $title[] = $v ;
			if ( $m[3] == 'b' ) $title[] = "($v)" ;
//			if ( $m[3] == 'h' ) $medium[] = $v ;
		}
		if ( $m[0] == 852 ) {
			if ( preg_match ( '/Library of Congress/i' , $m[4] ) ) $institution[] = '{{Institution:Library of Congress}}' ;
			$v = trim ( preg_replace ( '/[ :;.]+$/' , '' , $m[4] ) ) ;
			$location[] = $v ;
		}
		if ( $m[0] == 300 ) {
			$v = trim ( preg_replace ( '/[ :;.]+$/' , '' , $m[4] ) ) ;
			$v2 = trim ( $m[4] ) ;
			if ( $m[3] == 'a' ) $medium[] = $v ;
			if ( $m[3] == 'b' ) $medium[] = $v ;
			if ( $m[3] == 'c' ) {
				if ( preg_match ( '/^(\d+)\s*x\s*(\d+) in\.$/i' , $v2 , $matches ) ) {
					$dimensions[] = '{{Size|in|'.$matches[1].'|'.$matches[2].'}}' ;
				} else $dimensions[] = $v ;
			}
		}
		if ( $m[0] == 100 and $m[1] == 1 and $m[3] == 'a' ) {
			$x = explode ( ',' , $m[4] ) ;
			$author_lastname = $x[0] ;
			$x = array_reverse ( $x ) ;
			foreach ( $x AS $y => $z ) {
				$x[$y] = trim ( $z ) ;
			}
			$author = trim ( implode ( ' ' , $x ) ) ;

			if ( isset ( $author_license[$author] ) ) {
				$z = '{{' . $author_license[$author] . '}}' ;
				$license[$z] = $z ;
			} else {
				$lic = 'PD-' . $author_lastname ;
				$c = db_get_existing_pages ( array ( $lic ) , 'commons' , 'wikipedia' , 10 ) ;
				if ( count ( $c ) > 0 ) {
					$z = "{{" . $lic . "}}" ;
					$license[$z] = $z ;
				}
			}

			$a = db_get_existing_pages ( array ( $author ) , 'commons' , 'wikipedia' , 100 ) ;
			if ( count ( $a ) > 0 ) $author = "{{Creator:$author}}" ;
			
		}
		if ( $m[0] == 524 and $m[1] == 8 and $m[3] == 'a' ) {
			$permission[] = $m[4] ;
		}
		if ( $m[0] >= 600 and $m[0] < 700 ) {
			$c = $m[4] ;
			$c = preg_replace ( '/\.$/' , '' , $c ) ;
			$c = ucfirst ( trim ( $c ) ) ;
			
			if ( in_array ( $c , $avoid_categories ) ) continue ;
			$cat_cands[$c] = $c ;
			$cat_cands[$c] = $c ;
		}
		if ( $m[0] == 773 or $m[0] == 580 or $m[0] == 500 or $m[0] == 100 ) {
			$n = strtolower ( trim ( preg_replace ( '/[,.]+$/' , '' , $m[4] ) ) ) ;
			foreach ( $pot_source AS $k => $v ) {
//				print "$k / $n<br/>" ;
				if ( strtolower ( $k ) != $n ) continue ;
				foreach ( $v['cats'] AS $c ) $cat_cands[$c] = $c ;
				foreach ( $v['templates'] AS $t ) {
					$z = '{{' . $t . '}}' ;
					$license[$z] = $z ;
				}
				if ( $v['permission'] != '' ) $permission[] = $v['permission'] ;
			}
		}
	}

	$institution = implode ( '<br/>' , $institution ) ;

	foreach ( $notes AS $k => $v ) {
		$notes[$k] = implode ( '; ' , $v ) ;
	}
	$notes = implode ( "<br/>" , $notes ) ;

/*	$license_templates = implode ( "\n" , $license_templates ) ;
	if ( $license == '' ) {
		if ( in_array ( 'No known restrictions on publication.' , $permission ) and '' == $license_templates ) $license = "{{PD-author}}" ;
	}
	if ( $license != '' and $license_templates != '' ) $license .= "\n" ;
	$license .= $license_templates ;
	if ( $license != '' ) $license = "== {{int:license}} ==\n$license" ;
	*/
	
	if ( count ( $license ) >= 2 and isset ( $license['{{PD-author}}'] ) ) unset ( $license['{{PD-author}}'] ) ;
	$license = implode ( "\n" , $license ) ;
	if ( license != '' ) $license = "== {{int:license}} ==\n$license" ;
	
	
	$accession = implode ( ' ' , $accession ) ;
	
	$permission = implode ( ' ' , $permission ) ;
	$source = implode ( '' , $source ) ;
	$desc = '{{en|' . implode ( "\n\n" , $desc ) . '}}' ;
	
	$cats2 = db_get_existing_pages ( $cat_cands , 'commons' , 'wikipedia' , 14 , 1 ) ;
//	$cats2 = db_get_existing_categories ( $cat_cands , 'commons' , 'wikipedia' ) ;
	
	$cats = '' ;
	if ( 0 < count ( $cats2 ) ) {
		foreach ( $cats2 AS $c ) $cats .= '[[Category:' . str_replace ( '_' , ' ' , $c ) . "]]\n" ;
//		$cats = "[[Category:" . implode ( "]]\n[[Category:" , $cats2 ) . "]]" ;
	}
	
	$title = implode ( '; ' , $title ) ;
	$medium = implode ( '; ' , $medium ) ;
	$dimensions = implode ( '; ' , $dimensions ) ;
	$location = implode ( ', ' , $location ) ;
	
	$ret = "{{Artwork
|artist=$author
|title=$title
|description=$desc
|date=$date
|medium=$medium
|dimensions=$dimensions
|institution=$institution
|location=$location
|references=
|object history=
|credit line=
|inscriptions=
|notes=$notes
|accession number=$accession
|source=$source
|permission=$permission
|other_versions=
}}

$license

$cats" ;

	return trim ( $ret ) ;
}

function get_marc_from_html ( $locurl ) {
	$ret = array () ;
	$html = file_get_contents ( $locurl ) ;
	$html = array_pop ( explode ( ' id="marc"' , $html , 2 ) ) ;
	$html = array_pop ( explode ( '<table' , $html , 2 ) ) ;
	$html = array_shift ( explode ( '</table>' , $html , 2 ) ) ;
	$html = preg_replace("/<!--.*-->/Uis", "", $html); 
	$trs = explode ( '</tr>' , $html ) ;
	array_pop ( $trs ) ;
	array_shift ( $trs ) ;
	foreach ( $trs AS $tr ) {
		$tr = array_pop ( explode ( '<tr>' , $tr , 2 ) ) ;
		$tds = explode ( '</td>' , $tr ) ;
		array_pop ( $tds ) ;
		$d = array () ;
		foreach ( $tds AS $td ) {
			$td = array_pop ( explode ( '>' , $td , 2 ) ) ;
			$td = trim ( str_replace ( '&nbsp;' , ' ' , $td ) ) ;
			$d[] = $td ;
		}
		$ret[] = $d ;
//		print htmlspecialchars ( $s ) . "<br/>" ;
	}
	return $ret ;
}

$locid = get_request ( 'locid' , '' ) ;
$locurl = get_request ( 'locurl' , '' ) ;

print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' ;
print get_common_header ( "marcsman.php" , "MARCsman" ) ;
print "<h1>MARCsman</h1>Extract MARC data from the Library of Congress<br/>" ;

//LOC ID <input name='locid' type='text' size='20' value='$locid' />, <i>or</i>
print "<form method='get'>
URL <input name='locurl' type='text' size='100' value='$locurl' />
<input name='doit' value='Generate description' type='submit' />
</form>" ;

if ( isset ( $_REQUEST['doit'] ) and $locurl != '' ) {
	$marc = get_marc_from_html ( $locurl ) ;
	$cd = get_commons_description_from_marc ( $marc ) ;
	print "<textarea rows='25' cols='120'>" . htmlspecialchars ( $cd ) . "</textarea><br/>" ;
	print "<h2>Original MARC data</h2>" ;
	print "<table border=1>" ;
	foreach ( $marc AS $m ) {
		print "<tr>" ;
		foreach ( $m AS $td ) print "<td>" . htmlspecialchars ( $td ) . "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

print '</body></html>' ;

?>