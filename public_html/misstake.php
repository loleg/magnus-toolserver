<?php

error_reporting ( E_ALL ) ;
include_once ( "queryclass.php") ;

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$page = get_request ( 'page' , '' ) ;

print '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "misstake.php" ) ;
print "<body><h1>Ranked missing articles</h1>" ;

print "
<form method='post'>
<table>
<tr><th>Language</th><td><input name='language' value='$language' /></td></tr>
<tr><th>Project</th><td><input name='project' value='$project' /></td></tr>
<tr><th>Page(s)</th><td><textarea name='page' value='$page' cols=60 rows=3>$page</textarea></td><td>All namespaces allowed<br/>One page per line</td></tr>
<tr><th></th><td><input name='doit' value='Do it' type='submit' /></td></tr>
</table>
</form>
" ;

if ( !isset ( $_REQUEST['doit'] ) ) {
	print "</body></html>" ;
	exit ( 0 ) ;
}

$wq = new WikiQuery ( $language , $project ) ;

if ( $page != '' ) {
	$mysql_con = db_get_con_new ( $language , $project ) ;
	$db = get_db_name ( $language , $project ) ;

	$nss = $wq->get_namespaces() ;
#	print "<pre>" ; print_r ( $nss ) ; print "</pre>" ;

	$pageids = array() ;
	$pagelist = explode ( "\n" , $page ) ;
	foreach ( $pagelist AS $page ) {
		$page = str_replace ( '_' , ' ' , $page ) ;
		$ns = 0 ;
		foreach ( $nss AS $k => $v ) {
			$n = strtoupper ( $v . ':' ) ;
			if ( strtoupper ( substr ( $page , 0 , strlen ( $n ) ) ) == $n ) {
				$ns = $k ;
				$page = substr ( $page , strlen ( $n ) ) ;
				break ;
			}
		}
		
		if ( $ns == 14 ) { // Category
			make_db_safe ( $page ) ;
			$sql = "SELECT $slow_ok_limit DISTINCT page_title,page_namespace FROM page,categorylinks WHERE page_id=cl_from AND cl_to=\"{$page}\"" ;
			$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
			while ( $o = mysql_fetch_object ( $res ) ) {
//				print "<div>Adding " . $o->page_title . "</div>" ;
				$pid = db_get_page_id ( $language , $project , $o->page_title , $o->page_namespace ) ;
				$pageids[$pid] = $pid ;
			}
		} else {
			$pid = db_get_page_id ( $language , $project , $page , $ns ) ;
			$pageids[$pid] = $pid ;
		}
	}
	print "<div>Checking " . count ( $pageids ) . " pages for redlinks and their \"wantedness\"...</div>" ; myflush() ;
	if ( count ( $pageids ) > 1 ) {
		$pageid = implode ( ',' , $pageids ) ;
		$pageid = " IN ( $pageid ) " ;
	} else {
		$pageid = implode ( ',' , $pageids ) ;
		$pageid = " = $pageid " ;
	}

#	print "$pageid : $ns / $language , $project , $page<br/>" ;
	$sql = "select p1.pl_title AS title,count(*) as cnt from pagelinks p1,pagelinks p2 where p1.pl_namespace=0 and p1.pl_title=p2.pl_title and p2.pl_from $pageid and p2.pl_namespace=0 and not exists ( select * from page where page_title=p2.pl_title and page_namespace=0) group by p1.pl_title order by cnt desc" ;
#	print $sql ;

	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	print "<table>" ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$t = str_replace ( '_' , ' ' , $o->title ) ;
		print "<tr>" ;
		print "<td><a href='http://$language.$project.org/wiki/Special:WhatLinksHere/" . urlencode($o->title) . "'>" . $t . "</a></td>" ;
		print "<td>" . $o->cnt . "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

print "</body></html>" ;
