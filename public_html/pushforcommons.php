<?php

include "common.php" ;

function get_catscan_images ( $category , $project , $project_language , $depth = 3 ) {
	$ret = array () ;
	$url_cat = myurlencode ( str_replace ( ' ' , '+' , $category ) ) ;
	$url = "http://tools.wikimedia.de/~daniel/WikiSense/CategoryIntersect.php?wikifam=" ;
	if ( $project_language == 'commons' ) $project = 'wikimedia' ;
	if ( $project_language == 'commons' ) $url .= "{$project_language}" ;
	$url .= ".{$project}.org&basecat={$url_cat}&basedeep={$depth}&mode=iul&go=Scan&raw=on&userlang=en" ;
	if ( $project_language != 'commons' ) $url .= '&wikilang=' . $project_language ;

	$text = @file_get_contents ( $url ) ;
	if ( $text === false ) return $ret ; # Broken
	$lines = explode ( "\n" , $text ) ;
	foreach ( $lines AS $l ) {
		if ( trim ( $l ) == '' ) continue ;
		$l = explode ( "\t" , $l ) ;
		array_shift ( $l ) ;
		$ret[] = array_shift ( $l ) ;
	}
	return $ret ;
}


#____________________________________

$old_is_on_toolserver = $is_on_toolserver ;
$is_on_toolserver = false ;
$lang = trim ( strtolower ( get_request ( 'language' , 'de' ) ) ) ;
$project = trim ( strtolower ( get_request ( 'project' , 'wikipedia' ) ) ) ;
$max_results = get_request ( 'max_results' , 5 ) ;
$depth = get_request ( 'depth' , 3 ) ;
#$licenses = get_request ( 'licenses' , 'good,maybe,bad' ) ;
#$category = get_request ( 'category' , '' ) ;
#$startfrom = get_request ( 'startfrom' , '' ) ;
#$wikiuser = get_request ( 'wikiuser' , '' ) ;
$directupload = get_request ( 'directupload' , false ) ;
$welcomelog = get_request ( 'welcomelog' , false ) ;
#$interesting = isset ( $_REQUEST['interesting'] ) ;
#$licenses = explode ( ',' , strtolower ( str_replace ( ' ' , '' , $licenses ) ) ) ;
$thumbnail_width = 120 ;
$toynote = ' (using [[:Commons:User:Magnus Manske/PushForCommons|PushForCommons]])' ;

$newinput = true ;

if ( $newinput ) {
$mode_type = get_request ( 'mode_type' , 'random' ) ;
$mode_text = get_request ( 'mode_text' , '' ) ;
$startfrom = $category = $wikiuser = '' ;
if ( $mode_type == 'startfrom' ) $startfrom = $mode_text ;
if ( $mode_type == 'category' ) $category = $mode_text ;
if ( $mode_type == 'username' ) $wikiuser = $mode_text ;

$licenses = array() ;
if ( isset ( $_REQUEST['license_good'] ) ) $licenses[] = 'good' ;
if ( isset ( $_REQUEST['license_maybe'] ) ) $licenses[] = 'maybe' ;
if ( isset ( $_REQUEST['license_bad'] ) ) $licenses[] = 'bad' ;
$interesting = isset ( $_REQUEST['license_interesting'] ) ;
if ( !isset ( $_REQUEST['doit'] ) ) $licenses = 'good,maybe,bad' ;
else $licenses = implode ( ',' , $licenses ) ;
}

# Prepeare image list, if required
$image_list = array () ;
$use_list = false ;
if ( $startfrom != '' ) {
	$image_list = get_images_from ( $lang , $project , $startfrom ) ;
	$use_list = true ;
	$max = count ( $image_list ) ;
} else if ( $wikiuser != '' ) {
	$image_list = get_images_of_user ( $lang , $project , $wikiuser ) ;
	$use_list = true ;
	$max = count ( $image_list ) ;
} else if ( $category != '' ) {
	$image_list = get_catscan_images ( $category , $project , $lang , $depth ) ;
	$use_list = true ;
	$max = count ( $image_list ) ;
}

# Header
print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
$is_on_toolserver = $old_is_on_toolserver ;
print get_common_header ( "pushforcommons.php" ) ;
$is_on_toolserver = false ;
print "<h1>Push for commons</h1>" ;


if ( $newinput ) {
	$m_random = $mode_type == 'random' ? 'selected' : '' ;
	$m_category = $mode_type == 'category' ? 'selected' : '' ;
	$m_username = $mode_type == 'username' ? 'selected' : '' ;
	$m_startat = $mode_type == 'startat' ? 'selected' : '' ;
	
	$l_good = false !== strpos ( $licenses , 'good' ) ? 'checked' : '' ;
	$l_maybe = false !== strpos ( $licenses , 'maybe') ? 'checked' : '' ;
	$l_bad = false !== strpos ( $licenses , 'bad' ) ? 'checked' : '' ;
	$l_interesting = $interesting ? 'checked' : '' ;
	
	print "
	<form method='post'>
	<table border='1'>
	<tr><th>Language</th><td><input type='text' size='4' name='language' value='{$lang}'/></td></tr>
	<tr><th>Mode</th><td>
	<select name='mode_type'>
	<option {$m_random} value='random'>Random images</option>
	<option {$m_category} value='category'>Images from category ...</option>
	<option {$m_username} value='username'>Images uploaded by user ...</option>
	<option {$m_startat} value='startat'>Alphabetically, starting at ...</option>
	</select>
	<input type='text' size='30' name='mode_text' value='{$mode_text}'/>
	 (category depth <input type='text' size='1' name='depth' value='{$depth}'/>)
	</td></tr>
	<tr><th>Results</th><td><input type='text' size='4' name='max_results' value='{$max_results}'/></td></tr>
	<tr><th>Licenses</th><td>
	<input type='checkbox' name='license_good' value='1' {$l_good}/>Good 
	<input type='checkbox' name='license_maybe' value='1' {$l_maybe}/>Maybe 
	<input type='checkbox' name='license_bad' value='1' {$l_bad}/>Bad 
	<input type='checkbox' name='license_interesting' value='1' {$l_interesting}/>Interesting 
	<tr><td/><td><input type='submit' name='doit' value='Run'/></td></tr>
	</table>
	</form>
	" ;
	if ( !isset ( $_REQUEST['doit'] ) ) exit ;
} else {
	print "<p>A tool to mass-copy suitable images to commons. Can be called like this (default settings; add the <i>first</i> parameter with <i>?</i>, the other ones with <i>&</i>):<br/>" .
			"<i>pushforcommons.php?language=de&project=wikipedia&max=5&licenses=good,maybe,bad</i><br/>" .
			"You can also scan a group of images starting with a keyword: <i>pushforcommons.php?language=de&startat=SomeTitleOfYourLiking</i><br/>" .
			"Or you can avoid the \"boring\" images (good licence, nothing under that name on commons) with: <i>pushforcommons.php?max=50&interesting</i><br/>" .
			"Or have a list of the images you (or anyone) uploaded on a wiki: <i>pushforcommons.php?language=en&wikiuser=Your_user_name_on_en</i><br/>" .
			"Or have a list of the images in a category (and its subcategories): <i>pushforcommons.php?language=en&category=Some_category</i> (you can add <i>&depth=X</i> for subcategories; default is 3).</p>" ;
}

db_increase_usage_counter ( 'pushforcommons' ) ;
print db_get_usage_counter ( 'pushforcommons' ) ;

if ( $wikiuser != "" ) {
	$user_url = get_wikipedia_url ( $lang , 'User:' . $wikiuser , '' , $project ) ;
	print "<h3>Images by <a href='{$user_url}'>{$wikiuser}</a> on {$lang}.{$project}</h3><p><i>Note : this might be slightly outdated due to server lag</i></p>" ;
} else if ( $category != "" ) {
	$cat_url = get_wikipedia_url ( $lang , 'Category:' . $category , '' , $project ) ;
	print "<h3>Images in category <a href='{$cat_url}'>{$category}</a> and subcategories (depth {$depth}) on {$lang}.{$project}</h3><p><i>Note : this might be slightly outdated due to server lag</i></p>" ;
}

myflush () ;

# WelcomeLog sanity checks & initialization
if ( $welcomelog && $wikiuser == '' ) {
	print "<font color='red'>WelcomeLog only works for a single wikiuser - ignoring.</font><br/>" ;
	$welcomelog = false ;
}
if ( $welcomelog && $lang != 'commons' ) {
	print "<font color='red'>WelcomeLog only works for commons - ignoring.</font><br/>" ;
	$welcomelog = false ;
}
if ( $welcomelog ) {
	$url = "welcomelog.php" ;
	print "<form id='welcomelog' method=post enctype='multipart/form-data' action='{$url}'>" ;
	print "<input type='hidden' name='wikiuser' value='" . urlencode ( $wikiuser ) . "'/>" ;
}

$licenses = explode ( ',' , $licenses ) ;

# Now start da table
$column_width = $thumbnail_width + 10 ;
print "<table border='1' width='100%'>" ;
print "<tr><th width='15%'>File</th><th width='{$column_width}'>Thumbnail</th><th width='100%'>Description</th><th width='{$column_width}'>Commons</th></tr>" ;
$images = array () ;
$did_that = array () ;
$thecounter = 0 ;
while ( count ( $images ) < $max ) {
	if ( $thecounter >= $max_results ) break ;


	$timestamp = "" ;
	if ( $use_list && count ( $image_list ) == 0 ) break ; # List is done
	
	# Next image
	if ( $use_list ) {
		$i = new ImageData ( array_shift ( $image_list ) , $lang , $project ) ;
	} else {
		$i = new ImageData ;
		$i->make_random_image ( $lang , $project ) ;
	}
	
	if ( isset ( $did_that[$i->title] ) ) continue ; # Did that image in this session already
	if ( !$i->is_image() AND !$i->is_sound_file() ) continue ; # Only real images please!
	if ( !myfileexists ( $i->get_url() ) ) continue ; # File was deleted?

	$license = $i->check_license() ;

	# Image on commons?
	$ic = new ImageData ( $i->title , "commons" ) ;
	if ( $lang != 'commons' ) $is_on_commons = myfileexists ( $ic->get_url() ) ;
	else $is_on_commons = false ;
	if ( $interesting ) { # Interesting?
		if ( !$is_on_commons AND $license == 'good' ) continue ;
	}
	if ( $is_on_commons AND $i->is_equal_to ( $ic ) ) $equals_commons_file = true ;
	else $equals_commons_file = false ;

	# Show this?
	if ( !in_array ( $license , $licenses ) ) continue ;
#	print "<tr><td>TESTING{$i->title} : {$license}</td></tr><br/>" ; myflush() ; #continue ;
	if ( $interesting ) { # Interesting?
		if ( $is_on_commons AND $license == 'good' AND $i->has_commons_tag AND $equals_commons_file ) {
			$i->cleanup () ;
			$ic->cleanup () ;
			continue ;
		}
	}

	# Let's do it!
	$images[] = $i ;
	$did_that[$i->title] = 1 ;

	# Prepare buttons
	$buttons = array () ;
	$commons_buttons = array () ;
	if ( !$welcomelog ) { # Welcomelog must suppress all buttons, because each button has its own form
		$button_sep = ' ' ;
		if ( $license == 'maybe' ) { # Suggest licenses
			foreach ( $i->possible_licenses AS $pl ) {
				$tag = '{{' . $pl . '}}' ;
				$buttons[] = $i->get_edit_button ( $pl , $tag . $toynote , "\n" . $tag ) ;
			}
			if ( $lang == 'de' ) $buttons[] = $i->get_edit_button ( 'BLU' , '{{BLU}}' . $toynote , "\n{{BLU}}" ) ;
			else $buttons[] = $i->get_edit_button ( 'Unknown' , '{{Unknown}}' . $toynote , "\n{{Unknown}}" ) ;
		}
		if ( $lang != 'commons' && !$is_on_commons AND $license != 'bad' AND !$i->has_commons_tag ) { # Allow commons upload
			$commons_buttons[] = $i->get_commons_upload_button ( "Upload at commons" , $directupload ) ;
			$commons_buttons[] = $i->get_edit_button ( 'NC' , '{{NC}}' . $toynote , "\n{{subst:NC}}" ) ;
			$commons_buttons[] = $i->get_edit_button ( 'NCT' , '{{NCT}}' . $toynote , "\n{{subst:NCT}}" ) ;
		}
		if ( $lang != 'commons' && $is_on_commons AND !$equals_commons_file AND !$i->has_template ( 'ShadowsCommons' ) ) { # Shadows commons
			$buttons[] = $i->get_edit_button ( 'ShadowsCommons' , '{{ShadowsCommons}}' . $toynote , "\n{{ShadowsCommons}}" ) ;
		}
		if ( $lang != 'commons' && $is_on_commons AND $equals_commons_file AND !$i->has_commons_tag ) { # Now commons
			$buttons[] = $i->get_edit_button ( 'NC' , '{{NC}}' . $toynote , "\n{{NC}}" ) ;
			$buttons[] = $i->get_edit_button ( 'NCT' , '{{NCT}}' . $toynote , "\n{{NCT}}" ) ;
		}
		if ( $license == 'bad' ) {
			$buttons[] = $i->get_delete_link ( '' , '[DELETE]' ) ;
		}
	} else { # Welcomelog
		$button_sep = '<br/>' ;
		$id = count ( $image_list ) ;
		$buttons[] = "<input type='checkbox' name='nsd[{$id}]'> No source" ;
		$buttons[] = "<input type='checkbox' name='nld[{$id}]'> No license" ;
		$commons_buttons[] = "<input type='hidden' name='desc[{$id}]' value='" . urlencode ( $i->get_description() ) . "'>" ;
		$commons_buttons[] = "<input type='hidden' name='image[{$id}]' value='" . urlencode ( $i->title ) . "'>" ;
	}
	
	# START OUTPUT
	$out = '<tr>' ;

	# Name, link, buttons
	$out .= '<th>' ;
	$out .= $i->get_link () . "<br/>" ;
	$out .= implode ( $button_sep , $buttons ) ;
	$out .= '</th>' ;

	
	# Thumbnail
	$out .= '<td align="center" valign="center">' ;
	$out .= $i->get_thumbnail_link ( $thumbnail_width , $thumbnail_width , false ) ;
	$out .= '</td>' ;

	# Description
	if ( $is_on_commons AND $equals_commons_file ) $license = 'oncommons' ;
	$bgcolor = $markup_colors[$license] ;
	$desc = $i->get_description () ;
	$desc = str_replace ( "\n\n\n" , "\n\n" , $desc ) ;
	$desc = str_replace ( "\n" , '<br/>' , $desc ) ;
	$desc = str_replace ( '&' , ' &' , $desc ) ;
	$desc = str_replace ( '/' , '/ ' , $desc ) ;
	$out .= '<td style="width: 100%; background-color: ' . $bgcolor . '; font-size: 10px;" valign="top">' ;
	$out .= $desc ;
	$out .= '</td>' ;
	
	# Commons
	if ( $is_on_commons ) {
		if ( $equals_commons_file ) $bgcolor = $markup_colors['good'] ;
		else $bgcolor = $markup_colors['bad'] ;
		$out .= '<td align="center" valign="center" style="background-color: ' . $bgcolor . '">' ;
		$out .= $ic->get_thumbnail_link ( $thumbnail_width , $thumbnail_width , false ) . '<br/>' ;
		$out .= $ic->get_link () ;
		$out .= '</td>' ;
	} else {
		$bgcolor = $markup_colors['unknown'] ;
		$out .= '<td align="center" valign="center" style="background-color: ' . $bgcolor . '">' ;
		$out .= implode ( ' ' , $commons_buttons ) ;
		$out .= "</td>" ;
	}

	$out .= '</tr>' ;
	print $out ;
	myflush () ;
	$thecounter++ ;
	

	$i->cleanup () ;
	$ic->cleanup () ;
#	break ; # Testing
}
print "</table>" ;
if ( $welcomelog ) {
	print "<input type='submit' name='run' value='Run WelcomeLog'/> &nbsp; " ;
	print "<input type='checkbox' name='autoedit' value='1'> Perform edits automatically" ;
	print "</form>" ;
} else print "<p>Done!</p>" ;

print "</body></html>" ;

?>