<?PHP
error_reporting ( E_ALL ) ;

include_once ( 'queryclass.php' ) ;

$mode = get_request ( 'mode' , 'pnd_barch_foto' ) ;

print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "de_bio.php" ) ;

$t = trim ( file_get_contents ( "../apper.dbkey" ) ) ;
$mysql_password_apper = $t ;
$db = 'u_apper' ;
$server = "sql" ;
$user = "apper" ;

$mysql_con_apper = mysql_connect ( $server , $user , $mysql_password_apper ) ;

$ba = array () ;
$sql = 'SELECT * FROM w_ba WHERE wikipedia="" AND pnd != ""' ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$o->origin = 'ba' ;
	$ba[$o->pnd] = $o ;
}

$ft = array () ;
$sql = 'SELECT * FROM w_ft WHERE wikipedia="" AND pnd != ""' ;
$res = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
while ( $o = mysql_fetch_object ( $res ) ) {
	$o->origin = 'ft' ;
	$ft[$o->pnd] = $o ;
}

function get_entry ( $d , $link_name = true ) {
	$ret = explode ( ', ' , $d->name , 2 ) ;
	$name = htmlspecialchars_decode ( $ret[1] . ' ' . $ret[0] ) ;
	$name = preg_replace ( '/<\d+>/' , '' , $name ) ;
	$url = "http://de.wikipedia.org/wiki/" . myurlencode ( $name ) ;
	$pnd_url = 'https://portal.d-nb.de/opac.htm?method=simpleSearch&query=' ;
	$pnd_url .= urlencode ( 'atr=' . $d->pnd ) . "+OR+" ;
	$pnd_url .= urlencode ( 'nid=' . $d->pnd ) ;
	$ret = $name ;

	$born = $d->born ;
	$died = $d->died ;
	if ( !isset ( $born ) ) $born = '' ;
	if ( !isset ( $died ) ) $died = '' ;
	$dates = '' ;
	if ( $born != '' && $died != '' ) $dates = ", $born	- $died" ;
	else if ( $born != '' ) $dates = ", *$born" ;
	else if ( $died != '' ) $dates = ", &dagger;$died" ;


	if ( $link_name ) {
		$a = db_get_existing_pages ( array ( $name ) , 'de' , 'wikipedia' ) ;
		$col = count($a) == 0 ? 'red' : 'blue' ;
		$ret = "<a style='color:$col' href='$url'>" . $name . "</a>" ;
	}
	$ret .= $dates ;
	if ( $d->short != '' ) $ret .= ": " . $d->short ;
	if ( $d->country != '' ) $ret .= " (" . $d->country . ")" ;
	if ( $d->pnd != '' && $link_name ) $ret .= " [PND <a href='$pnd_url'>" . $d->pnd . "</a>]" ;
	if ( $d->origin == 'ft' ) $ret .= " [<a href='http://toolserver.org/~apper/fotothek/index.php?id={$d->id}'>Fotothek-Tool</a>]" ;
	
	if ( $link_name ) {
		$search_url = array () ;
		$search_url[] = $name ;
		
		$a = array () ;
		preg_match_all ( '/\b(\d{3,4})\b/' , $dates , $a ) ;
		$a = $a[1] ;
		if ( count ( $a ) < 3 ) {
			if ( count ( $a ) > 0 ) $search_url[] = $a[0] ;
			if ( count ( $a ) > 1 ) $search_url[] = $a[1] ;
		}

		$search_url = urlencode ( implode ( ' ' , $search_url ) ) ;
		$ret .= " <a href='http://www.google.de/search?q=$search_url'><i>google</i></a>" ;
	}
	
	return utf8_encode ( trim ( $ret ) ) ;
}

function get_self_link ( $key , $text ) {
	global $mode ;
	if ( $mode == $key ) return "<b>$text</b>" ;
	$url = "./de_bio.php" ;
	if ( $mode != '' ) $url .= "?mode=$key" ;
	return "<a href='$url'>$text</a>" ;
}

// DUMMY
function text2searchtext ( $text ) {
	$text = strtolower ( trim ( str_replace ( "," , "" , $text ) ) ) ;
	$text = 'aaa' . implode ( ' aaa' , explode ( ' ' , $text ) ) ;
	return $text ;
}

// This function by APPER
function getPDSearchQuery ( $text ) {
	$query_text = "";
	
	// remove "-" without " " before it
	$text = preg_replace('/([^ ])\-([^ ])/', '$1 $2', $text);      
	
	$temp = explode(" ", text2searchtext($text));
	
	while (list($key, $val) = each($temp)) {
		$val = trim($val);
		if ($val == "") { continue; }
		
		if (substr($val, 0, 1) == "-") { 
			$query_text .= " " . mysql_escape_string($val);
		} else {
			$query_text .= " +" . mysql_escape_string($val);
		}
	}

	if ($query_text == "") { $query_text = " 1 "; }
	else { $query_text = "MATCH (pd_pd.name_search) AGAINST ('{$query_text}' IN BOOLEAN MODE)"; }
	
	$query_text = "SELECT id, title, description, b_year, d_year FROM `pd_pd` WHERE " . $query_text . " AND state<100 ORDER BY name LIMIT 0,20;";    
	return $query_text;  
}


// ***************
// MAIN PROGRAM

$had_that = array () ;

print "<h1>Benötigte (?) Biographien</h1>" ;

$l = array() ;
$l[] = get_self_link ( 'pnd_barch_foto' , 'PND, BArch, und Fotothek' ) ;
$l[] = get_self_link ( 'pnd_foto' , 'PND und Fotothek' ) ;
$l[] = get_self_link ( 'barch_foto' , 'Gleiche Namen in BArch und Fotothek' ) ;
$l[] = get_self_link ( 'foto_cand' , 'Fotothek-Kandidaten' ) ;
$l[] = get_self_link ( 'foto_guess' , 'Massen-Vorauswahl' ) ;
print "Funktionen : " . implode ( " | " , $l ) ;

$pbf = array () ;
foreach ( $ba AS $pnd => $data ) {
	if ( !isset ( $ft[$pnd] ) ) continue ;
	if ( $mode == 'pnd_barch_foto' ) $pdb[] = "<li>" . get_entry ( $ft[$pnd] ) . "<br/>" . get_entry ( $data , false ) . "</li>" ;
	$had_that[$pnd] = 1 ;
}

if ( $mode == 'pnd_barch_foto' ) {
	print "<h2>PND, BArch, und Fotothek</h2><ol>" ;
	foreach ( $pdb AS $s ) {
		print $s ;
	}
	print "</ol>" ;
}

if ( $mode == 'pnd_foto' ) print "<h2>PND und Fotothek, mit Geburts- und Sterbedaten</h2><ol>" ;
foreach ( $ft AS $pnd => $data ) {
	if ( isset ( $had_that[$pnd] ) ) continue ;
	if ( $data->born == '' ) continue ;
	if ( $data->died == '' ) continue ;
	if ( $mode == 'pnd_foto' ) print "<li>" . get_entry ( $data ) . "</li>" ;
	$had_that[$pnd] = 1 ;
}
if ( $mode == 'pnd_foto' ) print "</ol>" ;

myflush();

if ( $mode == 'barch_foto' ) {
	print "<h2>Gleiche Namen in BArch und Fotothek, einer davon mit PND, mit Geburts- und Sterbedaten</h2>" ;
	print "<i>VORSICHT: Gleicher Name bedeutet nicht gleiche Person!</i><ol>" ;
	
	$sql = 'select w_ba.name AS name,w_ba.pnd AS ba_pnd,w_ft.pnd AS ft_pnd,w_ba.short AS s1,w_ft.short AS s2,born,died from w_ba,w_ft where w_ba.name=w_ft.name and w_ba.wikipedia = "" and w_ft.wikipedia = "" and ( w_ba.pnd != "" or w_ft.pnd != "" )' ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$o->short = $o->s1 . "; " . $o->s2 ;
		$o->pnd = $o->ba_pnd ;
		if ( $o->pnd == '' ) $o->pnd = $o->ft_pnd ;
		else if ( $o->ft_pnd != '' && $o->pnd != $o->ft_pnd ) continue ; // Mismatching PND
		if ( isset ( $had_that[$o->pnd] ) ) continue ;
		if ( $o->born == '' ) continue ;
		if ( $o->died == '' ) continue ;
		print "<li>" . get_entry ( $o ) . "</li>" ;
		$had_that[$o->pnd] = 1 ;
	}
	print "</ol>" ;
	myflush() ;
}

if ( $mode == 'foto_cand' ) {
	print "<h2>Fotothek-Kandidaten (unter demselben Namen bei BArch mit PND)</h2><ol>" ;
	$sql = 'SELECT w_ft.name AS name,w_ft.id AS id FROM w_ba,w_ft WHERE w_ft.state=10 AND w_ba.name=w_ft.name AND w_ft.pnd="" AND w_ba.pnd != ""' ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		print "<li><a href='http://toolserver.org/~apper/fotothek/index.php?id={$o->id}'>" . utf8_encode ( $o->name ) . "</a></li>" ;
	}
	print "</ol>" ;
}

if ( $mode == 'foto_guess' ) {
	$limit = 50 ;
	$unique_text = 'JOIhIOUGHFIUGDFIYGKUfgKUFUKYGKUYTU6876786866' ;
	print "<h2>Fotothek-Kandidaten (haben Geburts- und Sterbedatum), 1-$limit</h2>" ;
	print "<table border='1' style='font-size:10pt'>" ;
	$sql = 'SELECT * FROM w_ft WHERE state=10 AND ( born!="" AND died!="" ) LIMIT ' . $limit ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
	$keys = array ( 'name' , 'country' , 'short' ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		foreach ( $keys AS $k ) $o->$k = utf8_encode ( htmlspecialchars_decode ( $o->$k ) ) ;
		$o->name = preg_replace ( '/<\d+>/' , '' , $o->name ) ;
		
		$found_pnd = false ;
		$found_wiki = false ;
		
		$url = "http://toolserver.org/~apper/fotothek/load_dnb.php?name=" . urlencode ( $o->name ) ;
		$pnd_html = file_get_contents ( $url ) ;
		
		$y1 = array () ;
		preg_match_all ( '/\b(\d{3,4})\b/' , $o->born , $y1 ) ;
		if ( count ( $y1[1] ) == 1 ) $y1 = $y1[1][0] ;
		else $y1 = $unique_text ;
		
		$y2 = array () ;
		preg_match_all ( '/\b(\d{3,4})\b/' , $o->died , $y2 ) ;
		if ( count ( $y2[1] ) == 1 ) $y2 = $y2[1][0] ;
		else $y2 = $unique_text ;
		
		// Get PND data
		$pnds = array () ;
		preg_match_all ( '/<div class="resulttext">(.+?)<\/div>/' , $pnd_html , $pnds ) ;
		$pnd_html = "<table border='0'>" ;
		$checked = 'checked' ;
		foreach ( $pnds[1] AS $p ) {
			$pnd = '' ;
			if ( preg_match ( '/\((.+?);/' , $p , $pnd ) ) {
				$found_pnd = true ;
				$pnd = $pnd[1] ;
				$ch = '' ;
				$col = '' ;
				if ( preg_match ( "/$y1/" , $p ) and preg_match ( "/$y2/" , $p ) ) {
					$ch = 'checked' ;
					$col = 'style="background:#DDDDDD"' ;
					$checked = '' ;
				}
				$id = "pnd.{$o->id}.$pnd" ;
				$pnd_html .= "<tr><td $col><input type='radio' name='PND' value='$pnd' id='$id' $ch/></td><td $col><label for='$id'>$p</label></td></tr>" ;
			}
		}
//		print "<hr/>$pnd_html<pre>" ;		print_r ( $pnds ) ;		print "</pre>" ;
		$id = "pnd.{$o->id}.none" ;
		$pnd_html .= "<tr><td><input type='radio' name='PND' value='' id='$id' $checked /></td><td><label for='$id'>Kein PND</label></td></tr></table>" ;
		$pnd_html .= "<input type='hidden' name='id' value='{$o->id}' />" ;
		
		// Get wikipedia data
		$wp_html = "<table border='0'>" ;
		$checked = 'checked' ;
		$sql = getPDSearchQuery ( $o->name ) ;
//		print "$sql<br/>" ;
		$res2 = my_mysql_db_query ( $db , $sql , $mysql_con_apper ) ;
		while ( $p = mysql_fetch_object ( $res2 ) ) {
			$found_wiki = true ;
			$desc = utf8_encode ( $p->description ) ;
			$title = utf8_encode ( $p->title ) ; 
			$url = "http://de.wikipedia.org/wiki/" . myurlencode ( $title ) ;
			$ch = '' ;
			$col = '' ;
			if ( preg_match ( "/$y1/" , $p->b_year ) and preg_match ( "/$y2/" , $p->d_year ) ) {
				$ch = 'checked' ;
				$col = 'style="background:#DDDDDD"' ;
				$checked = '' ;
			}
			$id = "wp.{$o->id}.{$p->id}" ;
			$wp_html .= "<tr><td $col><input type='radio' name='WP' value='$title' id='$id' $ch/></td><td><label for='$id'><a href='$url'>{$title}</a> ({$p->b_year}-{$p->d_year})<br/>$desc</label></td></tr>" ;
		}
		$id = "wp.{$o->id}.none" ;
		$wp_html .= "<tr><td><input type='radio' name='WP' value='' id='$id' $checked /></td><td><label for='$id'>Kein Wikipedia-Eintrag</label></td></tr></table>" ;
		
		$do_html = "<input type='text' name='comment' value='' /><br/><input type='submit' name='submit1' value='Los!' />" ;
		
		if ( !$found_wiki and !$found_pnd ) continue ; // Don't show empty rows
		
		$bs = 'style="margin-bottom:5px"' ;
		print "<tr>" ;
		print "<form target='_blank' action='http://toolserver.org/~apper/fotothek/index.php' method='post'>" ; // submit.php
		print "<td><a href='http://toolserver.org/~apper/fotothek/index.php?id={$o->id}'>{$o->name}</a></td>" ;
		print "<td>{$o->born}</td>" ;
		print "<td>{$o->died}</td>" ;
		print "<td rowspan='2' $bs>$pnd_html</td>" ;
		print "<td rowspan='2' $bs>$wp_html</td>" ;
		print "<td rowspan='2' $bs>$do_html</td>" ;
		print "</tr><tr>" ;
		print "<td colspan='3' $bs>{$o->short}" ;
		print " (<i>{$o->country}</i>)</td>" ;
		print "</form></tr>" ;
		myflush();
	}
	print "</table>" ;
}

print "</body></html>" ;
