<?PHP

include "common.php" ;

$dbn = 'u_magnus_yarrow' ;
$dbu = db_get_con ( $dbn ) ;

$script = 'bibliophil.php' ;
$authors = array() ;
$id2title = array() ;

function load_id2title () {
	global $dbn , $dbu , $id2title ;
	$sql = "SELECT id,value FROM bibisbn WHERE tag='titel'" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$id2title[$o->id] = $o->value ;
	}
}

function form_search ( $k , $name , $q ) {
	$ret = "<form method='get' action='$script'>" ;
	$ret .= "<input type='hidden' name='action' value='search_$k' />" ;
	$ret .= "<input type='text' name='query_$k' value='$q' />" ;
	$ret .= "<input type='submit' name='doit' value='$name' />" ;
	$ret .= "</form>" ;
	return $ret ;
}

function add2authors ( $name , $id , &$authors ) {
	$linked = 0 ;
	$heraus = 0 ;
	if ( substr ( $name , 0 , 2 ) == '[[' ) {
		$linked = 1 ;
		$name = substr ( $name , 2 ) ;
		$name = array_shift ( explode ( '|' , $name ) ) ;
		$name = array_shift ( explode ( ']]' , $name ) ) ;
		$name = trim ( $name ) ;
	}
	if ( $heraus == 0 ) $name = preg_replace ( '/\s*\(Hg\.*\)/i' , '' , $name , 1 , $heraus ) ;
	if ( $heraus == 0 ) $name = preg_replace ( '/\s*\(Hrsg\.*\)/i' , '' , $name , 1 , $heraus ) ;
	if ( $heraus == 0 ) $name = preg_replace ( '/\s*\(Herausgeber\)/i' , '' , $name , 1 , $heraus ) ;
	if ( !isset ( $authors[$name] ) ) {
		$authors[$name] = '' ;
		$authors[$name]->name = $name ;
		$authors[$name]->count = 0 ;
		$authors[$name]->links = 0 ;
		$authors[$name]->linked = array() ;
		$authors[$name]->heraus = 0 ;
		$authors[$name]->ids = array() ;
	}
	$authors[$name]->count++ ;
	$authors[$name]->links += $linked ;
	$authors[$name]->linked[$id] = $linked ;
	$authors[$name]->heraus += $heraus ;
	$authors[$name]->ids[$id] = $id ;
}

function load_authors () {
	global $dbu , $dbn , $authors ;
	$sql = "SELECT id,value FROM bibisbn WHERE tag='autor'" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$v = $o->value ;
		$id = $o->id ;
		$name = $v ;
		add2authors ( $name , $id , $authors ) ;
	}
}

function print_stats () {
	global $dbu , $dbn , $authors , $script ;
	load_authors() ;

	print "<h3>Statistik</h3><ul>" ;

	print "<table border='1' style='float:right;font-size:8pt'>" ;
	print "<caption>Veröffentlichungen pro Jahr in der Datenbank</caption>" ;
	print "<tr><th>Jahr</th><th colspan=2>Anzahl Bücher</th></tr>" ;
	$sql = "SELECT count(*) AS num,value FROM bibisbn WHERE tag='jahr' AND value LIKE '____' GROUP BY value ORDER BY value DESC" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$style = "background:#BBBBBB;height:12px;width:" . ( $o->num * 5 ) . "px" ;
		print "<tr><td><a href='./$script?action=year2book&year={$o->value}'>{$o->value}</a></td>" ;
		print "<td>{$o->num}</td><td><div style='$style'></div></td></tr>" ;
	}
	print "</table>" ;
	
	$sql = "SELECT count(DISTINCT id) AS num FROM bibisbn" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$o = mysql_fetch_object ( $res ) ;

	print "<li>" . $o->num . " individuelle Bücher" ;
	print form_search ( 'book' , 'Buchsuche' , '' ) ;
	print form_search ( 'isbn' , 'ISBN-Suche' , '' ) ;
	print "</li>" ;
	
	
	print "<li>" . count ( $authors ) . " <a href='$script?action=search_author'>individuelle Autoren</a>" ;
	print form_search ( 'author' , 'Autorensuche' , '' ) ;
	print "</li>" ;


	$sql = "SELECT count(DISTINCT value) AS num FROM bibisbn WHERE tag='verlag'" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$o = mysql_fetch_object ( $res ) ;

	print "<li>" . $o->num . " <a href='$script?action=search_verlag'>Verlage</a>" ;
	print form_search ( 'verlag' , 'Verlagssuche' , '' ) ;
	print "</li>" ;

	print "</ul>" ;
}

function prettify_isbn ( $isbn ) {
return $isbn ; // SHORTCUT
	$i = preg_replace ( '/[^\dxX]/' , '' , $isbn ) ;
	if ( $i == '' ) return $isbn ;
	$t = file_get_contents ( "http://toolserver.org/gradzeichen/IsbnCheckAndFormat?ISBN=$i&FormatOnlyRaw=yes" ) ;
	if ( substr ( $t , 0 , 3 ) == 'Not' ) return $isbn ;
	return $t ;
}

function search_verlag () {
	global $dbu , $dbn , $script ;
	
	$cond = "" ;
	$q = get_request ( 'query_verlag' , '' ) ;
	if ( $q != '' ) {
		make_db_safe ( $q ) ;
		$q = str_replace ( '_' , ' ' , $q ) ;
		$cond = "value LIKE \"%$q%\" AND " ;
	}

	$data = array () ;
	$sql = "SELECT count(*) AS num,value FROM bibisbn WHERE $cond tag='verlag' GROUP BY value ORDER BY value" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$data[$o->value] = $o->num ;
	}

	print "<h3>Verlagssuche</h3>" ;
	print form_search ( 'verlag' , 'Verlagssuche' , $q ) ;
	print "<table border=1><tr><th>Verlag</th><th>Bücher</th></tr>" ;
	foreach ( $data AS $verlag => $num ) {
		print "<tr>" ;
		print "<td>" ;
		print $verlag ;
		print "</td>" ;
		print "<td>" ;
		print "<a href='./$script?action=verlag2book&verlag=".urlencode($verlag)."'>$num</a>" ;
		print "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

function search_isbn () {
	$q = get_request ( 'query_isbn' , '' ) ;
	$id = preg_replace ( '/[^\dxX]/' , '' , $q ) ;
	show_books ( '' , array ( $id ) ) ;
}

function search_book () {
	global $dbu , $dbn ;
	$q = get_request ( 'query_book' , '' ) ;
	make_db_safe ( $q ) ;
	$q = str_replace ( '_' , ' ' , $q ) ;
	$sql = "SELECT DISTINCT id FROM bibisbn WHERE value LIKE \"%$q%\" AND tag IN ('titel','titelerg','band','sammelwerk','originaltitel')" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$ids = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ids[$o->id] = $o->id ;
	}
	show_books ( $q , $ids ) ;
}

function show_books ( $q , $ids ) {
	global $dbu , $dbn , $script ;

	$sql = "SELECT * FROM bibisbn WHERE id IN ('".implode("','",$ids)."')" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$books = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		if ( $o->tag == 'autor' ) {
			$books[$o->id][$o->tag][$o->value] = $o->value ;
		} else $books[$o->id][$o->tag] = $o->value ;
	}
	
	print "<h3>Bücher</h3>" ;
	print form_search ( 'book' , 'Buchsuche' , $q ) ;
	print "<ol>" ;
	foreach ( $books AS $id => $book ) {
		$authors = array () ;
		foreach ( $book['autor'] AS $v ) {
			add2authors ( $v , $id , $authors ) ;
		}
	
		$erg = $book['titelerg'] ;
		if ( isset ( $erg ) ) {
			$erg = " <small>$erg</small>" ;
		} else $reg = '' ;
		
		$isbn = $book['isbn'] ;
		if ( !isset ( $isbn ) or $isbn == '' ) $isbn = $id ;
	
		print "<li>" ;
		print "<b>" . $book['titel'] . "</b>$erg (ISBN <a target='_blank' href='http://de.wikipedia.org/wiki/Vorlage:BibISBN/$id'>$isbn</a>)<br/>" ;
		$out = array () ;
		foreach ( $authors AS $a ) {
			$name = $a->name ;
			$s = "<a href='./$script?action=search_author&query_author=".urlencode($name)."'>$name</a>" ;
			if ( $a->links > 0 ) $s .= " [<a target='_blank' href='http://de.wikipedia.org/wiki/".myurlencode($name)."'>WP</a>]" ;
			$out[] = $s ;
		}
		print implode ( ', ' , $out ) ;
		print "<ul>" ;
		foreach ( $book AS $k => $v ) {
			if ( $k == 'titel' ) continue ;
			if ( $k == 'autor' ) continue ;
			if ( $k == 'titelerg' ) continue ;
			if ( $k == 'isbn' ) continue ;
			if ( $k == 'verlag' ) $v = "<a href='./$script?action=verlag2book&verlag=".urlencode($v)."'>$v</a>" ;
			if ( $k == 'jahr' and strlen ( $v ) != '4' ) $v = "<span style='color:red'>$v</span>" ;
			$k = ucfirst ( $k ) ;
			print "<li>$k : $v</li>" ;
		}
		print "</ul>" ;
		print "</li>" ;
	}
	print "</ol>" ;
}

function search_author () {
	global $authors , $id2title , $script ;
	load_authors() ;
	load_id2title();
	$q = get_request ( 'query_author' , '' ) ;
	$ak = array_keys ( $authors ) ;
	sort ( $ak ) ;
	print "<h3>Autorenliste</h3>" ;
	print form_search ( 'author' , 'Autorensuche' , $q ) ;
	print "<ol>" ;
	foreach ( $ak AS $author ) {
		if ( $q != '' and false === stristr ( $author , $q ) ) continue ;
		print "<li>" ;
		if ( $authors[$author]->links == 0 ) print "<b>$author</b>" ;
		else print "<b>$author</b> [<a target='_blank' href='http://de.wikipedia.org/wiki/" . myurlencode ( $author ) . "'>WP</a>] " ;
		$i = array () ;
		foreach ( $authors[$author]->ids AS $id ) {
			$isbn = prettify_isbn ( $id ) ;
			$s = "<a href='./$script?action=search_book&query_book=".urlencode($id2title[$id])."'>{$id2title[$id]}</a>" ;
			$s .= " (<a target='_blank' href='http://de.wikipedia.org/wiki/Vorlage:BibISBN/$id'>$isbn</a>)" ;
			if ( $authors[$author]->links > 0 && $authors[$author]->linked[$id] == 0 ) {
				$s .= " (Autor nicht verlinkt!)" ;
			}
			$i[] = $s ;
		}
		if ( count ( $i ) == 1 ) {
			print " : " . array_shift ( $i ) ;
		} else {
			print "<ul><li>" ;
			print implode ( "</li><li>" , $i ) ;
			print "</li></ul>" ;
		}
		print "</li>" ;
	}
	print "</ol>" ;
}

function verlag2book () {
	global $dbu , $dbn , $script ;
	$verlag = get_request ( 'verlag' , '' ) ;
	make_db_safe ( $verlag ) ;
	$verlag = str_replace ( '_' , ' ' , $verlag ) ;
	$data = array () ;
	$sql = "SELECT DISTINCT id FROM bibisbn WHERE tag='verlag' AND value='$verlag'" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$data[$o->id] = $o->id ;
	}
	show_books ( "" , $data ) ;
}

function year2book () {
	global $dbu , $dbn , $script ;
	$year = get_request ( 'year' , '' ) ;
	make_db_safe ( $year ) ;
	$data = array () ;
	$sql = "SELECT DISTINCT id FROM bibisbn WHERE tag='jahr' AND value LIKE '%$year%'" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$data[$o->id] = $o->id ;
	}
	show_books ( "" , $data ) ;
}

$action = get_request ( 'action' , 'stats' ) ;

print "<html><head></head><body>" ;
print get_common_header ( $script , "BiblioPhil" ) ;

if ( $action == 'stats' ) print "<h1>BiblioPhil</h1>" ;
else print "<h1><a href='./$script'>BiblioPhil</a></h1>" ;
print "<small>Daten basierend auf <a href='http://de.wikipedia.org/wiki/Wikipedia:BibRecord'>BibRecord</a>; wird alle 10 Minuten aktualisiert</small>" ;

if ( $action == 'stats' ) {
	print_stats();
} else if ( $action == 'search_book' ) {
	search_book () ;
} else if ( $action == 'search_isbn' ) {
	search_isbn () ;
} else if ( $action == 'search_author' ) {
	search_author () ;
} else if ( $action == 'search_verlag' ) {
	search_verlag () ;
} else if ( $action == 'verlag2book' ) {
	verlag2book () ;
} else if ( $action == 'year2book' ) {
	year2book () ;
}

print "</body></html>" ;

?>