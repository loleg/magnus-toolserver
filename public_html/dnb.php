<?PHP
error_reporting ( E_ALL ) ;
//$suppress_gz_handler = 1 ;

include_once ( 'queryclass.php' ) ;

function wikipre ( $t ) {
	$t = htmlspecialchars ( $t ) ;
	$t = str_replace ( "\n\n" , "<br/>" , $t ) ;
	return str_replace ( "\n" , "<br/>" , $t ) ;
}


$base_url = "http://en.wikipedia.org/w/index.php?title=User:Magnus_Manske/Dictionary_of_National_Biography/" ;
$page = get_request ( 'page' , '1' ) ;
$section = get_request ( 'section' , '1' ) ;


if ( strlen ( $page ) < 2 ) $page = "0$page" ;

$url = "$base_url$page&section=$section&action=raw" ;
$text = file_get_contents ( $url ) ;
$lines = explode ( "\n" , $text ) ;
$items = array () ;
foreach ( $lines AS $k => $v ) {
	if ( substr ( $v , 0 , 1 ) != ';' ) continue ;
	$l = explode ( ':' , trim ( substr ( $v , 1 ) ) , 2 ) ;
	if ( count ( $l ) != 2 ) continue ;
	$x = explode ( '[[' , ' ' . $l[0] , 2 ) ;
	$link = array_pop ( $x ) ;
	$x = array_pop ( $x ) ;
	$x = substr ( $x , 1 ) ;
//	$link = str_replace ( '[[' , '' , $l[0] ) ;
	$link = str_replace ( ']]' , '' , $link ) ;
	$link = explode ( '|' , trim ( $link ) , 2 ) ;
	$o = '' ;
	$o->page = $link[0] ;
	$o->name = count ( $link ) == 2 ? $link[1] : $link[0] ;
	$o->text = $l[1] ;
	$o->line = $k ;
	$o->pre = $x ;
	$o->v = $v ;
	$items[] = $o ;
}


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "dnb.php" ) . "\n" ;
myflush() ;

print "<h1>DNB page $page section $section</h1>" ;

if ( isset ( $_REQUEST['doit'] ) ) {
	$cb = $_POST['cb'] ;
	$name = $_POST['name'] ;
	$skip = $_POST['skip'] ;
	
	$out = "== Section $section ==\n" ;
	foreach ( $items AS $ik => $i ) {
		if ( isset ( $skip[$ik] ) ) { // Leave this line alone!
			$out .= $i->v . "\n" ;
			continue ;
		}
		$out .= ";" ;
		if ( isset ( $cb[$ik] ) ) $out .= "<s>" ;
		$out .= $i->pre ;
		$out .= "[[" ;
		if ( $name[$ik] == $i->name ) {
			$out .= $i->name ;
		} else {
			$out .= $name[$ik] . "|" . $i->name ;
		}
		$out .= "]]" ;
		if ( isset ( $cb[$ik] ) ) $out .= "</s>:<s>" ;
		else $out .= ":" ;
		$out .= $i->text ;
		if ( isset ( $cb[$ik] ) ) $out .= "</s>" ;
		$out .= "\n" ;
	}
	print "<textarea rows='20 cols='80' style='width:100%'>$out</textarea>" ;
	
	print cGetEditButton ( $out , "User:Magnus_Manske/Dictionary_of_National_Biography/$page" , 'en' , 'wikipedia' , "Cleanup of section $section, assisted by http://toolserver.org/~magnus/dnb.php" , "Click here to update" , false , false , true , false , $section ) ;
	
} else {
	print "<i>Usage : <b>dnb.php?page=X&section=Y</b> with <b>X</b>=1-12 and <b>Y</b>=1-100</i>" ;
	print "<p>Check the checkboxes for articles that exist and match the person in the DNB entry. Some checkboxes were pre-selected because of date matches. You can change link names in the respective text box. DNB entry in dark gray, Wikipedia page beginning in light gray.</p>" ;
	print "<form method='post' action='dnb.php'>" ;
	print "<input type='hidden' name='page' value='$page' />" ;
	print "<input type='hidden' name='section' value='$section' />" ;
	print "<ol>" ; 
	foreach ( $items AS $ik => $i ) {
		if ( substr ( $i->text , 0 , 3 ) == "<s>" ) {
			print "<input type='hidden' name='skip[$ik]' value='1' />" ;
			continue ;
		}
		
		print "<li style='margin-top:5px;border-top:1px dotted black'>" ; 
		
		$url = "http://en.wikipedia.org/w/index.php?section=0&action=raw&title=" . myurlencode ( $i->page ) ;
		$text = file_get_contents ( $url ) ;
		if ( strtoupper ( substr ( $text , 0 , 9 ) ) == '#REDIRECT' ) {
			$text = array_pop ( explode ( '[[' , $text , 2 ) ) ;
			$text = array_shift ( explode ( ']]' , $text , 2 ) ) ;
			$i->page = $text ;
			$url = "http://en.wikipedia.org/w/index.php?section=0&action=raw&title=" . myurlencode ( $i->page ) ;
			$text = file_get_contents ( $url ) ;
		}
	
		
		$dnb = array () ;
		preg_match_all ( '/\b(\d{3,})\b/' , $i->text , $dnb ) ;
		$dnb = $dnb[1] ;
		while ( count ( $dnb ) > 2 ) array_pop ( $dnb ) ;
		
		$cur_text = array_pop ( explode ( "'''" , $text , 2 ) ) ;
		$cur = array () ;
		preg_match_all ( '/\b(\d{3,})\b/' , $cur_text , $cur ) ;
		$cur = $cur[1] ;
		while ( count ( $cur ) > 2 ) array_pop ( $cur ) ;
		
		
		$all_matches = true ;
		if ( count ( $dnb ) == 0 ) $all_matches = false ;
		foreach ( $dnb AS $d ) {
			if ( in_array ( $d , $cur ) ) continue ;
			$all_matches = false ;
			break ;
		}
		
		// Paranoia
		if ( false !== stristr ( $text , '{{hndis' ) ) $all_matches = false ;
		if ( false !== stristr ( $text , '{{disambig' ) ) $all_matches = false ;
		
	
		$cs = "margin-bottom:3px;padding:1px" ;

		$out = "<div style='$cs'>" ;
		
		$alt = '' ;
		if ( $i->page != $i->name ) {
			$alt = " (<i>{$i->name}</i>) " ;
		}
		
		$link = $i->pre ;
		if ( $text == '' ) {
			$link .= "<a href=\"http://en.wikipedia.org/wiki/{$i->page}\" target='_blank' style='color:red'>" . $i->page . "</a>" ;
		} else {
			$link .= "<a href=\"http://en.wikipedia.org/wiki/{$i->page}\" target='_blank'>" . $i->page . "</a>" ;
		}
		$inp = "<input type='text' name='name[$ik]' value='{$i->page}' size='50'/>" ;
	
		if ( $all_matches ) {
			$out .= "<input type='checkbox' name='cb[$ik]' value='1' checked />$link $alt $inp <span style='padding:2px;color:white;background:green'>" ;
			$out .= count ( $dnb ) . " dates match.</span></div>" ;
		} else {
			$out = "<input type='checkbox' name='cb[$ik]' value='1' />$link $alt $inp</div>" ;
		}
		
		$itext = $i->text ;
		$itext = str_replace ( '<nowiki>' , '' , $itext ) ;
		$itext = str_replace ( '</nowiki>' , '' , $itext ) ;
		$out .= "<div style='background:#CCCCCC;$cs'>" . wikipre ( $itext ) . "</div>" ;
		$out .= "<div style='background:#EEEEEE;$cs'>" . wikipre ( $text ) . "</div>" ;
		
		print $out ;
		print "</li>" ;
		myflush() ;
	}
	print "</ol>" ; 
	print "<input type='submit' name='doit' value='Update' />" ;
	print "</form>" ;
}

print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
