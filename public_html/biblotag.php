<?PHP

include "common.php" ;

$dbn = 'u_magnus_biblotag_p' ;
$dbu = db_get_con ( $dbn ) ;
$script = 'biblotag.php' ;

$user_id = 0 ;
$user_name = 'Aloisius Anonymous' ;

function check_login () {
	global $user_id , $user_name ;
	if ( $_COOKIE['biblotag_logged_in'] == 1 ) {
		$user_name = str_replace ( '_' , ' ' , $_COOKIE['biblotag_user_name'] ) ;
		$user_id = $_COOKIE['biblotag_user_id'] ;
	}
}

function get_random_book_id () {
	global $dbn , $dbu , $user_id , $user_name ;
	$sql = "SELECT id FROM book ORDER BY RAND() LIMIT 1" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$o = mysql_fetch_object ( $res ) ;
	return $o->id ;
}

function print_book_details ( $bid ) {
	global $dbn , $dbu , $user_id , $user_name ;
	$sql = "SELECT * FROM book WHERE id=$bid" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$o = mysql_fetch_object ( $res ) ;
	$title = utf8_encode ( $o->title ) ;
	$text = utf8_encode ( $o->text ) ;
	print "<table><tr><th>Buchtitel</th><td style='font-size:120%'><i>$title</i></td></tr>" ;
	print "<tr><th>Beschreibung</th><td>$text</td></tr>" ;
	
	$sql = "SELECT count(*) AS cnt FROM tag WHERE book_id=$bid" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$o = mysql_fetch_object ( $res ) ;
	print "<tr><td/><td>Bisher wurden {$o->cnt} Vorschläge für dieses Buch eingereicht.</td></tr>" ;
	
	if ( $user_id != 0 ) {
		$sql = "SELECT DISTINCT wikipedia_title FROM tag WHERE book_id=$bid AND user_id=$user_id" ;
		$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
		$d = array () ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			$d[] = $o->wikipedia_title ;
		}
		if ( count ( $d ) > 0 ) {
			$d = implode ( "<br/>" , $d ) ;
			print "<tr><th>Von Dir eingereicht</th><td>$d</td></tr>" ;
		}
	}
	
	print "</table>" ;
}

function print_suggest_form ( $bid ) {
	global $script ;
	print "<form method='post' action='./$script' autocomplete='off'>" ;
	print "Dein Vorschlag : <input type='text' id='suggested_article' name='suggested_article' value='' size='50' />" ;
	print "<input type='hidden' name='bid' value='$bid' />" ;
	print "<input type='hidden' name='action' value='suggest' />" ;
	print "<input type='submit' name='doit' value='Vorschlag einreichen!' />" ;
	print "</form>" ;
}

function is_valid_article ( $q ) {
	$q = ucfirst ( trim ( $q ) ) ;
	make_db_safe ( $q ) ;
	$q = str_replace ( ' ' , '_' , $q ) ;
	$sql = "SELECT page_title FROM page WHERE page_title=\"$q\" AND page_namespace=0" ;
//	print "$sql<br/>" ;

	$mysql_con = db_get_con_new ( 'de','wikipedia') ;
	$db = get_db_name  ( 'de','wikipedia') ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	print mysql_error () ;
	if ( $o = mysql_fetch_object ( $res ) ) {
		return $q == $o->page_title ;
	} else return false ;
}

function add_tag ( $bid , $q ) {
	global $user_id , $dbn , $dbu ;
	make_db_safe ( $bid ) ;
	$q = ucfirst ( trim ( $q ) ) ;
	make_db_safe ( $q ) ;
	$q = str_replace ( ' ' , '_' , $q ) ;
	
	if ( $user_id != 0 ) {
		$sql = "SELECT count(*) AS cnt FROM tag WHERE user_id=$user_id AND book_id=$bid AND wikipedia_title=\"$q\"" ;
		$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
		if ( $o = mysql_fetch_object ( $res ) ) {
			if ( $o->cnt > 0 ) {
				print "<p>Du hast diesen Artikel bereits für dieses Buch vorgeschlagen!</p>" ;
				return false ;
			}
		}
	}
	
	$ts = date ( "YmdHis" ) ;
	$sql = "INSERT INTO tag ( user_id , book_id , wikipedia_title , timestamp ) VALUES ( $user_id , $bid , \"$q\" , \"$ts\" )" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	print "<p>Vorschlag eingereicht!</p>" ;
	return true ;
}

function print_user_form () {
	global $user_id , $user_name , $script ;
	print "<div style='float:right;border:2px solid #222288;padding:2px;margin-right:5px'>" ;
	print "Hallo, <b>$user_name</b>!" ;
	if ( $user_id == 0 ) {
		print "<br/><a href='./$script?action=login'>Anmelden</a> | <a href='./$script?action=new_account'>Neues Konto</a>" ;
	} else {
		print "<br/><a href='./$script?action=logout'>Abmelden</a>" ;
	}
	print "<br/><a href='./$script'>Anderes Buch probieren</a>" ;
	print "<br/><a href='./$script?action=list_books'>Bücherliste</a>" ;
	print "</div>" ;
}

function get_pwd_hash ( $pwd ) {
	$salt = file_get_contents ( '/home/magnus/tusc_salt.txt' ) ;
	$hash = substr ( sha1 ( $pwd . $salt ) , 0 , 64 ) ;
	return $hash ;
}

function print_login_form ( $denovo ) {
	global $script ;
	if ( $denovo ) $message = "Neues Konto anlegen" ;
	else $message = "Anmelden" ;
	print "<form method='post' action='./$script'><table border='1'>" ;
	print "<tr><td>Name</td><td><input name='user_name' type='text' value='' /></td></tr>" ;
	print "<tr><td>Passwort</td><td><input name='password' type='password' value='' /></td></tr>" ;
	if ( $denovo ) print "<tr><td>Passwort (erneut)</td><td><input name='password2' type='password' value='' /></td></tr>" ;
	print "<tr><td/><td><input name='doit' type='submit' value='$message' /></td></tr>" ;
	print "</table>" ;
	if ( $denovo ) print "<input type='hidden' name='action' value='new_account' />" ;
	else print "<input type='hidden' name='action' value='login' />" ;
	print "</form>" ;
}

function login_user ( $name , $pwd ) {
	global $dbn , $dbu , $user_id , $user_name ;
	
	make_db_safe ( $name ) ;
	$hash = get_pwd_hash ( $pwd ) ;
	$sql = "SELECT * FROM user WHERE name=\"$name\" AND password_hash=\"$hash\"" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	if ( $o = mysql_fetch_object ( $res ) ) {
		$user_id = $o->id ;
		$user_name = str_replace ( '_' , ' ' , $o->name ) ;
	} else {
		print "<p>Benutzername oder Passwort falsch!</p>" ;
		return false ;
	}
	
	setcookie ( 'biblotag_logged_in' , 1 ) ;
	setcookie ( 'biblotag_user_name' , $name ) ;
	setcookie ( 'biblotag_user_id' , $user_id ) ;
	check_login () ;
	return true ;
}

function add_new_account () {
	global $dbn , $dbu , $user_id , $user_name ;
	$name = get_request ( 'user_name' , '' ) ;
	$pwd1 = get_request ( 'password' , '' ) ;
	$pwd2 = get_request ( 'password2' , '' ) ;
	if ( trim ( $name ) == '' ) { print "<p>Benutzername darf nicht leer sein!</p>" ; return false ; }
	if ( trim ( $pwd1 ) == '' ) { print "<p>Passwort darf nicht leer sein!</p>" ; return false ; }
	if ( $pwd1 != $pwd2 ) { print "<p>Passwörter nicht identisch!</p>" ; return false ; }
	
	make_db_safe ( $name ) ;
	$sql = "SELECT name FROM user WHERE name=\"$name\"" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	if ( $o = mysql_fetch_object ( $res ) ) {
		if ( $o->name == $name ) {
			print "<p>Benutzername existiert bereits!</p>" ;
			return false ;
		}
	}
	
	$hash = get_pwd_hash ( $pwd1 ) ;
	
	$sql = "INSERT INTO user ( name , password_hash ) VALUES ( \"$name\" , \"$hash\" )" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	
	login_user ( $name , $pwd1 ) ;
	
	return true ;
}




$action = get_request ( 'action' , '' ) ;
$bid = get_request ( 'bid' , '' ) ;

if ( $action == 'login' and $user_id != 0 ) {
	print '<p>Du bist schon angemeldet!</p>' ;
	$action = '' ;
}

print '<html><head><meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />' ;
print "
<script type='text/javascript' src='lib/jquery.js'></script>
<script type='text/javascript' src='lib/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='lib/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='lib/thickbox-compressed.js'></script>
<script type='text/javascript' src='lib/jquery.autocomplete.js'></script>
<script type='text/javascript' src='biblotag.js'></script>
<link rel='stylesheet' type='text/css' href='lib/jquery.autocomplete.css' />
<link rel='stylesheet' type='text/css' href='lib/thickbox.css' />
</head><body>" ;

/*
<script type='text/javascript' src='localdata.js'></script>
<link rel="stylesheet" type="text/css" href="main.css" />
*/

print get_common_header ( $script , "BibloTag" ) ;

if ( $action == 'logout' ) {
	print "<p>Abgemeldet!</p>" ;
	setcookie ( 'biblotag_logged_in' , 0 ) ;
	$_COOKIE['biblotag_logged_in'] = 0 ;
	$action = '' ;
}

check_login () ;

if ( $action == 'new_account' ) {
	if ( get_request ( 'doit' , '' ) != '' ) {
		if ( !add_new_account() ) {
			print_user_form () ;
			print_login_form ( true ) ;
		} else {
			print "<p>Benutzerkonto erstellt!</p>" ;
			print_user_form () ;
			$action = '' ;
		}
	} else {
		print "<h1>Benutzerkonto erstellen</h1>" ;
		print_user_form () ;
		print_login_form ( true ) ;
	}
} else if ( $action == 'login' ) {
	if ( get_request ( 'doit' , '' ) != '' ) {
		$name = get_request ( 'user_name' , '' ) ;
		$pwd1 = get_request ( 'password' , '' ) ;
		if ( login_user ( $name , $pwd1 ) ) {
			print "<p>Angemeldet!</p>" ;
			print_user_form () ;
			$action = '' ;
		} else {
			print_user_form () ;
			print_login_form ( false ) ;
		}
	} else {
		print "<h1>Anmelden</h1>" ;
		print_user_form () ;
		print_login_form ( false ) ;
	}
} else print_user_form () ;



if ( $action == 'suggest' ) {
	$suggestion = get_request ( 'suggested_article' , '' ) ;
	$suggestion = str_replace ( '_' , ' ' , $suggestion ) ;
	if ( is_valid_article ( $suggestion ) and add_tag ( $bid , $suggestion ) ) {
//		print_book_details ( $bid ) ;
		$action = '' ;
		$bid = '' ;
	} else {
		print "<p>\"$suggestion\" ist kein Wikipedia-Artikel.</p>" ;
		$action = '' ;
	}
}

if ( $action == 'list_books' ) {
	$sql = "SELECT book_id,count(*) AS cnt FROM tag GROUP BY book_id" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	$tag_cnt = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$tag_cnt[$o->book_id] = $o->cnt ;
	}
	
	$sql = "SELECT * FROM book" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	print "<h1>Bücher in der Datenbank</h1>" ;
	print "<table border='1'><tr><th>Titel</th><th>Beschreibung/Link</th><th>Tags</th></tr>" ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$o->title = utf8_encode ( $o->title ) ;
		$o->text = utf8_encode ( $o->text ) ;
		$tc = $tag_cnt[$o->id] * 1 ;
		$c = $tc == 0 ? ' bgcolor="#FF030D"' : '' ;
		print "<tr><td><a href=\"./$script?action=show_book&book_id={$o->id}\">{$o->title}</a></td>" ;
		if ( $o->url != '' ) print "<td><a href=\"{$o->url}\">{$o->text}</a></td>" ;
		else print "<td>{$o->text}</td>" ;
		print "<td $c align='center'>$tc</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

if ( $action == 'show_book' ) {
	$book_id = get_request ( 'book_id' ) ;
	make_db_safe ( $book_id ) ;
	$sql = "SELECT * FROM book WHERE id=\"$book_id\"" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	if ( $o = mysql_fetch_object ( $res ) ) {
	} else {
		print "Buch #$book_id nicht gefunden!" ;
		exit ;
	}
	print "<h1>{$o->title}</h1>" ;
	
	$tag_logged_in = array () ;
	$tag_anon = array () ;
	$tags = array () ;
	$sql = "SELECT * FROM tag WHERE book_id=\"$book_id\"" ;
	$res = my_mysql_db_query ( $dbn , $sql , $dbu ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$tags[$o->wikipedia_title]++ ;
		if ( $o->user_id == 0 ) $tag_logged_in[$o->wikipedia_title]++ ;
		else $tag_anon[$o->wikipedia_title]++ ;
	}
	
	ksort ( $tags ) ;
	print "<table border='1'><tr><th>Wikipedia-Titel</th><th>Anonyme Stimmen</th><th>Stimmen mit Benutzerkonto</th><th>Total</th></tr>" ;
	$cnt1 = 0 ;
	$cnt2 = 0 ;
	foreach ( $tags AS $tag => $total ) {
		print "<tr>" ;
		print "<td><a href=\"http://de.wikipedia.org/wiki/".urlencode($tag)."\">$tag</a></td>" ;
		print "<td>" . ( $tag_anon[$tag] * 1 ) . "</td>" ;
		print "<td>" . ( $tag_logged_in[$tag] * 1 ) . "</td>" ;
		print "<td>" . ( $total ) . "</td>" ;
		print "</tr>" ;
		$cnt1 += $tag_anon[$tag] ;
		$cnt2 += $tag_logged_in[$tag] ;
	}
	print "<tr><td>Total</td><td>$cnt1</td><td>$cnt2</td><td>" . ( $cnt1 + $cnt2 ) . "</td></tr>" ;
	print "</table><br/>" ;
	$action = '' ;
	$bid = $book_id ;
}

if ( $action == '' ) {
	if ( $bid == '' ) $bid = get_random_book_id () ;
	print "<h1>Finde den passenden Wikipedia-Artikel zum Buch!</h1>" ;
	print_book_details ( $bid ) ;
	print_suggest_form ( $bid ) ;
}

print "</body></html>" ;

?>