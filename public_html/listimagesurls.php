<?PHP

include "common.php" ;

$language = get_request ( "language" , "commons" ) ;
$project = get_request ( "project" , "wikipedia" ) ;
$category = get_request ( "category" , "" ) ;

if ( $category == '' ) {
  print "Usage : ?language=commons&project=wikipedia&category=XYZ" ;
  exit ;
}

$wq = new WikiQuery ( $language , $project ) ;
$data = array_keys ( $wq->get_images_in_category ( $category ) ) ;
foreach ( $data AS $i ) {
  $i = array_pop ( explode ( ':' , $i , 2 ) ) ;
  print get_image_url ( $language , $i , $project ) . "<br/>" ;
}

?>