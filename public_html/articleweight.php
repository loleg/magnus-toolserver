<?PHP

include "common.php" ;


$disambig = array ( 'begriffsklärung' , 'disambiguation' ) ;
$bad_starts = array ( 'liste der ' , 'liste von ' , 'list of ' ) ;


function evaluate_last_form ( $language , $project ) {
	if ( !isset ( $_POST['doit'] ) ) return ; # No previous form
	global $mysql_con , $mysql_password ;
	$language = mysql_escape_string ( $language ) ;
	$project = mysql_escape_string ( $project ) ;
	$weight = mysql_escape_string ( get_request ( 'decision' , '0' ) ) ;
	$title1 = mysql_escape_string ( urldecode ( get_request ( 'title1' , '' )  ) ) ;
	$title2 = mysql_escape_string ( urldecode ( get_request ( 'title2' , '' ) ) ) ;
	$oweight = $weight ;

	if ( $weight == 0 || $title1 == '' || $title2 == '' ) return ; # Something's wrong

	if ( $mysql_password == "" && !get_database_password() ) {
		print "Sorry, no database connection available" ;
		exit ;
	}

	if ( 0 ) { # Testing
		$server = "127.0.0.1" ;
		$user = "root" ;
		$db = "u_magnus" ;
		$password = "KMnO4" ;
	} else { # Production
		$server = "sql" ;
		$user = "magnus" ;
		$db = "u_magnus" ;
		$password = $mysql_password ;
	}

	if ( !isset ( $mysql_con ) ) {
		if ( !$mysql_con = mysql_connect ( $server , $user , $password ) ) {
			echo 'Could not connect to mysql';
			exit;
		}
	}

	$wdiff = $weight > 0 ? "+{$weight}" : $weight ;
	$sql = "INSERT INTO pageranks (language,project,title,rank,counter) VALUES ('{$language}','{$project}','{$title1}','{$weight}',1) " .
			"ON DUPLICATE KEY UPDATE rank = rank {$wdiff}, counter = counter + 1" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( !$res ) {
		echo "Error when trying to contact {$db} (page)" ;
		exit ;
	}

	$weight = -$weight ;
	$wdiff = $weight > 0 ? "+{$weight}" : $weight ;
	$sql = "INSERT INTO pageranks (language,project,title,rank,counter) VALUES ('{$language}','{$project}','{$title2}','{$weight}',1) " .
			"ON DUPLICATE KEY UPDATE rank = rank {$wdiff}, counter = counter + 1" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( !$res ) {
		echo "Error when trying to contact {$db} (page)" ;
		exit ;
	}

	$ip = $_SERVER['REMOTE_ADDR'] ;
	$timestamp = date ( 'YmdHis' ) ;
	$sql = "INSERT INTO pageranklog (ip,project,language,timestamp,title1,title2,rank) VALUES " .
		"('{$ip}','{$project}','{$language}','{$timestamp}','{$title1}','{$title2}',{$oweight})" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( !$res ) {
		echo "Error when trying to contact {$db} (page)" ;
		exit ;
	}

	print "<i>Page ranks were stored successfully!<br/>" ;
}

function get_two_db_titles ( &$title1 , &$title2 , $language , $project ) {
	$title1 = "A1" ;
	$title2 = "A2" ;
	global $mysql_con , $mysql_password ;
	$language = mysql_escape_string ( $language ) ;
	$project = mysql_escape_string ( $project ) ;

	if ( $mysql_password == "" && !get_database_password() ) {
		print "Sorry, no database connection available" ;
		exit ;
	}

	if ( 0 ) { # Testing
		$server = "127.0.0.1" ;
		$user = "root" ;
		$db = "u_magnus" ;
		$password = "KMnO4" ;
	} else { # Production
		$server = "sql" ;
		$user = "magnus" ;
		$db = "u_magnus" ;
		$password = $mysql_password ;
	}

	if ( !isset ( $mysql_con ) ) {
		if ( !$mysql_con = mysql_connect ( $server , $user , $password ) ) {
			echo 'Could not connect to mysql';
			exit;
		}
	}

	$sql = "SELECT title,rank FROM pageranks WHERE language='{$language}' AND project='{$project}' ORDER BY rand() LIMIT 2" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	$o1 = mysql_fetch_object ( $res ) ;
	$o2 = mysql_fetch_object ( $res ) ;
	
	$title1 = $o1->title ;
	$title2 = $o2->title ;

}

function show_list ( $language , $project , $mode ) {
	if ( $mode != 'showlist' ) return ;
	global $mysql_con , $mysql_password ;
	$language = mysql_escape_string ( $language ) ;
	$project = mysql_escape_string ( $project ) ;
	$start = mysql_escape_string ( get_request ( 'start' , '0' ) ) ;
	$limit = mysql_escape_string ( get_request ( 'limit' , '25' ) ) ;

	if ( $mysql_password == "" && !get_database_password() ) {
		print "Sorry, no database connection available" ;
		exit ;
	}

		if ( 0 ) { # Testing
			$server = "127.0.0.1" ;
			$user = "root" ;
			$db = "u_magnus" ;
			$password = "KMnO4" ;
		} else { # Production
			$server = "sql" ;
			$user = "magnus" ;
			$db = "u_magnus" ;
			$password = $mysql_password ;
		}

	if ( !isset ( $mysql_con ) ) {
		if ( !$mysql_con = mysql_connect ( $server , $user , $password ) ) {
			echo 'Could not connect to mysql';
			exit;
		}
	}

	$sql = "SELECT title,rank,counter FROM pageranks WHERE language='{$language}' AND project='{$project}' ORDER BY rank DESC , title ASC LIMIT {$limit} OFFSET {$start}" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	$titles = array () ;
	$counter = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$titles[] = $o->title ;
		$ranks[] = $o->rank ;
		$counter[] = $o->counter ;
	}

	print "<a href='articleweight.php?language={$language}&project={$project}'>Back to Article Weight.</a><br/>" ;
	
	print "<ol>" ;
	$cnt = 1 ;
	foreach ( $titles AS $k => $title ) {
		$num = $start + $cnt ;
		$url = get_wikipedia_url ( $language , $title , '' , $project ) ;
		print "<li value='{$num}'><a href='{$url}'>{$title}</a> (weight {$ranks[$k]}, {$counter[$k]} votes)</li>" ;
		$cnt++ ;
	}
	print "</ol>" ;
	
	if ( $start > 0 ) {
		$num = $start - $limit ;
		if ( $num < 0 ) $num = 0 ;
		print "<a href='articleweight.php?mode=showlist&language={$language}&project={$project}&start={$num}&limit={$limit}'>Show previous {$limit}</a> &nbsp; " ;
	}
	
	if ( $cnt + $start >= $limit + $start ) {
		$num = $limit + $start ;
		print "<a href='articleweight.php?mode=showlist&language={$language}&project={$project}&start={$num}&limit={$limit}'>Show next {$limit}</a> &nbsp; " ;
	}
	
	exit ;
}

function is_disambiguation_page ( $text ) {
	global $disambig ;
	$tl = strtolower ( utf8_decode ( $text ) ) ;
	foreach ( $disambig AS $d ) {
		$d = '{{' . $d ;
		if ( false === strpos ( $tl , $d ) ) continue ;
		return true ;
	}
	return false ;
}

function is_unsuited_title ( $title ) {
	global $bad_starts ;
	if ( trim ( $title ) == '' ) return true ;
	$tl = strtolower ( utf8_decode ( $title ) ) ;
	foreach ( $bad_starts AS $d ) {
		if ( substr ( $tl , 0 , strlen ( $d ) ) != $d ) continue ;
		return true ;
	}
	return false ;
}

function is_unsuited ( $title , $text ) {
	if ( strtolower ( substr ( trim ( $text ) , 0 , 9 ) ) == '#redirect' ) return true ;
	if ( is_unsuited_title ( $title ) ) return true ;
	if ( is_disambiguation_page ( $text ) ) return true ;
	return false ;
}

function get_weight ( $language , $project , $title ) {
	global $mysql_con , $mysql_password ;
	$language = mysql_escape_string ( $language ) ;
	$project = mysql_escape_string ( $project ) ;
	$title = mysql_escape_string ( str_replace ( '_' , ' ' , $title ) ) ;

	if ( $mysql_password == "" && !get_database_password() ) {
		print "Sorry, no database connection available" ;
		exit ;
	}

	if ( 0 ) { # Testing
		$server = "127.0.0.1" ;
		$user = "root" ;
		$db = "u_magnus" ;
		$password = "KMnO4" ;
	} else { # Production
		$server = "sql" ;
		$user = "magnus" ;
		$db = "u_magnus" ;
		$password = $mysql_password ;
	}

	if ( !isset ( $mysql_con ) ) {
		if ( !$mysql_con = mysql_connect ( $server , $user , $password ) ) {
			echo 'Could not connect to mysql';
			exit;
		}
	}
	
	$sql = "SELECT * FROM pageranks WHERE language='{$language}' AND project='{$project}' AND title='{$title}'" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( ! ( $o = mysql_fetch_object ( $res ) ) ) return "no" ;
	mysql_free_result ( $res ) ;
	return $o->rank ;
	
}

$categoryname = get_request ( 'categoryname' , '' ) ;
$mode = get_request ( 'mode' , '' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$language = get_request ( 'language' , 'de' ) ;
$depth = get_request ( 'depth' , '3' ) ;

$getweight = get_request ( 'getweight' , '' ) ;
if ( $getweight != '' ) {
	print get_weight ( $language , $project , $getweight ) ;
	exit ;
}

print "<html><head>" ;
print "<script language='javascript'>
function mytoggle(id){
	if ( id.style.display == \"none\" )
		id.style.display=\"block\";
	else
		id.style.display=\"none\";
}
</script>" ;
print "</head><body>" ;


print get_common_header ( 'articleweight.php' ) ;
print "<h1>Weight articles</h1>" ;
print "Here, you can weight the importance of articles against each other.<br/>" ;

evaluate_last_form ( $language , $project ) ;
show_list ( $language , $project , $mode ) ;

if ( $mode != '' ) {
	
	# Loading stuff
	print "Loading two articles " ; 
	
	if ( $mode == 'randomtitles' ) {
		print ".." ; myflush () ;
		$load1 = true ;
		$load2 = true ;
		$is_on_toolserver = false ;
		do {
			print "." ; myflush () ;
			if ( $load1 ) {
				$title1 = get_random_title_from_db ( $language , $project ) ;
#				print "Trying {$title1}<br/>" ; myflush();
				$text1 = get_wikipedia_article ( $language , $title1 , false , $project ) ;
#				print "loaded<br/>" ; myflush () ;
				$load1 = false ;
			}
			
			if ( $load2 ) {
				$title2 = get_random_title_from_db ( $language , $project ) ;
#				print "Trying {$title2}<br/>" ; myflush();
				$text2 = get_wikipedia_article ( $language , $title2 , false , $project ) ;
#				print "loaded<br/>" ; myflush () ;
				$load2 = false ;
			}
			
			$load1 = is_unsuited ( $title1 , $text1 ) ;
			$load2 = is_unsuited ( $title2 , $text2 ) ;
		} while ( $load1 OR $load2 ) ; # While one of the pages is a disambiguation page

	} else if ( $mode == 'alreadyinlist' ) {
		print ".." ; myflush () ;
		get_two_db_titles ( $title1 , $title2 , $language , $project ) ;
		$text1 = get_wikipedia_article ( $language , $title1 , true , $project ) ;
		$text2 = get_wikipedia_article ( $language , $title2 , true , $project ) ;
	} else if ( $mode == 'samecategory' || $mode == 'samecategorytree' ) {
		if ( $mode == 'samecategory' ) $depth = 0 ;
		
		$clear_category = false ;
		do {
			$again = false ;
			if ( $categoryname == '' ) {
				$categoryname = get_random_title_from_db ( $language , $project , 14 ) ;
				$clear_category = true ;
			}
			
			$pages = get_catscan_pages2 ( $language , $categoryname , $depth , $project ) ;
			#print count ( $pages ) . "{$categoryname} {$language} {$depth} {$project}<br/>" ; myflush () ;
		
			if ( count ( $pages ) < 2 ) {
				if ( $clear_category ) {
					print ".. not enough suitable articles in category {$categoryname}, retrying ..<br>" ;
					$again = true ;
					$categoryname = '' ;
				} else {
					print ".. less than two articles in category {$categoryname}, aborting" ;
					exit ;
				}
			}
		} while ( $again ) ;
		
		print " (in category \"{$categoryname}\") .." ; myflush () ;
		if ( $clear_category ) $categoryname = '' ;
		
#		print "<pre>" ; print_r ( $pages ) ; print "</pre>" ;
		
		$is_on_toolserver = false ;
		$load1 = true ;
		$load2 = true ;
		do {
			print "." ; myflush () ;
			if ( $load1 ) {
				$key1 = array_rand ( $pages ) ;
				$title1 = $pages[$key1]->title ;
				$text1 = get_wikipedia_article ( $language , $title1 , false , $project ) ;
				unset ( $pages[$key1] ) ;
				$load1 = false ;
			}

			if ( $load2 ) {
				$key2 = array_rand ( $pages ) ;
				$title2 = $pages[$key2]->title ;
				$text2 = get_wikipedia_article ( $language , $title2 , false , $project ) ;
				unset ( $pages[$key2] ) ;
				$load2 = false ;
			}
	
			$load1 = is_unsuited ( $title1 , $text1 ) ;
			$load2 = is_unsuited ( $title2 , $text2 ) ;
			
			if ( count ( $pages ) < 2 ) {
				print " not enough suitable articles in category, aborting." ;
				exit ;
			}
		} while ( $load1 OR $load2 ) ;

	}
	
	print "<br/>" ;
	
	$url1 = get_wikipedia_url ( $language , $title1 , '' , $project ) ;
	$url2 = get_wikipedia_url ( $language , $title2 , '' , $project ) ;
	$sum1 = str_replace ( "\n" , "<br/>" , str_replace ( "<" , "&lt;" , get_initial_paragraph ( $text1 ) ) ) ;
	$sum2 = str_replace ( "\n" , "<br/>" , str_replace ( "<" , "&lt;" , get_initial_paragraph ( $text2 ) ) ) ;
	$long_sum1 = str_replace ( "\n" , "<br/>" , str_replace ( "<" , "&lt;" , $text1 ) ) ;
	$long_sum2 = str_replace ( "\n" , "<br/>" , str_replace ( "<" , "&lt;" , $text2 ) ) ;
	$chars1 = strlen ( $text1 ) ;
	$chars2 = strlen ( $text2 ) ;
	$lines1 = count ( explode ( "\n" , $text1 ) ) ;
	$lines2 = count ( explode ( "\n" , $text2 ) ) ;

	$sum1 = "<input type='button' onclick='mytoggle(left_sum);mytoggle(left_text)' value='Toggle summary/text' /><br/>
	<div id='left_sum' style='display:block'>{$sum1}</div>
	<div id='left_text' style='display:none'>{$long_sum1}</div>" ;
	$sum2 = "<input type='button' onclick='mytoggle(right_sum);mytoggle(right_text)' value='Toggle summary/text' /><br/>
	<div id='right_sum' style='display:block'>{$sum2}</div>
	<div id='right_text' style='display:none'>{$long_sum2}</div>" ;
	
	
	$button_text = "
	<form method='post' style='display : inline'>
	<input type='submit' name='doit' value='" . '{$1}' . "'/>
	<input type='hidden' name='decision' value='" . '{$2}' . "'/>
	<input type='hidden' name='language' value='{$language}'/>
	<input type='hidden' name='project' value='{$project}'/>
	<input type='hidden' name='depth' value='{$depth}'/>
	<input type='hidden' name='mode' value='{$mode}'/>
	<input type='hidden' name='categoryname' value='{$categoryname}'/>
	<input type='hidden' name='title1' value='" . myurlencode ( $title1 ) . "'/>
	<input type='hidden' name='title2' value='" . myurlencode ( $title2 ) . "'/>
	</form>
	" ;
	$b_p2 = str_replace ( '{$1}' , 'Much more important' , str_replace ( '{$2}' , '3' , $button_text ) ) ;
	$b_p1 = str_replace ( '{$1}' , 'More important' , str_replace ( '{$2}' , '1' , $button_text ) ) ;
	$b_p0 = str_replace ( '{$1}' , 'Equally important' , str_replace ( '{$2}' , '0' , $button_text ) ) ;
	$b_m2 = str_replace ( '{$1}' , 'Much more important' , str_replace ( '{$2}' , '-3' , $button_text ) ) ;
	$b_m1 = str_replace ( '{$1}' , 'More important' , str_replace ( '{$2}' , '-1' , $button_text ) ) ;
	
	print "<table border='1' width='100%'>" ;
	print "<tr><th colspan='2' width='50%'><a href='{$url1}'>{$title1}</a></th><th colspan='2' width='50%'><a href='{$url2}'>{$title2}</a></th></tr>" ;
	print "<tr>" ;
	print "<td width='50%'>{$b_p2}{$b_p1}<br/>{$lines1} lines, {$chars1} characters.</td>" ;
	print "<td valign='top' colspan='2'>{$b_p0}</td>" ;
	print "<td width='50%' align='right'>{$b_m1}{$b_m2}<br/>{$lines2} lines, {$chars2} characters.</td>" ;
	print "</tr>" ;
	print "<tr><td colspan='2' valign='top'>{$sum1}</td><td colspan='2' valign='top'>{$sum2}</td></tr>" ;
	print "</table>" ;
	print "<small>Note: Only the first paragraph of each article is displayed</small><hr/>" ;
}

# Print the settings form
print "<a href='articleweight.php?mode=showlist&language={$language}&project={$project}'>Show ranked page list</a><br/>" ;

if ( $depth == 0 ) $depth = '3' ;
if ( $mode == '' ) $mode = 'randomtitles' ;
$mode1 = $mode == 'randomtitles' ? 'checked' : '' ;
$mode2 = $mode == 'samecategory' ? 'checked' : '' ;
$mode3 = $mode == 'samecategorytree' ? 'checked' : '' ;
$mode4 = $mode == 'alreadyinlist' ? 'checked' : '' ;

print "
<form method='post'>
<input type='radio' name='mode' {$mode1} value='randomtitles'/>Random titles<br/>
<input type='radio' name='mode' {$mode2} value='samecategory'/>Same category<br/>
<input type='radio' name='mode' {$mode3} value='samecategorytree'/>Same category tree<br/>
<input type='radio' name='mode' {$mode4} value='alreadyinlist'/>Two articles already in the \"weighted list\"<br/>
<table>
<tr><td>Category</td><td><input type='text' name='categoryname' value='{$categoryname}'/> (leave blank for random category)</td></tr>
<tr><td>Depth</td><td><input type='text' name='depth' value='{$depth}'/></td></tr>
<tr><td>Project</td><td><input type='text' name='project' value='{$project}'/></td></tr>
<tr><td>Language</td><td><input type='text' name='language' value='{$language}'/></td></tr>
</table>
<input type='submit' name='OK' value='Run this!'/>
</form>
" ;

print "<hr/><small><b>Prepare an URL:</b><br/>
* <i>language=de</i> sets the project language ('de' is default)<br/>
* <i>project=wikipedia</i> sets the project type ('wikipedia' is default)<br/>
* <i>mode=randomtitles|samecategory|samecategorytree</i> sets a mode ('randomtitles' is default)<br/>
* <i>depth=3</i> sets the depth for 'samecategorytree' ('3' is default)<br/>
* <i>categoryname=Your_category_name</i> sets the (root) category for 'samecategory' and 'samecategorytree'
</small>" ;
print "</body></html>" ;

?>
