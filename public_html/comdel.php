<?PHP

include_once ( 'queryclass.php' ) ;

#__________________________________________________________

$deletion_templates = array (
	'delete' ,
	'deletion request' ,
) ;

#__________________________________________________________

function startswith ( $long , $short ) {
	return substr ( $long , 0 , strlen ( $short ) ) == $short ;
}

function remove_templates_from_text ( &$text , $templates ) {
	do {
		$otext = $text ;
		$t = '' ;
		foreach ( $templates AS $template ) {
			$p = stripos ( $text , '{{' . $template ) ;
			if ( $p === false ) continue ;
			$t = $template ;
			break ; # Template found
		}
		if ( $t == '' ) break ; # No templates, done
		
		$t1 = trim ( substr ( $text , 0 , $p ) ) ;
		$t2 = substr ( $text , $p + 2 ) ;
		$t2 = array_pop ( explode ( '}}' , $t2 , 2 ) ) ;
		$t2 = array_pop ( explode ( "\n" , $t2 , 2 ) ) ;
		if ( $t1 != '' ) $t1 .= "\n" ;
		$text = $t1 . trim ( $t2 ) ;
		while ( substr ( $text , 0 , 1 ) == '-' ) $text = trim ( substr ( $text , 1 ) ) ;
	} while ( $text != $otext ) ;
}

function my_get_edit_timestamp ( $title , $language , $project ) {
	$url = "http://{$language}.{$project}.org/wiki/Special:Export/" . myurlencode ( $title ) ;
	$t = @file_get_contents ( $url ) ;
	$desc = $t ;
	$t = explode ( '<timestamp>' , $t , 2 ) ;
	$t = explode ( '</timestamp>' , array_pop ( $t ) , 2 ) ;
	$t = array_shift ( $t ) ;
	$t = str_replace ( '-' , '' , $t ) ;
	$t = str_replace ( ':' , '' , $t ) ;
	$t = str_replace ( 'T' , '' , $t ) ;
	$t = str_replace ( 'Z' , '' , $t ) ;
	return $t ;
}

function get_edit_button ( $title , $button_label , $edit_summary , $edit_text , $mode = 'Preview' ) {
	global $language , $project , $query ;
	$starttime = date ( "YmdHis" , time() + (12 * 60 * 60) ) ;
	#$timestamp = date ( "YmdHis" , time() ) ;
	$timestamp = my_get_edit_timestamp ( $title , $language , $project ) ;
	$mode = ucfirst ( trim ( strtolower ( $mode ) ) ) ;
	if ( $mode == 'Diff' ) $action = 'submit' ;
	else $action = 'edit' ;
	$url = "http://{$language}.{$project}.org/w/index.php?title=" . myurlencode ( $title ) . '&action=' . $action ;
	$edit_text = trim ( mask_quotes ( $edit_text ) ) ;
	$edit_text = str_replace ( '&#126;' , '&amp;#126;' , $edit_text ) ;
	$edit_summary = trim ( mask_quotes ( $edit_summary ) ) ;

	$button = "<form id='upload' method=post enctype='multipart/form-data' action='{$url}' target='_blank' style='display:inline'>" ;
	$button .= "<input type='hidden' name='wpTextbox1' value='" . $edit_text . "'/>" ;
	$button .= "<input type='hidden' name='wpSummary' value='{$edit_summary}'/>" ;
#	if ( $mode != 'Preview' ) $button .= "<input type='hidden' name='wpPreview' value='wpPreview' />" ;
	$button .= "<input type='hidden' value='{$starttime}' name='wpStarttime' />" ;
	$button .= "<input type='hidden' value='{$timestamp}' name='wpEdittime' />" ;
	$button .= "<input type='submit' name='wp{$mode}' value='{$button_label}'/>" ;
	$button .= "</form>" ;
	return $button ;
}



function get_comdel_date ( $title ) {
	global $language , $project , $query ;
	$data = array_keys ( $query->get_backlinks ( $title ) ) ;
#	print "<pre>" ; print_r ( $d ) ; print "</pre>" ;
	
	foreach ( $data AS $d ) {
		if ( !startswith ( $d , 'Commons:Deletion requests/20' ) ) continue ; # Good until the year 2100 ;-)
		return array_pop ( explode ( '/' , $d , 2 ) ) ;
	}
	return '' ;
}

function get_comdel_edit ( $title , $action , $reason ) {
	global $language , $project , $query ;
	$ret = '' ;
	$comdel_title = 'Commons:Deletion requests/' . $title ;
	$comdel_text = get_wikipedia_article ( $language , $comdel_title , false , $project ) ;
	
	if ( $comdel_text == '' ) {
		$url = get_wikipedia_url ( $language , $comdel_title , '' , $project ) ;
		$date = get_comdel_date ( $title ) ;
		$ret = "An <a href='{$url}'>image-specific ComDel page</a> does not appear to exist." ;
		if ( $date != '' ) {
			$url_date = get_wikipedia_url ( $language , "Commons:Deletion requests/{$date}" , '' , $project ) ;
			$ret .= " It might be on the <a href='{$url_date}'>dated ComDel page</a>, though." ;
		}
		return $ret ;
	}
	
	if ( $action == 'keep' ) {
		$edit_text = "{{delh}}\n----\n{$comdel_text}\n----\n'''Kept'''. {$reason} --~~~~\n{{delf}}" ;
	} else if ( $action == 'delete' ) {
		$edit_text = "{{delh}}\n----\n{$comdel_text}\n----\n'''Deleted'''. {$reason} --~~~~\n{{delf}}" ;
	} else return "Invalid action <i>{$action}</i>!" ;
	
	$edit_summary = $reason ;
	$ret = get_edit_button ( $comdel_title , 'Close issue on ComDel page' , $edit_summary , $edit_text , 'diff' ) ;
#	$ret = cGetEditButton ( $edit_text , $comdel_title , 'commons' , 'wikimedia' , 'Close issue on ComDel page' , 'Close issue' , true , false , true ) ;
	
	return $ret ;
}

function remove_delete_notice ( $title , $reason ) {
	global $language , $project , $query ;
	global $deletion_templates ;
	$ret = '' ;
	
	$text = get_wikipedia_article ( $language , $title , false , $project ) ;
	
	if ( $text == '' ) {
		$url = get_wikipedia_url ( $language , $title , '' , $project ) ;
		return "The <a href='{$url}'>image description page</a> does not appear to exist, or is blank." ;
	}
	
	
	$otext = $text ;
	remove_templates_from_text ( $text , $deletion_templates ) ;
	
	if ( $text == $otext ) {
		$url = get_wikipedia_url ( $language , $title , '' , $project ) ;
		return "No deletion template could be found on the <a href='{$url}'>image description page</a>." ;
	}
	
	$ret = get_edit_button ( $title , 'Remove deletion template from image description' , "Keeping image : {$reason}" , $text , 'diff' ) ;
	return $ret ;
}

function remove_userpage_notification ( $title , $note ) {
	global $language , $project , $query ;
	$d = $query->get_image_data ( $title ) ;
	$username = $d['image']['user'] ;
	
	if ( $username == '' ) return 'Image has apparently been deleted.' ;
	
	$user_title = "User talk:{$username}" ;
	$text = get_wikipedia_article ( $language , $user_title , false , $project ) ;
	
	$title2 = str_replace ( ' ' , '_' , $title ) ;
	
	
	$seeks = array (
		"==Image Tagging [[:{$title2}]]==\n" ,
		"==[[:{$title2}]]==\n" ,

		"==Image Tagging [[:{$title}]]==\n" ,
		"==[[:{$title}]]==\n" ,
	) ;
	
	do {
		$seek = array_shift ( $seeks ) ;
		$p = strpos ( $text , $seek ) ;
	} while ( count ( $seeks ) > 0 AND $p === false ) ;
	
	
	if ( $p === false ) {
		$url = get_wikipedia_url ( $language , $user_title , '' , $project ) ;
		return "Unable to find the deletion warning on <a href='{$url}'><i>{$username}</i>'s talk page</a>." ;
	}
	
	$t1 = substr ( $text , 0 , $p ) ; # Before heading
	$t2 = substr ( $text , $p + strlen ( $seek ) ) ; # After heading
	$t2 = explode ( "\n=" , $t2 , 2 ) ;
	if ( count ( $t2 ) == 1 ) $t2 = '' ;
	else $t2 = "\n=" . array_pop ( $t2 ) ;
	$text = $t1 . $t2 ;

	$ret = get_edit_button ( $user_title , 'Remove deletion warning from user talk page' , "{$note}, thus removing deletion warning" , $text , 'diff' ) ;
	return $ret ;
}

function delete_page ( $title , $reason ) {
	global $language , $project , $query ;
	
	$reason = "Deleted as per [[Commons:Deletion requests/{$title}]] ({$reason})." ;
	
	$u1 = myurlencode ( $title ) ;
	$u2 = myurlencode ( array_pop ( explode ( ':' , $title , 2 ) ) ) ;
	$url = "http://{$language}.{$project}.org/w/index.php?title={$u1}&image={$u2}&action=delete&wpReason=" . urlencode ( $reason ) ;
	
	$ret = "<a href='{$url}'>Delete image \"<i>{$title}</i>\"</a>" ;
	
	return $ret ;
}

function get_main_form () {
	global $language , $project , $query ;
	global $title , $reason , $action , $usedate ;
	$check = array ( 'keep' => '' , 'delete' => '' ) ;
	$check[$action] = ' checked' ;
	
	$reason_warning = ( isset ( $_REQUEST['doit'] ) AND $reason == '' ) ? " <font color='red'>Gimme a reason, dammit!</font>" : '' ;
	$usedate_checked = $usedate ? " checked" : '' ;
	$t1 = time()-(10*24*60*60) ; # 10 days ago
	$t2 = time() ;
	$year1 = get_request ( 'year1' , date ( "Y" , $t1 ) ) ;
	$month1 = get_request ( 'month1' , date ( "m" , $t1 ) ) ;
	$day1 = get_request ( 'day1' , date ( "d" , $t1 ) ) ;
	$year2 = get_request ( 'year2' , date ( "Y" , $t2 ) ) ;
	$month2 = get_request ( 'month2' , date ( "m" , $t2 ) ) ;
	$day2 = get_request ( 'day2' , date ( "d" , $t2 ) ) ;
	
	$ret = "<form method='post'>" ;
	$ret .= "<table width='500px'>" ;
	$ret .= "<tr><th>Date range</th><td>" .
			"<input type='checkbox' name='usedate' value='1'{$usedate_checked}/> " .
			"<input size='3' type='text' name='year1' value='{$year1}'/>/" .
			"<input size='1' type='text' name='month1' value='{$month1}'/>/" .
			"<input size='1' type='text' name='day1' value='{$day1}'/> - " .
			"<input size='3' type='text' name='year2' value='{$year2}'/>/" .
			"<input size='1' type='text' name='month2' value='{$month2}'/>/" .
			"<input size='1' type='text' name='day2' value='{$day2}'/> <i>or</i>" .
			"</td></tr>" ;
	$ret .= "<tr><th>Title</th><td><input style='width:100%' type='text' name='title' value='{$title}'/></td></tr>" ;
	$ret .= "<tr><th>Decision</th><td>" .
			"<input type='radio' name='action' value='keep'{$check['keep']}/>Keep " .
			"<input type='radio' name='action' value='delete'{$check['delete']}/>Delete" .
			"</td></tr>" ;
	$ret .= "<tr><th>Reason</th><td nowrap><input style='width:100%' type='text' name='reason' value='{$reason}'/>{$reason_warning}</td></tr>" ;
	$ret .= "<tr><td/><td><input type='submit' name='doit' value='Generate edit buttons'/></td></tr>" ;
	$ret .= "</table>" ;
	$ret .= "</form>" ;
	return $ret ;
}

function get_date_form () {
	global $language , $project , $query ;
	global $title , $reason , $action , $usedate ;
	$year1 = get_request ( 'year1' ) ;
	$month1 = get_request ( 'month1' ) ;
	$day1 = get_request ( 'day1' ) ;
	$year2 = get_request ( 'year2' ) ;
	$month2 = get_request ( 'month2' ) ;
	$day2 = get_request ( 'day2' ) ;

	print "<form method='post'>" ;
	print "<table>" ;
	
	$t1 = mktime ( 0 , 0 , 0 , $month1 , $day1 , $year1 ) ;
	$t2 = mktime ( 0 , 0 , 0 , $month2 , $day2 , $year2 ) ;
	while ( $t2 >= $t1 ) {
		$d = date ( "Y/m/d" , $t1 ) ;
		$target = "Commons:Deletion requests/{$d}" ;
		$t = $query->get_used_templates ( $target , true ) ;
		foreach ( $t AS $k => $v ) {
			if ( !startswith ( $v , "Deletion requests/Image:" ) ) unset ( $t[$k] ) ;
			else $t[$k] = array_pop ( explode ( '/' , $v , 2 ) ) ;
		}
		
		$cc = 0 ;
		print "<tr><th align='left' colspan='3' style='background-color:#AAAAFF'>{$d}</th></tr>" ;
		foreach ( $t AS $i ) {
			$x = 'choices[' . urlencode ( $i ) . ']' ;
			$y = 'reasons[' . urlencode ( $i ) . ']' ;
			$i2 = "Commons:Deletion requests/" . $i ;
			$url = get_wikipedia_url ( $language , $i , '' , $project ) ;
			
			$text = get_wikipedia_article ( $language , $i2 , false , $project ) ;
			$text = trim ( array_pop ( explode ( '|}</noinclude>' , $text , 2 ) ) ) ;
			while ( substr ( $text , 0 , 2 ) == '==' ) $text = trim ( array_pop ( explode ( "\n" , $text , 2 ) ) ) ;
			$text = str_replace ( '<' , '&lt;' , $text ) ;
			$text = str_replace ( "\n" , '<br>' , $text ) ;
			
			$cc = 1 - $cc ;
			$col = $cc ? '#FFEEEE' : '#EEFFEE' ;
			print "<tr bgcolor='{$col}'>" ;
			print "<td valign='top'>" .
					"<a href='{$url}'>{$i}</a><br/>" .
					"<input type='radio' name='{$x}' value='keep'/>Keep<br/>" . 
					"<input type='radio' name='{$x}' value='delete'/>Delete<br/>" . 
					"<input type='radio' name='{$x}' value='' checked/>Ignore" . 
					"</td>" ;
			print "<td valign='top'><small>{$text}</small>" ;
			print "</tr><tr bgcolor='{$col}'><th align='left'>Reason</th><td><input type='text' name='{$y}' value='' style='width:100%'/></td>" ;
			print "</tr>" ;
			myflush () ;
		}

		$day1++ ;
		if ( $day1 > 31 ) {
			$day1 = 1 ;
			$month1++ ;
			if ( $month1 > 12 ) {
				$month1 = 1 ;
				$year1++ ;
			}
		}
		$t1 = mktime ( 0 , 0 , 0 , $month1 , $day1 , $year1 ) ;
	}


	print "</table>" ;
	print "<input type='hidden' name='viachoice' value='1'/>" ;
	print "<input type='submit' name='doit' value='Generate edit buttons'/>" ;
	print "</form>" ;
}

#__________________________________________________________

$project = 'wikimedia' ;
$language = 'commons' ;
$action = get_request ( 'action' , 'keep' ) ;
$usedate = get_request ( 'usedate' , false ) ;
$reason = str_replace ( '_' , ' ' , get_request ( 'reason' , '' ) ) ;
$title = str_replace ( '_' , ' ' , get_request ( 'title' , '' ) ) ;
$viachoice = get_request ( 'viachoice' , false ) ;

$action = trim ( strtolower ( $action ) ) ;
if ( $action != 'keep' AND $action != 'delete' ) $action = 'keep' ;
$query = new WikiQuery ( $language , $project ) ;


# Header
print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "comdel.php" ) ;

if ( isset ( $_REQUEST['doit'] ) AND $usedate AND !$viachoice ) {
	get_date_form () ;
} else if ( $viachoice OR ( isset ( $_REQUEST['doit'] ) AND $reason != '' AND $title != '' ) ) {
	print "<h1>ComDel</h1>" ;
	print "<p><small><i>Note : </i> Links will open in this window/tab, buttons in a new window/tab.</small></p>" ;
	myflush () ;
		
	$titles = array () ;
	$actions = array () ;
	$reasons = array () ;
	if ( isset ( $_REQUEST['choices'] ) ) {
		$choices = $_REQUEST['choices'] ;
		$rea = $_REQUEST['reasons'] ;
		foreach ( $choices AS $k => $v ) {
			if ( $v == '' ) continue ;
			$ok = $k ;
			$k = urldecode ( $k ) ;
			$titles[] = $k ;
			$actions[$k] = $v ;
			$reasons[$k] = $rea[$ok] ;
		}
	} else $titles = explode ( "\n" , $title ) ;
	
	
	print "<table width='98%'>" ;
	
	foreach ( $titles AS $title ) {
		$title = trim ( $title ) ;
		if ( $title == '' ) continue ;
		
		if ( isset ( $actions[$title] ) ) $action = $actions[$title] ;

		if ( isset ( $reasons[$title] ) ) $reason = $reasons[$title] ;
		if ( $reason == '' ) $reason = 'No reason given.' ;
		
		$button1 = get_comdel_edit ( $title , $action , $reason ) ;
		
		if ( $action == 'keep' ) {
			$button2 = remove_delete_notice ( $title , $reason ) ;
			$button3 = remove_userpage_notification ( $title , 'Keeping image' ) ;
		} else if ( $action == 'delete' ) {
			$button2 = delete_page ( $title , $reason ) ;
			$button3 = remove_userpage_notification ( $title , 'Deleted image' ) ;
		}
		
		print "<tr><th align='left' colspan='3' style='background-color:#AAAAFF'>" . ucfirst  ( $action ) . "  \"<i>{$title}</i>\"</th></tr>" ;
		print "<tr>" ;
		print "<td width='33%'>{$button1}</td>\n" ;
		print "<td width='33%'>{$button2}</td>\n" ;
		print "<td width='33%'>{$button3}</td>\n" ;
		print "</tr>" ;
		myflush () ;
		$reason = '' ;
	}
	
	print "</table>" ;
	
} else {
	print "<h1>ComDel - a Commons keep/delete tool</h1>" ;
	print "<p><small><i>The</i> choice of hamsters and deletionists alike!</small></p>" ;
	print get_main_form () ;
	print "<p><hr/><small>
	<b>Pre-fill values using URL parameters:</b><br/>
	<ul>
	<li><b>title=</b>Image:Xyz.jpg</li>
	<li><b>action=</b>keep|delete</li>
	<li><b>reason=</b>The reason for it all.</li>
	</ul>
	<b>Example :</b> comdel.php?title=Image:Xyz.jpg&action=delete&reason=Why_not.
	</small></p>" ;
}

print "</body></html>" ;
?>