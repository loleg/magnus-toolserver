<?PHP

$hide_header = true ;
$hide_doctype = true ;
include ( "queryclass.php" ) ;

$script = get_request ( 'script' , 'sources.php' ) ;
$dozip = get_request ( 'zip' , 0 ) ;

$script = array_pop ( explode ( '\\' , $script ) ) ;
$script = array_pop ( explode ( '/' , $script ) ) ;

$files = array ( $script , 
	'common.php' , 
	'queryclass.php' , 
	'class_wikiquery.php' , 
	'database_functions.php' , 
	'common_data.php' , 
	'class_pagedata.php' , 
	'class_imagedata.php'
) ;

function make_source_link ( $f ) {
	global $script ;
	$ret = "<a href=\"http://tools.wikimedia.de/~magnus/common.php?common_source={$f}\">" ;
	if ( $script == $f ) $ret .= "<b>" ;
	$ret .= $f ;
	if ( $script == $f ) $ret .= "</b>" ;
	$ret .= "</a>" ;
	return $ret ;
}


if ( $dozip ) {
	$zip = new ZipArchive;
	$fn = tempnam ( "/tmp" , 'src' ) . '.zip' ;
	if ($zip->open($fn,ZIPARCHIVE::CREATE) !== TRUE) exit ;
	foreach ( $files AS $f ) {
		$zip->addFile($f, $f);
	}
	$zip->close();
	
	header("Content-type: application/zip; filename=\"{$source}.zip\"");
	$thefile = fopen ( $fn , 'rb' ) ;
	fpassthru  ( $thefile ) ;
	fclose ( $thefile ) ;
	
	unlink ( $fn ) ;
	exit ;
}

header('Content-type: text/html; charset=utf-8');
print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' . "\n\n" ;
print get_common_header ( 'sources.php' ) ;
print "<h1>Sources</h1><p>This page links to the sources of <a href=\"{$script}\">{$script}</a> and associated files (which are necessary to run {$script}).<br/>
All files are under GPL unless noted otherwise.<ul>" ;

foreach ( $files AS $f ) {
	print "<li>" . make_source_link ( $f ) . "</li>" ;
}

print "</ul><hr/><a href=\"sources.php?script={$script}&zip=1\">All files in a single ZIP file</a></p>" ;

?>