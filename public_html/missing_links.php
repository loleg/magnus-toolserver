﻿<?PHP

error_reporting ( E_ALL ) ;

include_once ( "common.php" ) ;
$is_on_toolserver = false ;

$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$title = get_request ( 'title' , '' ) ;
$max_words = get_request ( 'max_words' , 3 ) ;
$min_words = get_request ( 'min_words' , 2 ) ;
$redirects = get_request ( 'redirects' , 1 ) ;
$test = isset ( $_REQUEST['test'] ) ;

$bad_words = array (
	'class' , 'xxxx' , 'caption' , 'float' , 'cellpadding' , 'bgcolor' , 'http' , 'co' , 'www' , 'km' , 'nbsp' ,
	
	// DE
	'der' , 'die' , 'das' , 'und' , 'oder' , 'hat' , 'ein' , 'eine' , 'dass' , 'daß' , 'des' , 'dem' , 'den' , 'man' , 'was' , 'sich' , 'am' , 'um' , 'mit' , 'zu' , 'ist' , 
	'sind' , 'war' , 'waren' , 'uk' , 'style' , 'auch ' , 'noch' , 'welcher' , 'eines' , 'Jahrhundert' , 'großen' , 'wie' , 'er' , 'sie' , 'es' , 'vor' , 'ca' , 'sehr' , 'einer' ,
	'alt' , 'als' , 'schon' , 'jahre' , 'dafür' , 'alle' , 'allem' , 'allen' , 'bis' , 'weblinks' , 'right' , 'border' , 'thumb' , 'align' , 'center' , 'etwa'  , 'aus' , 'aber' , 
	'wurde' , 'wird' , 'label' , 'lat' , 'small' , 'type' , 'left' , 'erstmal' , 'vermutlich' , 'sogenannten' , 'dort' , 'diesen' , 'ringsum' , 'einem' , 'long' , 'zur'
) ;

function iterate_titles ( $group , $start , &$ret ) {
	if ( count ( $group ) == 0 ) {
		$ret[] = $start ;
//		print "$start<br/>" ;
		return ;
	}
	$start .= '_' ;
	$s = strtolower ( array_shift ( $group ) ) ;
	iterate_titles ( $group , $start . ucfirst ( $s ) , $ret ) ;
	iterate_titles ( $group , $start . $s , $ret ) ;
}

function get_titles ( $words , $max ) {
	global $bad_words ;
	$group = array () ;
	$ret = array () ;
	while ( count ( $words ) > 0 ) {
		while ( count ( $words ) > 0 && count ( $group ) < $max ) {
			$group[] = array_shift ( $words ) ;
		}
		$bad = false ;
		foreach ( $group AS $g ) {
			if ( !in_array ( strtolower ( $g ) , $bad_words ) ) continue ;
			$bad = true ;
			break ;
		}
		if ( $bad ) {
//			print "BAD : $title<br/>" ;
		} else {
//			print "good : $title<br/>" ;
//			$title = implode ( '_' , $group ) ;
			$g = $group ;
			$start = ucfirst ( array_shift ( $g ) ) ;
			iterate_titles ( $g , $start , $ret ) ;
//			$ret[] = $title ;
		}
		array_shift ( $group ) ;
	}
	return $ret ;
}

function db_get_similar_titles ( $title , $language , $project , $redirects ) {
	make_db_safe ( $title ) ;
//	$title = str_replace ( '_' , '?' , $title ) ;
	$ret = array () ;
	$mysql_con = db_get_con_new($language) ;
	$db = $language . 'wiki_p' ;
	
	$u2 = implode ( "," , $users_ids ) ;
	$sql = "SELECT page_title FROM page WHERE page_namespace = 0 AND page_title LIKE \"$title\"" ;
	if ( $redirects == '0' ) $sql .= " AND page_is_redirect = 0" ;
//	print "$sql<br/>" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return $ret ; // Something's broken
	
	$nt = strtolower ( $title ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$pt = strtolower ( $o->page_title ) ;
		$pt = preg_replace ( '/-/' , '_' , $pt ) ;
		if ( $pt != $nt ) continue ;
	    $ret[] = $o->page_title ;
  	}
	
	return $ret ;
}



print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;
print "</head><body>" ;
print get_common_header ( "missing_links.php" ) . "\n" ;

if ( $title == '' ) $wiki = '' ;
else $wiki = get_wikipedia_article ( $language , $title , false , $project ) ;
if ( strtoupper ( substr ( $wiki , 0 , 9 ) ) == '#REDIRECT' ) {
	$title = array_pop ( explode ( '[[' , $wiki , 2 ) ) ;
	$title = array_shift ( explode ( ']]' , $title , 2 ) ) ;
	$title = trim ( $title ) ;
	print "<i>REDIRECTing to $title</i><br/>" ;
	myflush() ;
	$wiki = get_wikipedia_article ( $language , $title , false , $project ) ;
}

#print "<pre>$wiki</pre>" ;
#print "<hr/>" ;

$wiki = preg_replace ( '/\[\[[^\[\]]+\]\]/' , ' xxxx ' , $wiki ) ;

$words = array () ;
preg_match_all ( '/[a-zA-ZäöüÜÖÄ][a-zäüöß]+/' , $wiki , &$words ) ;
$words = $words[0] ;

#print "<pre>$wiki</pre>" ;

$dbt = str_replace ( ' ' , '_' , $title ) ;
$links = db_get_page_links ( $dbt , $language , $project ) ;
$links[$dbt] = $dbt ;


$titles = array () ;
for ( $i = $min_words ; $i <= $max_words ; $i++ ) {
	$nt = array_unique ( get_titles ( $words , $i ) ) ;
	while ( count ( $nt ) > 0 ) $titles[] = array_pop ( $nt ) ;
}


$out = array () ;
foreach ( $titles AS $t ) {
	$t2 = db_get_similar_titles ( $t , $language , $project , $redirects ) ;
	if ( count ( $t2 ) == 0 ) continue ;
	while ( count ( $t2 ) > 0 ) {
		$l = array_pop ( $t2 ) ;
		$l = ucfirst ( str_replace ( ' ' , '_' , $l ) ) ;
		if ( in_array ( $l , $links ) ) continue ;
		if ( in_array ( $l , $out ) ) continue ;
		$out[] = $l ;
	}
}

asort ( $out ) ;

$nice_title = str_replace ( '_' , ' ' , $title ) ;
print "<h2>Link suggestions for \"<a target='_blank' href='http://$language.$project.org/wiki/$title'>$nice_title</a>\", $min_words-$max_words words</h2>" ;

if ( count ( $out ) > 0 ) {
	print "<ol>" ;
	foreach ( $out AS $o ) {
		$nice = str_replace ( '_' , ' ' , $o ) ;
		$url = "http://$language.$project.org/wiki/$o" ;
		$comment = '' ;
		if ( db_is_redirect ( $o , $language , $project ) ) {
			$wiki = get_wikipedia_article ( $language , $o , false , $project ) ;
			$o2 = array_pop ( explode ( '[[' , $wiki , 2 ) ) ;
			$o2 = array_shift ( explode ( ']]' , $o2 , 2 ) ) ;
			$o2 = str_replace ( ' ' , ' ' , trim ( $o2 ) ) ;
			$o2n = str_replace ( '_' , ' ' , $o2 ) ;
			$in_article = in_array ( $o2 , $links ) ? ', already linked' : '' ;
			$comment .= " [#REDIRECT <a target='_blank' href='http://$language.$project.org/wiki/$o2'>$o2n</a>$in_article]" ;
		}
		
		if ( $language == 'de' ) {
			$cats = get_categories_of_page ( $language , $project , $o ) ;
			if ( isset ( $cats['Begriffsklärung'] ) ) $comment .= " [BKL]" ;
		}
		
		print "<li><a target='_blank' href='$url'>$nice</a>$comment</li>" ;
	}
	print "</ol>" ;
} else {
	print "None found." ;
}

print "<hr/><small>Usage : http://toolserver.org/~magnus/missing_links.php?language=en&project=wikipedia&title=The_article&min_words=2&max_words=3&redirects=1</small>" ;

print "</body>" ;
print "</html>\n" ;
myflush() ;



?>
