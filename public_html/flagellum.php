<?PHP

include_once ( "common.php" ) ;

$title = get_request ( 'title' , '' ) ;
$language = get_request ( 'language' , 'de' ) ;

$h = '500px' ;

// Get revision data
$wq = new WikiQuery ( $language , "wikipedia" ) ;
$data = $wq->get_revisions ( $title , 15 ) ;

// Collect user data
$users = array () ;
foreach ( $data AS $d ) {
	if ( isset ( $d['anon'] ) ) continue ;
	$users[$d['user']] = $d['user'] ;
}
$ud = $wq->get_user_data ( $users ) ;

// Output
$current = '' ;
$hist = "<table style='font-size:8pt' cellpadding='2px' cellspacing='0px'>" ;
foreach ( $data AS $d ) {
	$col = '' ;
	$flag = '' ;
	if ( isset ( $ud[$d['user']]['editcount'] ) ) $ec = $ud[$d['user']]['editcount'] ;
	else $ec = -1 ;
	if ( isset ( $d['minor'] ) ) $flag .= 'M' ;
	if ( isset ( $d['anon'] ) ) {
		$flag .= 'A' ;
		$col = '#FBEC5D' ;
	}
	if ( isset ( $ud[$d['user']]['bot'] ) ) {
		$flag .= 'B' ;
		$col = '#33A1DE' ;
	} else {
		if ( $ec > 500 ) $col = '#C5E3BF' ;
		if ( $ec < 50 ) $col = '#FBEC5D' ;
	}
	if ( isset ( $ud[$d['user']]['sysop'] ) ) {
		$col = '#C5E3BF' ;
		$flag .= 'S' ;
	}
	
	if ( $current == '' ) {
		$diff2cur = '' ;
		$current = $d['revid'] ;
	} else {
		$diff2cur = "http://$language.wikipedia.org/w/index.php?title=$title&diff=$current&oldid=" . $d['revid'] ;
		$diff2cur = "<a target='diff' href=\"$diff2cur&useskin=nostalgia\">cur</a>" ;
	}
	
	$hist .= "<tr bgcolor='$col'>" ;
	$hist .= "<td>$diff2cur</td>" ;
	$hist .= "<td>$flag</td>" ;
	$hist .= "<td>" . $d['timestamp'] . "</td>" ;
	$hist .= "<td><a href='http://$language.wikipedia.org/wiki/User:" . myurlencode ( $d['user'] ) . "'>" . $d['user'] . "</a>" ;
	if ( $ec >= 0 ) $hist .= ' [' .  $ec . ' edits]' ;
	$hist .= "</td>" ;
	$hist .= "<td>" . $d['comment'] . "</td>" ;
	$hist .= "</tr>" ;
}
$hist .= "</table>" ;

print "<html>
<head></head>
<body>
<table width='100%' border='1'>
<tr>
<td height='$h' width='50%'>
<iframe style='width:100%;height:100%' src=\"http://$language.wikipedia.org/w/index.php?title=$title&useskin=nostalgia\"></iframe>
</td>
<td height='$h' valign='top'>
$hist
</td>
</tr>
<tr>
<td colspan='2' style='height:300px'>
<iframe name='diff' style='height:100%;width:100%'/>
</td>
</tr>
</table>
</body>
</html>" ;

# <iframe style='width:100%;height:100%' src=\"http://$language.wikipedia.org/w/index.php?title=$title&action=history\"></iframe>

?>