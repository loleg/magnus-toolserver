<?php

require_once ( "common.php" ) ;
$is_on_toolserver = false ; # I don't know why it doesn't work on the toolserver with that setting on...

function get_form ( $baseon ) {
	$ret = "" ;
	$ret .= "<small>Usage : summarizer.php?title=<i>sometitle</i>&basedon=<i>en</i></small>" ;
	$ret .= "<form method='post' enctype='multipart/form-data'>" ;
	$ret .= "Start with article <input type='text' name='title' cols=30 /> on <input type='text' name='baseon' cols='4' value='{$baseon}'/>.<br/>" ;
	$ret .= "<input type='submit' name='doit' value='OK' />" ;
	$ret .= "</form>" ;
	return $ret ;
}


function summarizer_scan_interwiki_pages ( &$texts , &$language_links , $lang , $title , $initial = false , $do_output = true , $did_write = false ) {
	$lang = strtolower ( trim ( $lang ) ) ;
	if ( $lang == "" ) return ;
	if ( !$initial && isset ( $texts[$lang] ) ) return ; # Already done
	
	if ( !isset ( $texts[$lang] ) ) { # Can be skipped for $initial == true
		if ( $do_output ) {
#			if ( !$did_write ) print "Loading " ;
#			else print ", " ;
			$did_write = true ;
			$url = get_wikipedia_url ( $lang , $title ) ;
		}
		$texts[$lang] = get_wikipedia_article ( $lang , $title ) ;
		$p = get_initial_paragraph ( $texts[$lang] , $lang ) ;
		if ( $do_output ) {
			
			$apiurl = "http://$lang.wikipedia.org/w/api.php?format=xml&action=parse&text=" . urlencode ( $p ) ;
			$html = file_get_contents ( $apiurl ) ;
			$html = array_pop ( explode ( '<text>' , $html , 2 ) ) ;
			$html = array_shift ( explode ( '</text>' , $html , 2 ) ) ;
			$html = html_entity_decode ( $html ) ;
			
			print "<h2>{$lang}:" . wiki2html ( $title ) . "</h2>" ;
			print "<p>$html</p>" ;
			myflush () ;
		}

	}
	$lls = get_language_links ( $texts[$lang] , false ) ;
	
	if ( !isset ( $language_links[$lang] ) ) {
		$language_links[$lang] = array () ;
	}
	
	# Recursively scan other languages
	foreach ( $lls AS $llk => $llv ) {
		$llk = strtolower ( trim ( $llk ) ) ;
		if ( $llk == "" ) continue ;
		$language_links[$lang][$llk] = $llv ;
		summarizer_scan_interwiki_pages ( $texts , $language_links , $llk , $llv , false , $do_output , $did_write ) ;
	}
}

# MAIN


# Check form
$baseon = trim ( strtolower ( get_request ( 'baseon' , 'en' ) ) ) ;
$title = trim ( get_request ( 'title' ) ) ;
$commons_title = trim ( get_request ( 'commons_title' , $title ) ) ;
$use_common_sense = get_request ( 'use_common_sense' , false ) ;
$directlinks = isset ( $_REQUEST['directlinks'] ) ;
$random = false ;

if ( $title == 'randomtitle' ) {
	$wq = new WikiQuery ( $basedon , 'wikipedia' ) ;
	$title = array_pop ( $wq->get_random_page () ) ;
	$title = $title['title'] ;
	$random = true ;
}

# Output
print "<html>" ;

if ( $title == "" ) {
	print "<body>" ;
	print get_common_header ( "summarizer.php" ) ;
	print "<h1>Summarizer</h1>" ;
	print get_form ( $baseon ) ;
	print "</body></html>" ;
	exit ;
} else {
	if ( $random ) {
	print "<script type='text/javascript'>
function reloadthis () {
setTimeout ( \"window.location='./summarizer.php?baseon=$baseon&title=randomtitle'\" , 5000 ) ;
}
</script>" ;
	print "<body onload='reloadthis();'>" ;
	} else print "<body>" ;
}

#db_increase_usage_counter ( 'summarizer' ) ;
#print db_get_usage_counter ( 'summarizer' ) . "<br/>" ;


# Get texts and interlanguage links
$texts = array () ;
$language_links = array () ;
summarizer_scan_interwiki_pages ( $texts , $language_links , $baseon , $title ) ;

print "<hr/>" ;
print "</body></html>" ;

?>
