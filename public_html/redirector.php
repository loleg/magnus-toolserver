<?php

error_reporting ( E_ALL ) ;

include_once ( "queryclass.php") ;

@set_time_limit ( 20*60 ) ; # Time limit 20 min


#________________________________________________________________________________________________________________________

function add_redirect ( $from , $lang , &$redirects ) {
    if ( !isset ( $redirects[$from] ) ) $redirects[$from] = array () ;
    $redirects[$from][$lang] = $lang ;
}


#________________________________________________________________________________________________________________________

$bigones = array ( 'en' , 'de' , 'fr' , 'pl' , 'ja' , 'nl' , 'it' , 'pt' , 'es' , 'sv' ) ;
asort ( $bigones ) ;

$language = fix_language_code ( get_request ( 'language' , 'de' ) ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$depth = get_request ( 'depth' , 0 ) ;
$category = get_request ( 'category' , '' ) ;
$hidezero = isset ( $_REQUEST['hidezero'] ) ;
$doit = isset ( $_REQUEST['doit'] ) ;
if ( !$doit ) $hidezero = 1 ; # Default
$hzchk = $hidezero ? 'checked' : '' ;

# Header
print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "missingtopics.php" ) ;
print "<body><h1>Redirector</h1>" ;
print "Scan a category tree, find language links, find redirects to those, and check if these redirects are also wanted in the original project. Duh!<br/>" ;
print "<i>Currently checks " . implode ( ", " , $bigones ) . " wikipedias.</i><br/>" ;
print "<small>Output format : Link to existing redirect or button to create new redirect ; number of times the redirect is linked to in the original wiki ; languages that have an article or redirect under that name.</small>" ;

print "<form method='post' taget='pages_in_cats.php'>
<table>
<!--<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>-->
<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
<tr><th>Category</th><td><input type='text' name='category' value='{$category}'/></td></tr>
<tr><th>Depth</th><td><input type='text' name='depth' value='{$depth}'/></td></tr>
<tr><th></th><td><input type='checkbox' name='hidezero' value=1 {$hzchk}/>Hide redirects that are not wanted</td></tr>
<tr><th></th><td><input type='submit' name='doit' value='Run'/></td></tr>
</table></form>" ;

if ( !$doit ) {
  print "</body></html>" ;
  exit ;
}

$wq = array () ;
foreach ( $bigones AS $b ) $wq[$b] = new WikiQuery ( $b , $project ) ;
if ( !isset ( $wq[$language] ) ) $wq[$language] = new WikiQuery ( $language , $project ) ;

$pages_orig = db_get_articles_in_category ( $language , $category , $depth ) ;

print "Scanning " . count ( $pages_orig ) . " articles..." ; myflush();
print "<table border='1'>" ;

foreach ( $pages_orig AS $page ) {
  $pretty_page = str_replace ( '_' , ' ' , $page ) ;
  $lls = db_get_language_links ( $page , $language , $project ) ;
  
  $redirects = array () ;
  foreach ( $lls AS $k => $ll ) {
    if ( !in_array ( $k , $bigones ) ) continue ;
    add_redirect ( $ll , $k , $redirects ) ;
    
    $res = $wq[$k]->get_backlinks_api ( $ll , 'redirects' ) ;
    foreach ( $res AS $re ) add_redirect ( $re , $k , $redirects ) ;
  }
  unset ( $redirects[$page] ) ;
  
  $existing = $wq[$language]->get_existing_pages ( array_keys ( $redirects ) ) ;
  
  $recount = array () ;
  foreach ( $redirects AS $repage => $v ) {
    $bls = $wq[$language]->get_backlinks_api ( $repage ) ;
    $recount[$repage] = count ( $bls ) ;
  }
  ksort ( $recount ) ;
  arsort ( $recount ) ;
  
  if ( $hidezero ) {
    foreach ( $recount AS $k => $v ) {
      if ( $v == 0 ) unset ( $recount[$k] ) ;
    }
  }
  
  print "<tr>" ;
  print "<th valign='top'><a target='_blank' href=\"http://$language.$project.org/wiki/$page\">$pretty_page</a></th>" ;

  if ( count ( $recount ) == 0 ) {
    print "<td colspan='2'><i>No redirects wanted!</i></td></tr>" ;
    myflush();
    continue ;
  }

  
  print "<td valign='top'>Non-existing redirects:<ul>" ;
  foreach ( $recount AS $k => $v ) {
    if ( in_array ( $k , $existing ) ) continue ;
    print "<li>" ;
    $text = "#REDIRECT [[$pretty_page]]" ;
    $summary = "$text (http://tools.wikimedia.de/~magnus/redirector.php)" ;
    $bt = "Redirect \"$k\" to \"$pretty_page\"" ;
    print cGetEditButton ( $text , $k , $language , $project , $summary , $bt , true , false , false , true ) ;

    print " ($v&times;) <i>" . implode ( ", " , $redirects[$k] ) . "</i></li>" ;
  }
  print "</ul></td>" ;
  
  print "<td valign='top'>Existing redirects:<ul>" ;
  foreach ( $recount AS $k => $v ) {
    if ( !in_array ( $k , $existing ) ) continue ;
    print "<li><a target='_blank' href=\"http://$language.$project.org/wiki/$k\">$k</a>" ;
    print " ($v&times;) <i>" . implode ( ", " , $redirects[$k] ) . "</i></li>" ;
  }
  print "</ul></td>" ;
  
  print "</tr>" ;
  myflush() ;
}

print "</table>All done!" ;

?>