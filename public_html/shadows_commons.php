<?PHP

error_reporting ( E_ALL ) ;
$suppress_gz_handler = 1 ;
include_once ( 'queryclass.php' ) ;
high_mem ( 256 , 'shadows_commons' ) ;

print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "shadows_commons.php" ) . "\n" ;
myflush() ;

$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$nonidentical = isset ( $_REQUEST['nonidentical'] ) ;


function read_sha1_from_file ( $file , &$images ) {
	$ret = array () ;
	$fh = fopen($file, 'r');
	while ( !feof ( $fh ) ) {
		$l = explode ( "\t" , fgets($fh) ) ;
		if ( count ( $l ) != 3 ) continue ;
		if ( !in_array ( $l[2] , $images ) ) continue ;
//		print $l[2] . "<br/>\n" ; myflush();
		$ret[$l[2]] = $l[0] . " " . $l[1] ;
	}
	fclose($fh);	
	return $ret ;
}



function db_get_image_usage ( $language , $project , $image ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	make_db_safe ( $image ) ;
	
	$ret = array () ;
	$sql = "SELECT * FROM page,imagelinks WHERE il_to=\"$image\" AND page_id=il_from" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return $ret ; // Something's broken
	
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[$o->cl_to] = $o ;
	}
	return $ret ;
}


$file = "../images.shadows.$language" ;
if ( $project == "wikiversity" ) $file .= "_wv" ;
$text = file_get_contents ( $file ) ;
if ( $text == '' ) {
	print "Data set empty or $language.$project is not supported yet." ;
} else {

	$q = new WikiQuery ( $language , $project ) ;
	$ns = $q->get_namespaces () ;

	$lines = explode ( "\n" , trim ( $text ) ) ;
	
	print count ( $lines ) . " files on $language.$project have the same name as a file on Commons, making the latter unaccessible at $language.$project.\n" ;
	
/*
	$file = "../images.$language" ;
	if ( $project == "wikiversity" ) $file .= "_wv" ;
	$local = read_sha1_from_file ( $file , $lines ) ;
	$commons = read_sha1_from_file ( "../images.commons" , $lines ) ;
*/
	$local = db_get_images_data ( $lines , $language , $project ) ;
	$commons = db_get_images_data ( $lines , 'commons' , 'wikimedia' ) ;

	print "<table border=1 cellspacing=0 cellpadding=2><tr><th>Local file</th><th>Commons file</th><th>Identical</th></tr>" ;
	foreach ( $lines AS $image ) {
		if ( $nonidentical and $local[$image]->img_sha1 == $commons[$image]->img_sha1 ) continue ;
		$ni = str_replace ( '_' , ' ' , $image ) ;
		print "<tr>" ;
		print "<td><a target='_blank' href=\"http://$language.$project.org/wiki/Image:$image\">$ni</a></td>" ;
		print "<td><a target='_blank' href=\"http://commons.wikimedia.org/wiki/Image:$image\">$ni</a></td>" ;
		print "<td bgcolor=" ;
		if ( $local[$image]->img_sha1 == $commons[$image]->img_sha1 ) print "green>yes" ;
		else print "red>no" ;
		print "</td>" ;
/*		
		$usage = db_get_image_usage ( $language , $project , $image ) ;
		if ( count ( $usage ) == 0 ) {
			print "; not used on $language.$project" ;
		} else {
			print "<br/>used in " ;
			foreach ( $usage AS $k => $u ) {
				if ( $k > 0 ) print ", " ;
				$t = $ns[$u->page_namespace] ;
				if ( $t != '' ) $t .= ':' ;
				$t .= $u->page_title ;
				$nt = str_replace ( '_' , ' ' , $t ) ;
				print "<a target='_blank' href=\"http://$language.$project.org/wiki/$t\">$nt</a>" ;
			}
		}*/
		
		print "</tr>\n" ;
		myflush();
	}
	print "</table>" ;
}

print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
