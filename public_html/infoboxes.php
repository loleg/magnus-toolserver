﻿<?PHP
error_reporting ( E_ALL ) ;
$suppress_gz_handler = 1 ;

include_once ( 'queryclass.php' ) ;

$jobs = array (
	'writer' => '',
	'educator' => '',
	'teacher' => 'educator',
) ;

$ifb = array (
	'actor' => 'Actor',
	'author' => 'Writer',
	'writer' => 'Writer',
	'minister' => 'Officeholder',
	'governor' => 'Officeholder',
	'president' => 'Officeholder',
	'painter' => 'Artist',
	'musician' => 'Artist',
	'sculptor' => 'Artist',
	'army' => 'Military Person',
	'navy' => 'Military Person',
	'air force' => 'Military Person',
	'scientist' => 'Scientist',
	'geologist' => 'Scientist',
	'biologist' => 'Scientist',
	'physicist' => 'Scientist',
	'chemist' => 'Scientist',
	'astronomer' => 'Scientist',
	'engineer' => 'Scientist',
	'mathematician' => 'Scientist',
) ;

$ib_keys = array (

	'Person' => array (
		'name' => 'name',
		'image' => 'image',
		'occupation' => 'occupation',
		'birth_date' => 'birth_date',
		'birth_place' => 'birth_place',
		'death_date' => 'death_date',
		'death_place' => 'death_place',
	) ,

	'Military Person' => array (
		'birth_date' => 'born',
		'birth_place' => 'placeofbirth',
		'death_date' => 'died',
		'death_place' => 'placeofdeath',
	) ,

	'Writer' => array (
		'birth_date' => 'birthdate',
		'birth_place' => 'birthplace',
		'death_date' => 'deathdate',
		'death_place' => 'deathplace',
	) ,

	'Artist' => array (
		'birth_date' => 'birthdate',
		'birth_place' => 'location',
		'death_date' => 'deathdate',
		'death_place' => 'deathplace',
	) ,

) ;

$nationalities = array (
	'german' => 'Germany',
	'dutch' => 'Netherlands',
	'netherlands' => 'Netherlands',
	'flamish' => 'Netherlands',
	'spanish' => 'Spain',
	'spain' => 'Spain',
	'portuguese' => 'Portugal',
	'portugal' => 'Portugal',
	'italian' => 'Italy',
	'english' => 'British',
	'u.k.' => 'British',
	'u. k.' => 'British',
	'british' => 'British',
	'u.s.' => 'United States',
	'u. s.' => 'United States',
	'united states' => 'United States',
	'america' => 'United States',
	'france' => 'France',
	'french' => 'France',
	'norway' => 'Norway',
	'norwegian' => 'Norway',
	'canada' => 'Canada',
	'canadian' => 'Canada',
) ;


$months = array ( 
	1 => 'january' , 
	2 => 'february' , 
	3 => 'march' , 
	4 => 'april',
	5 => 'may',
	6 => 'june' , 
	7 => 'july' , 
	8 => 'august' , 
	9 => 'september' , 
	10 => 'october' , 
	11 => 'november' , 
	12 => 'december'
) ;

function parse_date_place ( $t ) {
	global $months ;
	$ret = '' ;
	$ret->fulldate = false ;
	
	$t = str_replace ( 'born ' , '' , $t ) ;
	$t = str_replace ( 'died ' , '' , $t ) ;

	$ret->place = '' ;
	$t2 = explode ( ' in ' , $t , 2 ) ;
	if ( count ( $t2 ) == 2 ) {
		$t = $t2[0] ;
		$ret->place = trim ( $t2[1] ) ;
	}
	
	$t2 = $t ;
	$ok = false ;
	foreach ( $months AS $k => $m ) {
		for ( $day = 31 ; $day > 0 ; $day-- ) {
			if ( false !== stristr ( $t , "$day $m" ) ) {
				$t2 = str_ireplace ( "$day $m" , '' , $t2 ) ;
				$ok = true ;
			} else if ( false !== stristr ( $t , "$day. $m" ) ) {
				$t2 = str_ireplace ( "$day. $m" , '' , $t2 ) ;
				$ok = true ;
			} else if ( false !== stristr ( $t , "$m $day" ) ) {
				$ok = true ;
				$t2 = str_ireplace ( "$m $day" , '' , $t2 ) ;
			}
			if ( !$ok ) continue ;
			$ret->month_name = $m ;
			$ret->month_num = $k ;
			$ret->day = $day ;
			$x = array () ;
			preg_match ( '/\d+/' , $t2 , $x ) ;
			$ret->year = $x[0] ;
			$ret->fulldate = true ;
			break ;
		}
		if ( $ok ) break ;
	}
	
	$t = trim ( $t ) ;
	$ret->fullday = $t ;
	if ( preg_match ( '/^\d+$/' , $t ) ) {
		$ret->year = $t ;
		$ret->fullday = "[[$t]]" ;
	}
	return $ret ;
}

function get_life_and_death ( $ini ) {
	$i = array_pop ( explode ( '(' , $ini , 2 ) ) ;
	$b = 1 ;
	$desc = '' ;
	for ( $j = 0 ; $j < strlen ( $i ) ; $j++ ) {
		if ( substr ( $i , $j , 1 ) == '(' ) $b++ ;
		else if ( substr ( $i , $j , 1 ) == ')' ) $b-- ;
		if ( $b > 0 ) continue ;
		$desc = trim ( substr ( $i , $j+1 ) ) ;
		$i = substr ( $i , 0 , $j ) ;
		break ;
	}
	
	$birthplace = '' ;
	$desc = str_replace ( ', co. ' , ' co ' , $desc ) ;
	if ( false !== strstr ( $desc , "born in" ) ) {
		$d = explode ( 'born in' , $desc , 2 ) ;
		$desc = trim ( array_shift ( $d ) ) ;
		$d = array_pop ( $d ) ;
		$birthplace = trim ( before_next ( $d , ',' ) ) ;
		$birthplace = trim ( before_next ( $birthplace , '.' ) ) ;
		$d = substr ( $d , strlen ( $birthplace ) + 1 ) ;
		$desc = trim ( trim ( $desc ) . ' ' . trim ( $d ) ) ;
	}
	if ( false !== strstr ( $desc , "born at" ) ) {
		$d = explode ( 'born at' , $desc , 2 ) ;
		$desc = trim ( array_shift ( $d ) ) ;
		$d = array_pop ( $d ) ;
		$birthplace = trim ( before_next ( $d , ',' ) ) ;
		$birthplace = trim ( before_next ( $birthplace , '.' ) ) ;
		$d = substr ( $d , strlen ( $birthplace ) + 1 ) ;
		$desc = trim ( trim ( $desc ) . ' ' . trim ( $d ) ) ;
	}
	$desc = trim ( $desc , ',. ' ) ;
	
	if ( substr ( $desc , 0 , 4 ) == 'was ' or substr ( $desc , 0 , 3 ) == 'is ' ) $desc = trim ( array_pop ( explode ( ' ' , $desc , 2 ) ) ) ;
	if ( substr ( $desc , 0 , 3 ) == 'an ' or substr ( $desc , 0 , 2 ) == 'a ' or substr ( $desc , 0 , 4 ) == 'the ' ) $desc = trim ( array_pop ( explode ( ' ' , $desc , 2 ) ) ) ;
	$longdesc = '' ;
	if ( $desc != '' ) {
		$b = 0 ;
		for ( $j = 0 ; $j < strlen ( $desc ) ; $j++ ) {
			if ( $desc[$j] == '(' or $desc[$j] == '[' or $desc[$j] == '{' ) $b++ ;
			else if ( $desc[$j] == ')' or $desc[$j] == ']' or $desc[$j] == '}' ) $b-- ;
			if ( $b > 0 or $desc[$j] != '.' ) continue ;
			$desc = trim ( substr ( $desc , 0 , $j ) ) ;
		}
	}

	$i2 = explode ( '&ndash;' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = explode ( '–' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = explode ( '&mdash;' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = explode ( ' - ' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = explode ( '-' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = explode ( '; ' , $i , 2 ) ; 
	if ( count ( $i2 ) == 1 ) $i2 = array ( $i , '' ) ;
	
	$d1 = parse_date_place ( $i2[0] ) ;
	$d2 = parse_date_place ( $i2[1] ) ;
	if ( $d1->place == '' ) $d1->place = $birthplace ;
	
	$desc = trim ( $desc , ',. ' ) ;
	$ret = '' ;
	$ret->birth = $d1 ;
	$ret->death = $d2 ;
	$ret->desc = $desc == '' ? $longdesc : $desc ;
	$ret->longdesc = $longdesc ;
	return $ret ;
}

function before_next ( $t , $k ) {
	$b = 0 ;
	for ( $j = 0 ; $j < strlen ( $t ) ; $j++ ) {
		if ( $b == 0 and $t[$j] == $k ) return substr ( $t , 0 , $j ) ;
		if ( $t[$j] == '(' or $t[$j] == '[' or $t[$j] == '{' ) $b++ ;
		else if ( $t[$j] == ')' or $t[$j] == ']' or $t[$j] == '}' ) $b-- ;
	}
	return $t ;
}

function get_name ( $ini , $alt ) {
	$ini = " $ini" ;
	$ini = explode ( "'''" , $ini ) ;
	if ( count ( $ini ) == 3 ) {
		return $ini[1] ;
	}
	return $alt ;
}



#______________________

$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$category = str_replace ( ' ' , '_' , get_request ( 'category' , '' ) ) ;
$the_page = str_replace ( ' ' , '_' , get_request ( 'page' , '' ) ) ;
$depth = 0 ;


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "infoboxes.php" ) . "\n" ;
myflush() ;

print "<p>Checking articles in <i>$category</i></p>\n" ; myflush() ;

if ( $category != '' ) {
	$done_cats = array() ;
	$pages = db_get_articles_in_category ( $language , $category , $depth , 0 , $done_cats , false , '' , $project ) ;
} else if ( $the_page != '' ) {
	$pages = array ( $the_page ) ;
} else {
	print "Specify page=SOMEPAGE or category=SOMECATEGORY." ;
	exit ;
}

print "<p style='color:red'>ATTENTION : The following Infobox/Persondata templates are automatically filled as good as possible, but most will require manual error fixing and extension. DO NOT JUST COPY THEM OVER BLINDLY!</p>" ;
print "<p>Checking " . count ( $pages ) . " pages...</p><ol>" ;
myflush() ;
foreach ( $pages AS $page ) {
	$url = "http://$language.$project.org/wiki/$page" ;
	$page2 = str_replace ( '_' , ' ' , $page ) ;
	$pagel = "<a target='_blank' href=\"$url\">$page2</a>" ;
	$templates = db_get_used_templates ( $language , $page , 0 , $project ) ;
	$i = array () ;
	foreach ( $templates AS $t ) {
		if ( substr ( $t , 0 , 7 ) != 'Infobox' ) continue ;
		$i[] = $t ;
	}
	print "<li>" ;
	if ( count ( $i ) > 0 ) {
		print "<b>$pagel</b> uses " . implode ( ', ' , $i ) ;
	} else {
		print "<b>$pagel</b> [<a target='_blank' href=\"$url?action=edit\">edit</a>] does not use infoboxes.<br/>" ;
		$cats = get_categories_of_page ( $language , $project , $page ) ;
		$text = file_get_contents ( "http://$language.$project.org/w/index.php?title=$page&action=raw&section=0" ) . "\n" ;
		$ini = get_initial_paragraph ( $text , $language ) ;
		$il = strtolower ( $ini ) ;

		$lad = get_life_and_death ( $ini ) ;
		
		$image = '' ;
		$i = explode ( '[[image:' , strtolower ( $text ) , 2 ) ;
		if ( count ( $i ) > 1 ) {
			$i = substr ( $text , strlen ( $i[0] ) + 8 ) ;
			$i = before_next ( $i , ']' ) ;
			$in = explode ( '|' , $i , 2 ) ;
			$image = array_shift ( $in ) ;
		}
		
		$name = get_name ( $ini , $page2 ) ;
		$altname = $name == $page2 ? '' : $page2 ;
		$listname = '' ;
		foreach ( $cats AS $v ) {
			if ( $v->cl_sortkey == '' ) continue ;
			$listname = $v->cl_sortkey ;
		}
		if ( $listname == '' ) {
			$l = explode ( ' ' , $name ) ;
			$listname = array_pop ( $l ) . ', ' ;
			$listname .= implode ( ' ' , $l ) ;
		}
		
		$infobox = 'Person' ;
		$ibk = $ib_keys[$infobox] ;
		$occupations = array () ;
		foreach ( $jobs AS $k => $v ) {
			if ( false === stristr ( $il , $k ) ) continue ;
			if ( $v == '' ) $v = $k ;
			if ( isset ( $ifb[$v] ) ) {
				$infobox = $ifb[$v] ;
			} else $occupations[] = $v ;
		}
		$occupation = implode ( "<br/>\n" , $occupations ) ;
		if ( $occupation == '' ) $occupation = $lad->longdesc ;
		if ( $occupation == '' ) $occupation = $lad->desc ;
		
		if ( $infobox == 'Person' ) {
			foreach ( $ifb AS $k => $v ) {
				if ( false === stristr ( $occupation , $k ) ) continue ;
				$infobox = $v ;
				if ( strtolower ( $occupation ) == strtolower ( $v ) ) $occupation = '' ;
				break ;
			}
		}
		
		if ( $lad->birth->fulldate ) {
			if ( $lad->death->fullday == '' ) { // Living
				$birthdate = "{{Birth date and age|{$lad->birth->year}|{$lad->birth->month_num}|{$lad->birth->day}|mf=y}}" ;
			} else { // Dead
				$birthdate = "{{Birth date|{$lad->birth->year}|{$lad->birth->month_num}|{$lad->birth->day}|mf=y}}" ;
			}
		} else $birthdate = $lad->birth->fullday;
		//  <!-- {{Birth date|YYYY|MM|DD}} -->
		
		if ( $lad->birth->fulldate and $lad->death->fulldate ) {
			$deathdate = "{{Death date and age|{$lad->death->year}|{$lad->death->month_num}|{$lad->death->day}|{$lad->birth->year}|{$lad->birth->month_num}|{$lad->birth->day}|mf=y}}" ;
		} else $deathdate = $lad->death->fullday;
		
		if ( isset ( $ib_keys[$infobox] ) ) {
			foreach ( $ib_keys[$infobox] AS $k => $v ) {
				$ibk[$k] = $v ;
			}
		}
		
		if ( $image == '' ) $image = 'Replace this image1.svg' ;
		
		$nationality = '' ;
		foreach ( $nationalities AS $k => $v ) {
			if ( false === stristr ( $ini , $k ) ) continue ;
			$nationality = "[[$v]]" ;
			break ;
		}
		
		if ( $infobox == 'Military Person' ) {
			$image = "[[Image:$image]]" ;
		}
		
		
		$infobox = "{{Infobox $infobox
|{$ibk['name']}=$name
|{$ibk['image']}=$image
|{$ibk['occupation']}=$occupation
|{$ibk['birth_date']}  = $birthdate
|{$ibk['birth_place']} = {$lad->birth->place}
|{$ibk['death_date']}  = $deathdate
|{$ibk['death_place']} = {$lad->death->place}
|nationality = $nationality
}}" ;
		

		$persondata = "<!-- Metadata: see [[Wikipedia:Persondata]] -->
{{Persondata
|NAME              = $listname
|ALTERNATIVE NAMES = $altname
|SHORT DESCRIPTION = {$lad->desc}
|DATE OF BIRTH     = {$lad->birth->fullday}
|PLACE OF BIRTH    = {$lad->birth->place}
|DATE OF DEATH     = {$lad->death->fullday}
|PLACE OF DEATH    = {$lad->death->place}
}}" ;
		
		
		print htmlspecialchars ( $ini ) ;
		print "<pre>$infobox\n\n$persondata</pre>" ;
	}
	print "</li>" ;
	myflush() ;
//	if ( $cnt++ > 15 ) break ;
}
print "</ol>" ;


print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
