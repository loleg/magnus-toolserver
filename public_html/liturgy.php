<?PHP
error_reporting ( E_ALL ) ;
//@set_time_limit ( 15*60 ) ; # Time limit 45min

include_once ( 'queryclass.php' ) ;

function query_plos ( $article , $reviews_only , $fulltext_search ) {
	myflush() ; print "Querying PLoS...<br/>\n" ; myflush() ;
	$ret = array () ;
	
	$ua = urlencode ( strtolower ( $article ) ) ;
	$url = "http://www.plosone.org/search/advancedSearch.action?pageSize=100&sort=Relevance&queryField=abstract&unformattedQuery=" ;
	$url .= $fulltext_search == 1 ? $ua : "(title:$ua)+OR+(abstract:$ua)" ;
	$url .= "&journalOpt=all&subjectCatOpt=all&filterArticleTypeOpt=some" ;
	if ( $reviews_only == 1 ) $url .= "&filterArticleType=Review" ;

	$s = file_get_contents ( $url ) ;
	$doc = new DOMDocument();
	$doc->loadHTML ( $s ) ;
	
	$e = $doc->getElementById ( 'searchResults' ) ;
	foreach ( $e->childNodes AS $n ) {
		if ( strtolower ( $n->nodeName ) != 'li' ) continue ;
		$o = array () ;
		$o['source'] = 'plos' ;
		$o['doi'] = $n->attributes->getNamedItem('doi')->nodeValue ;
		$o['url'] = 'http://dx.doi.org/' . $o['doi'] ;
		$o['type'] = 'review' ;
		
		foreach ( $n->childNodes AS $n2 ) {
			if ( strtolower ( $n2->nodeName ) == 'span' ) {
				if ( 'article' == $n2->attributes->getNamedItem('class')->nodeValue ) {
					$o['title'] = trim ( $n2->textContent ) ;
/*					foreach ( $n2->childNodes AS $n3 ) { // Using DOI instead
						if ( strtolower ( $n3->nodeName ) != 'a' ) continue ;
						$o['url'] = $n3->attributes->getNamedItem('href')->nodeValue ;
					}*/
				} else if ( 'authors' == $n2->attributes->getNamedItem('class')->nodeValue ) {
					$o['authors'] = trim ( $n2->textContent ) ;
				}
			} else if ( strtolower ( $n2->nodeName ) == 'strong' and !isset ( $o['journal'] ) ) {
				$o['journal'] = trim ( $n2->textContent ) ;
			}
		}
		
		$ret[] = $o ;
	}
	
	return $ret ;
}

function print_pretty ( $papers ) {
	print "<h2>Results</h2>" ;
	print "<ol>" ;
	foreach ( $papers AS $p ) {
		print "<li style='margin-bottom:2pt'><div>" ;
		print "<small>" . $p['authors'] . "</small><br/>" ;
		print "<strong>" . $p['title'] . "</strong><br/>" ;
		print "<em>" . $p['journal'] . "</em> / " . ucfirst ( $p['type'] ) . " / <a target='_blank' href=\"" . $p['url'] . "\">DOI : " . $p['doi'] . "</a><br/>" ;
		print "<small><tt>{{cite journal |author=".$p['authors']." |year= |title=".$p['title']." |journal=".$p['journal']." |publisher= |volume= |issue= |pages= |url= |doi=".$p['doi']." |pmid= |pmc= }}</tt></small>" ;
		print "</div></li>" ;
	}
	print "</ol><hr/>" ;
}

$article = get_request ( 'article' , '' ) ;
$reviews_only = get_request ( 'reviews_only' , '' ) ;
$fulltext_search = get_request ( 'fulltext_search' , '' ) ;
$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;

if ( !isset ( $_REQUEST['doit'] ) ) $reviews_only = '1' ;

print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "liturgy.php" ) ;
print "<h1>LITurgy - the Wikipedia literature game</h1>" ;

$reviews_only_cb = $reviews_only == 1 ? 'checked' : '' ;
$fulltext_search_cb = $fulltext_search == 1 ? 'checked' : '' ;
print "(Currently only for en.wikipedia)
<form method='get'>
<table border='1'>
<tr><th>Language</th><td><input type='text' size='20' name='language' value='$language' /></td></tr>
<tr><th>Article</th><td><input type='text' size='100' name='article' value='$article' /></td></tr>
<tr><th>Type</th><td><input type='checkbox' name='reviews_only' value='1' id='reviews_only' $reviews_only_cb /><label for='reviews_only'>Reviews only</label></td></tr>
<tr><th>Search</th><td><input type='checkbox' name='fulltext_search' value='1' id='fulltext_search' $fulltext_search_cb /><label for='fulltext_search'>Fulltext</label></td></tr>
<tr><td/><td><input type='submit' name='doit' value='Do it!' /></td></tr>
</table>
</form>" ;

// <tr><th>Project</th><td><input type='text' size='100' name='project' value='$project' /></td></tr>


if ( isset ( $_REQUEST['doit'] ) && $article != '' ) {
	$art = $article ;
	if ( $language != 'en' ) {
		$q = new WikiQuery ( $language , $project ) ;
		$ll = $q->get_language_links ( $article ) ;
		if ( isset ( $ll['en'] ) ) {
			$art = $ll['en'] ;
			print "Using English equivalent \"$art\" instead.<br/>" ;
		} else {
			print "Could not find English equivalent for \"$article\", aborting." ;
			exit ( 0 ) ;
		}
	}
	
	$art = str_replace ( '_' , ' ' , $art ) ;
	$art = str_replace ( '+' , ' ' , $art ) ;
	$papers = array () ;
	$d = query_plos ( $art , $reviews_only , $fulltext_search ) ;
	foreach ( $d AS $k => $v ) $papers[] = $v ;
	
	print_pretty ( $papers ) ;
	
/*	print "<pre>" ;
	print_r ( $papers ) ;
	print "</pre>" ;*/
}

print "</body></html>" ;

?>