<?PHP

if ( isset ( $_REQUEST['kml'] ) ) {
	$hide_header = true ;
	$hide_doctype = true ;
}

include_once ( 'common.php' ) ;
include_once ( 'geohack/geo_param.php' ) ;

$geohack_key = 'http://stable.toolserver.org/geohack/geohack.php' ;

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$namespace = get_request ( 'namespace' , 0 ) ;
$title = get_request ( 'title' , '' ) ;

if ( isset ( $_REQUEST['kml'] ) and $title != '' ) {
	$kml = "<?xml version='1.0' encoding='UTF-8'?>\n<kml xmlns='http://www.opengis.net/kml/2.2'>\n<Document>\n" ;
	$kml .= "<name>$title</name>\n<open>1</open>\n<description>Places related to $title (namespace $namespace) from $language.$project.</description>\n<Folder>\n" ;

	make_db_safe ( $language ) ;
	make_db_safe ( $project ) ;
	$language = strtolower ( $language ) ;
	$project = strtolower ( $project ) ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	$stitle = get_db_safe ( $title ) ;
	make_db_safe ( $namespace ) ;
	$sql = "select page_id from page where page_title=\"$stitle\" and page_namespace=\"$namespace\"" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$pid = 0 ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$pid = $o->page_id ;
	}
	if ( $pid == 0 ) {
		print "<p>Page not found : $title</p>" ;
	} else {
		$sql = "select DISTINCT page_id from page,pagelinks where pl_from=$pid and pl_namespace=page_namespace and pl_title=page_title" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		$pids = array () ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			$pids[] = $o->page_id ;
		}
		$pids = implode ( ',' , $pids ) ;
		$sql = "select distinct page_title,el_to from page,externallinks where el_from in ($pids) and el_from=page_id and page_namespace=0" ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			if ( substr ( $o->el_to , 0 , strlen ( $geohack_key ) ) != $geohack_key ) continue ;
			$param = array_pop ( explode ( '&params=' , $o->el_to ) ) ;
			$gp = new geo_param ( $param ) ;
			$id = urlencode ( $o->page_title ) ;
			$pt = str_replace ( '_' , ' ' , $o->page_title ) ;
			$desc = "http://$language.$project.org/wiki/" . $o->page_title ;
			$kml .= "<Placemark id='$id'>\n<name>$pt</name>\n<description>$desc</description>\n<Point>\n<coordinates>{$gp->londeg},{$gp->latdeg},0</coordinates>\n</Point>\n</Placemark>\n" ;
//			print $o->page_title . " : " . $param . " : " . $gp->latdeg . "/" . $gp->londeg . "<br/>" ;
		}
	}
	
	$kml .= "</Folder></Document>\n</kml>" ;
//	print "<pre>" . htmlentities ( $kml ) . "</pre>" ;
	header("Content-Type: application/xml; charset=UTF-8");
	print $kml ;
	exit ( 0 ) ;
}


function print_form () {
	global $language , $project , $namespace , $title ;
	$ns = array () ;
	$ns[0] = '(Article)' ;
	$ns[2] = 'User' ;
		
	print "<form method='get' action='./related_places.php'>
	<table border='1'>
	<tr><th>Language</th><td><input type='text' name='language' value='$language' /></td></tr>
	<tr><th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>
	<tr><th>Namespace</th><td>
	<select name='namespace'>" ;
	
	foreach ( $ns AS $num => $txt ) {
		$checked = $namespace == $num ? 'selected' : '' ;
		print "<option value='$num' $checked>$txt</option>" ;
	}
	
	print "</select>
	</td></tr>
	<tr><th>Title</th><td><input type='text' name='title' value='$title' /></td></tr>
	<tr><td colspan='2' align='right'><input type='submit' name='doit' value='Do it!' /></td></tr>
	</table>
	</form>" ;
}

print "<html><head>" ;
print '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' ;
print "</head><body>" ;
print get_common_header ( 'related_places.php' , 'Related places' ) ;
print "<h1>Related places</h1>" ;

if ( isset ( $_REQUEST['doit'] ) ) {
	$url = "http://toolserver.org/~magnus/related_places.php?kml=1&language=$language&project=$project&namespace=$namespace&title=" . urlencode ( $title ) ;
	$url = 'http://maps.google.com/maps?f=q&hl=en&q=' . urlencode ( $url ) ;
	print "<p>See <a href=\"$url\">the map</a> with the locations.</p>" ;
}

print_form () ;

print "</body></html>" ;

?>